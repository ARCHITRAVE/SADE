![ARCHITARVE logo](https://gitlab.gwdg.de/ARCHITRAVE/SADE/raw/e541e20999135a3eb11f99686a507f7649fc8105/templates/images/logo.svg "ARCHITRAVE logo")

# ARCHITRAVE-SADE

ARCHITRAVE is a digital edition project funded by DFG and ANR. This German-French research project provides several previously unedited reports of German travelers to France from the period 1685-1723.

## About

[![pipeline status](https://gitlab.gwdg.de/ARCHITRAVE/build/badges/master/pipeline.svg)](https://gitlab.gwdg.de/ARCHITRAVE/build/commits/master)

This repository is a fork of [SADE/SADE](https://gitlab.gwdg.de/SADE/SADE) and has been modified for the [ARCHITRAVE website](https://architrave.eu). SADE (Scalable Architecture for Digital Editions) is a backend and frontend application for publishing scholarly editions in a digital medium. It's based on [eXist database](https://exist-db.org).

## How to Use
This repository is part of a continuous integration process that leads to a ready to use [debian package](https://ci.de.dariah.eu/packages/pool/snapshots/s/sade-architrave/) designed for Ubuntu 18.

## Build 

Prerequisites:
- ant >= 1.10.0
- Elastic Search = 6

To get a version of ARCHITRAVE SADE running locally on your computer, first clone this repository in your preferred git directory:

```sh
mkdir ARCHITRAVE-SADE
cd ARCHITRAVE-SADE
git clone git@gitlab.gwdg.de:ARCHITRAVE/SADE.git
```

Once the repository is cloned, run

```sh
ant
```

which installs dependencies and creates a local version of SADE in `/src`.

## Run

Prerequisites:
- running Elastic Search 6 on `localhost:9200`

To start the eXist database, run

```sh
bash src/eXist-db-*/bin/startup.sh
```

SADE's backend is now available at `localhost:8080`. To have a look at the website, view `http://localhost:8080/exist/apps/sade/index.html` in your preferred browser.

### Search functionality

The search functionality is a React component integrated as part of the project JS.
To run it locally, download ElasticSearch 6 ([Link](https://www.elastic.co/de/downloads/past-releases#elasticsearch)) and run it via:

```
  ./bin/elasticsearch
```

By publishing a text/edition via TextGridLab it will automatically be indexed. If you want to reindex already serialised texts, run the script `db/app/sade/modules/elastic_runner.xq` within the eXist database. This will delete all indices and reindex all contents of the published texts/editions.

The contents of the website are indexed differently. On server, it is managed via eXistDB cron job. To index website content locally, you have to provide the content API secret and paste it into `system/repo/content-parser/content/config.xml` within the eXist database. Then run the script `db/app/sade/modules/websitecontentscheduler.xq` within the eXist database.

### Website Content

The website content is loaded through the [Sepia Content Parser library](https://gitlab.gwdg.de/sepia/content-parser). In case of a local setup, the corresponding config must be edited manually to show website content. Therefore, please follow the instructions in the [readme of the content-parser](https://gitlab.gwdg.de/sepia/content-parser/-/blob/main/README.md?ref_type=heads). The config is preset to load the website content for the ARCHITRAVE website from [this repository](https://gitlab.gwdg.de/ARCHITRAVE/website-content). Only the corresponding access token has to be added manually. To change the content of the website please follow the instructions in the [readme of the content git repository](https://gitlab.gwdg.de/ARCHITRAVE/website-content/-/blob/main/README.md?ref_type=heads).

## Contact
Please contact via [contact form on our website](https://architrave.eu/contact.html).
