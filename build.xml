<?xml version="1.0" encoding="UTF-8"?>
<project default="build" name="SADE"
  xmlns:xdb="http://exist-db.org/ant"
  xmlns:ivy="antlib:org.apache.ivy.ant"
  xmlns:git="antlib:com.rimerosolutions.ant.git"
  basedir="."
  >

  <property environment="env" />

  <property name="project.name" value="https://sade.textgrid.de/ns/SADE/architrave" />
  <property name="project.version" value="3.0.0" />
  <property name="project.title" value="[ARCHITRAVE] SADE" />
  <property name="project.abbrev" value="SADE-architrave" />
  <property name="project.processorversion" value="5.3.1" />

  <property name="build.dir" value="build" />
  <property name="src.dir" value="src" />
  <property name="destfile" value="${build.dir}/${project.abbrev}-${project.version}.xar" />
  <property name="exist.root" value="${src.dir}/eXist-db-${project.processorversion}" />
  <property name="exist.memory" value="2048m" />
  <property name="exist.enable-xinclude" value="false" />

  <!-- debian package -->
  <property name="deb.description" value="SADE-ARCHITRAVE debian package: https://architrave.eu" />
  <property name="deb.name" value="sade-architrave" />
  <property name="resources.dir" value="${basedir}/resources" />
  <property name="ivy.version" value="2.5.0" />
  <property name="ivy.jardir" value="${basedir}/lib" />
  <property name="ivy.jarfile" value="${ivy.jardir}/ivy.jar" />
  <tstamp>
    <format property="timestamp" pattern="yyyyMMddHHmmss" unit="hour"/>
  </tstamp>

  <xmlproperty file="expath-pkg.xml.tmpl" />

  <antversion property="antversion" />

  <target name="antversion-test">
    <antversion property="antversionReady" atleast="1.10" />
    <fail unless="antversionReady">installed version of ant (${antversion}) is lower than the
      required one (1.10.0)</fail>
    <echo message="installed version of ant (${antversion}) is satisfying" />
  </target>

  <target name="cleanup">
    <delete dir="${build.dir}" />
    <delete dir="${src.dir}" />
  </target>

  <target name="xar" depends="cleanup">
    <echo message="building xar package for ARCHITRAVE SADE"/>
    <copy file="expath-pkg.xml.tmpl" tofile="expath-pkg.xml" filtering="true" overwrite="true">
      <filterset>
        <filter token="project.name" value="${project.name}" />
        <filter token="project.version" value="${project.version}" />
        <filter token="project.title" value="${project.title}" />
        <filter token="project.abbrev" value="${project.abbrev}" />
        <filter token="project.processorversion" value="${project.processorversion}" />
      </filterset>
    </copy>
    <mkdir dir="${build.dir}" />
    <zip basedir="." destfile="${destfile}" defaultexcludes="no"
      excludes=".git/,${build.dir}/,${src.dir}/,search/node_modules/,search/public/" />
  </target>

  <target name="build" depends="antversion-test, xar">
    <echo message="loading eXist ${project.processorversion}" />
    <!-- this path may be and is subject to change! -->
    <get src="https://github.com/eXist-db/exist/releases/download/eXist-${project.processorversion}/exist-distribution-${project.processorversion}-unix.tar.bz2"
      dest="${build.dir}/eXist-db-${project.processorversion}.tar.bz2"
      skipexisting="true" />
    <untar src="${build.dir}/eXist-db-${project.processorversion}.tar.bz2" dest="${src.dir}" compression="bzip2" />

    <!-- directory name changed: exist-distribution-5.0.0-->
    <move todir="${src.dir}/eXist-db-${project.processorversion}">
      <fileset dir="${src.dir}/exist-distribution-${project.processorversion}" />
    </move>

    <!-- task setpermissions requries at least ant 1.10.0 -->
    <echo message="making shell scripts executable in exist/bin"/>
    <setpermissions permissions="OWNER_READ,OWNER_WRITE,OWNER_EXECUTE,OTHERS_READ,OTHERS_EXECUTE,GROUP_READ,GROUP_EXECUTE">
      <fileset dir="${exist.root}/bin">
        <include name="*.sh"/>
      </fileset>
    </setpermissions>

    <!-- patch exist conf.xml for elastic search index for website content startup trigger -->
    <echo message="patching exist conf.xml for elastic search index for website content startup trigger"/>
    <patch patchfile="exist-conf.patch" originalfile="${src.dir}/eXist-db-${project.processorversion}/etc/conf.xml" />

    <echo message="loading dependencies to autodeploy"/>

    <get src="http://exist-db.org/exist/apps/public-repo/find?abbrev=shared&amp;processor=${project.processorversion}"
      dest="${exist.root}/autodeploy/shared-latest.xar"
      ignoreerrors="false" />

    <!-- FunctX is already available. Due to existdb installation?
    <get src="http://exist-db.org/exist/apps/public-repo/find?abbrev=functx&amp;processor=${project.processorversion}"
      dest="${exist.root}/functx-latest.xar"
      ignoreerrors="false" />
    -->

    <get src="http://exist-db.org/exist/apps/public-repo/find?abbrev=markdown&amp;processor=${project.processorversion}"
      dest="${exist.root}/autodeploy/markdown-latest.xar"
      ignoreerrors="false" />

    <get src="https://ci.de.dariah.eu/exist-repo/public/sade_assets-1.1.0.xar"
      dest="${exist.root}/autodeploy/sade-assets-1.1.0.xar"
      ignoreerrors="false" />
    
    <get src="https://ci.de.dariah.eu/exist-repo/public/code-viewer-2.0.0.xar"
      dest="${exist.root}/autodeploy/codeview-2.0.0.xar"
      ignoreerrors="false" />

    <get src="https://ci.de.dariah.eu/exist/apps/public-repo/public/content-parser-1.5.2.xar"
      dest="${exist.root}/autodeploy/content-parser-1.5.2.xar"
      ignoreerrors="false" />

    <echo message="loading xar package for ARCHITRAVE SADE to autodeploy"/>
    <copy file="${destfile}" todir="${exist.root}/autodeploy" />
  </target>

  <!-- ************************************* -->
  <!-- * Building Debain package below only if specifically triggered * -->
  <!-- ************************************* -->

  <target name="debian-package" depends="ant-dependencies, build, prepare-sade-dir, sade-memory, configure-xinclude-handling">
    <property name="deb.archive.file" value="${build.dir}/${deb.name}_${project.version}-${timestamp}.tar.gz" />
    <property name="deb.version" value="${project.version}-${timestamp}" />

    <!-- https://github.com/tcurdt/jdeb/blob/master/docs/ant.md -->
    <copy todir="${build.dir}/deb">
      <fileset dir="resources/deb"/>
      <filterset begintoken="[[" endtoken="]]">
        <filter token="version" value="${deb.version}"/>
        <filter token="description" value="${deb.description}"/>
        <filter token="name" value="${deb.name}"/>
        <filter token="existMemory" value="${exist.memory}"/>
      </filterset>
    </copy>

    <tar destfile="${deb.archive.file}" compression="gzip" longfile="gnu">
      <tarfileset dir="${build.dir}/sade" filemode="755" username="${deb.name}" group="${deb.name}">
        <include name="**/*.sh"/>
      </tarfileset>
      <tarfileset dir="${build.dir}/sade" includes="**" username="${deb.name}" group="${deb.name}">
        <exclude name="**/*.sh"/>
      </tarfileset>
    </tar>

    <deb destfile="${build.dir}/${deb.name}_${deb.version}.deb" control="${build.dir}/deb/control" verbose="false" >
      <data src="${deb.archive.file}" type="archive">
        <mapper type="perm" prefix="/opt/${deb.name}"/>
      </data>
      <data src="${build.dir}/deb/sade.service"
            dst="/lib/systemd/system/${deb.name}.service"
            type="file" />
    </deb>
    <delete file="${deb.archive.file}" />
    <delete dir="${build.dir}/sade" />
  </target>

  <target name="exist-conf">
    <!-- enable autostart scripts -->
    <xslt-inplace
      input="${exist.root}/etc/conf.xml"
      stylesheet="${resources.dir}/configure-startupTrigger.xsl"
    />

    <!-- enable startup trigger -->
    <xslt-inplace
      input="${exist.root}/etc/conf.xml"
      stylesheet="${resources.dir}/configure-whitespace-preserve.xsl"
    />

    <!-- set backup settings -->
    <xslt-inplace
      input="${exist.root}/etc/conf.xml"
      stylesheet="${resources.dir}/configure-backup.xsl" />
  </target>

  <target name="sade-memory" depends="exist-conf">
    <replace dir="${build.dir}/sade/bin">
      <include name="*.sh" />
      <replacetoken>$$JAVA_OPTS</replacetoken>
      <replacevalue>$$JAVA_OPTS -Xmx${exist.memory}</replacevalue>
    </replace>
    <echo>Total memory available for SADE's eXist-db: ${exist.memory}.</echo>
  </target>

  <target name="configure-xinclude-handling" depends="prepare-sade-dir">
    <xslt-inplace
        input="${build.dir}/sade/etc/conf.xml"
        stylesheet="${resources.dir}/configure-xinclude-handling.xsl">
        <param name="configParam" expression="${exist.enable-xinclude}"/>
    </xslt-inplace>
    <echo>Are XIncludes expanded automatically? ${exist.enable-xinclude}.</echo>
  </target>

  <target name="prepare-sade-dir" depends="build">
    <delete dir="${build.dir}/sade"/>
    <!-- move eXist to the SADE path -->
    <copy todir="${build.dir}/sade">  
      <fileset dir="${exist.root}" includes="**"/>  
    </copy>
  </target>

  <!-- ************************************* -->
  <!-- *    ALL MACROS BELOW THIS LINE!    * -->
  <!-- ************************************* -->
  <macrodef name="move2autodeploy">
    <attribute name="source"/>
    <sequential>
      <move todir="${build.dir}/sade/autodeploy/">
        <fileset dir="@{source}">
          <include name="*.xar"/>
        </fileset>
      </move>
    </sequential>
  </macrodef>

  <macrodef name="xslt-inplace">
    <attribute name="input"/>
    <attribute name="stylesheet"/>
    <element name="param" optional="yes" implicit="true"/>
    <sequential>
      <xslt
        in="@{input}"
        out="@{input}.tmp"
        style="@{stylesheet}">
        <param />
      </xslt>
      <move
        file="@{input}.tmp"
        tofile="@{input}"/>
    </sequential>
  </macrodef>

  <!-- ************************************* -->
  <!-- * ALL DEPENDENCIES BELOW THIS LINE! * -->
  <!-- ************************************* -->
  <target name="ant-dependencies" depends="install-ivy">
    <property name="ivy.default.ivy.user.dir" value="${basedir}/ivy"/>
    <ivy:retrieve conf="tasks"/>
    <path id="classpath">
      <fileset dir="./lib">
        <include name="*.jar"/>
      </fileset>
    </path>
    <!-- git -->
    <taskdef uri="antlib:com.rimerosolutions.ant.git"
        resource="com/rimerosolutions/ant/git/jgit-ant-lib.xml" classpathref="classpath"/>
    <!-- contrib has if -->
    <taskdef resource="net/sf/antcontrib/antcontrib.properties" classpathref="classpath"/>
    <!-- jdeb -->
    <taskdef name="deb" classname="org.vafer.jdeb.ant.DebAntTask" classpathref="classpath"/>
  </target>

  <target name="download-ivy" unless="skip.download">
    <mkdir dir="${ivy.jardir}"/>
    <get
      src="https://repo1.maven.org/maven2/org/apache/ivy/ivy/${ivy.version}/ivy-${ivy.version}.jar"
      dest="${ivy.jarfile}" usetimestamp="true"/>
  </target>

  <target name="install-ivy" depends="download-ivy" description="installs ivy">
    <path id="ivy.lib.path">
      <pathelement location="${ivy.jarfile}"/>
    </path>
    <taskdef resource="org/apache/ivy/ant/antlib.xml" uri="antlib:org.apache.ivy.ant"
        classpathref="ivy.lib.path"/>
  </target>

</project>