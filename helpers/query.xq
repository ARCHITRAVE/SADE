xquery version "3.1";

import module namespace config="https://sade.textgrid.de/ns/config" at "../modules/config.xqm";
import module namespace elastic="http://elastic.io" at "../modules/elastic.xqm";
import module namespace transformXML="https://sade.textgrid.de/ns/transform" at "../modules/architrave/transform.xqm";
import module namespace functx="http://www.functx.com";

declare boundary-space preserve;

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace xhtml="http://www.w3.org/1999/xhtml";
declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";

declare option output:indent "no"; 


let $doc := 1

(:let $harrach_de := transformXML:importFromTGLab('34zmq'):)
(:let $sturm_de := transformXML:importFromTGLab('34zs7'):)
(:let $corfey_de := transformXML:importFromTGLab('3ptwg'):)
(:let $neumann_de := transformXML:importFromTGLab('3qr4f'):)
(:let $pitzler_de := transformXML:importFromTGLab('34znb'):)
(:let $knesebeck_de := transformXML:importFromTGLab('3c0m2'):)

(:let $harrach_fr := transformXML:importFromTGLab('3czfj'):)
(:let $sturm_fr := transformXML:importFromTGLab('3q4rq'):)
(:let $corfey_fr := transformXML:importFromTGLab('3r0fv'):)
(:let $neumann_fr := transformXML:importFromTGLab('3r3nn'):)
(:let $pitzler_fr := transformXML:importFromTGLab('350mg'):)
(:let $knesebeck_fr := transformXML:importFromTGLab('3czn9'):)

(:return $doc:)

(:let $doc := doc("/db/apps/sade-architrave/textgrid/data/3czn9.xml"):)
(:let $e := $doc//tei:pb[@n="45"]/following-sibling::tei:figure[1]/tei:p[2]:)
(:let $e2 := $e//tei:num[2]:)
(:let $name := string($e2):)
(:let $name2 := fn:substring-before(string($e2),'/'):)
  
(:let $name := normalize-unicode('éveque'):)
(:return $name:)

(:let $col := collection("/db/apps/sade-architrave/textgrid/data/"):)
(:let $results := $col//*[contains(@xml:id, 'fig')]:)
(:let $doc := doc("/db/apps/sade-architrave/textgrid/data/3q4rq.xml"):)
(:let $results := $doc//tei:ref[contains(@target, 'sturm_tab.23_drawing.2')]:)


(:let $id  := '3qr4f':)
(:let $doc := collection(concat($config:app-root, '/templates/', $id)):)
(:let $page := ($doc/xhtml:div)[88]:)
(:let $lang := elastic:lang-for-edition-id($id):)
(:let $results := elastic:index-edition-page($page, 'de'):)

(:  :let $target-uri := '/db/apps/sade-architrave/templates/itinerary'
let $c := xmldb:create-collection($target-uri, 'raster')
let $collection-uri := concat($target-uri, '/', 'raster')
let $results := xmldb:store-files-from-pattern(
    $collection-uri, '/vagrant/src/raster', '**/*', 'image/png', fn:true()
)
:)

(:let $locale := 'de':)
(:let $editions := collection(concat($config:app-root, '/textgrid/data')):)
(:let $locale-editions := $editions//tei:text[@xml:lang=$locale]:)
(:let $refs := $locale-editions//tei:ref[@target and not(starts-with(@target, 'http'))]:)
(::)
(:let $z :=:)
(:    for $r in $refs:)
(:    let $target := data($r/@target):)
(:    let $id := substring($target, 2):)
(:    return $id:)
    
(:let $results := elastic:build-ref-map('de'):)
let $results := tokenize('abc,xxx', '\s*,\s*')

return count(('abc'))
