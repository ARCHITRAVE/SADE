xquery version "3.1";

import module namespace transformXML="https://sade.textgrid.de/ns/transform" at "../modules/architrave/transform.xqm";
import module namespace elastic="http://elastic.io" at "../modules/elastic.xqm";

let $c := transformXML:cacheAllEditions()
let $i := elastic:setup()
let $r := elastic:refresh()

return $r