xquery version "3.1";

declare function local:assert($x) {
    if (xs:boolean($x) != fn:true()) then
        error((), 'assertion failed')
    else
        fn:true()
};

declare function local:test-page-extraction() {
    local:assert(0)
};

let $results := (
    local:test-page-extraction()
)

return fn:true()
