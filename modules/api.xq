xquery version "3.1";

import module namespace elastic="http://elastic.io" at "elastic.xqm";
import module namespace functx="http://www.functx.com";
import module namespace config="https://sade.textgrid.de/ns/config" at "config.xqm";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace tg="http://textgrid.info/namespaces/metadata/core/2010";
declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";

declare option output:method "json";
declare option output:media-type "application/json";

declare variable $action := request:get-parameter('action', 'search');
declare variable $terms := request:get-parameter('terms', '');
declare variable $targets := request:get-parameter('targets', '');
declare variable $page := xs:integer(request:get-parameter('page', 1));
declare variable $per_page := xs:integer(request:get-parameter('per_page', 10));
declare variable $locale := request:get-parameter('locale', 'any');
declare variable $app-root := '/db/apps/sade-architrave';

declare variable $edition := request:get-parameter('edition', '');

(:
  Defines the endpoints for the json search api. Each function essentially
  queries an elasticsearch index for results with highlighting, pagination and
  locale filters.

  The interaction with elasticsearch (http requests) can be found in
  /modules/elastic.xqm.
:)

declare function local:search-editions-elastic() {
    let $filters := if ($locale eq 'any') then [] else [
        map {"term": map {"locale": $locale}}
    ]
    let $response := elastic:search("edition_pages", map {
        "from": ($page - 1) * $per_page,
        "size": $per_page,
        "query": map {
            "bool": map {
                "must": [
                    map {
                        "query_string": map {
                            "query": $terms
                        }
                    }
                ],
                "filter": $filters
            }
        },
        "highlight": map {
            "fragment_size": 100,
            "require_field_match": fn:false(),
            "pre_tags": ['<span class="hl">'],
            "post_tags": ['</span>'],
            "type": "plain",
            "highlight_query": map {
                "query_string": map {
                    "query": $terms
                }
            },
            "fields": map {
              "name": map {"number_of_fragments": 0},
              "search_data": map {}
            }
        }
    })
    return $response
};

declare function local:search-pages-elastic() {
    let $filters := if ($locale eq 'any') then [] else [
        map {"term": map {"locale": $locale}}
    ]
    let $response := elastic:search("wiki_pages", map {
        "from": ($page - 1) * $per_page,
        "size": $per_page,
        "query": map {
            "bool": map {
                "must": [
                    map {
                        "query_string": map {
                            "query": $terms
                        }
                    }
                ],
                "filter": $filters
            }
        },
        "highlight": map {
            "fragment_size": 100,
            "require_field_match": fn:false(),
            "pre_tags": ['<span class="hl">'],
            "post_tags": ['</span>'],
            "type": "plain",
            "highlight_query":  map {
                "query_string": map {
                    "query": $terms
                }
            },
            "fields": map {
              "search_data": map {}
            }
        }
    })
    return $response
};

declare function local:search-register-elastic($type) {
    let $fields := if ($locale eq 'de') then
        ['search_data.de']
    else if ($locale eq 'fr') then
        ['search_data.fr']
    else
        ['search_data.de', 'search_data.fr']
    let $response := elastic:search($type, map {
        "from": ($page - 1) * $per_page,
        "size": $per_page,
        "query": map {
            "query_string": map {
                "query": $terms,
                "fields": $fields
            }
        },
        "highlight": map {
            "fragment_size": 100,
            "require_field_match": fn:false(),
            "pre_tags": ['<span class="hl">'],
            "post_tags": ['</span>'],
            "type": "plain",
            "fields": map {
                "name.de": map {"number_of_fragments": 0},
                "name.fr": map {"number_of_fragments": 0},
                "search_data.de": map {},
                "search_data.fr": map {}
            }
        }
    })
    return $response
};

declare function local:error($message as xs:string, $status as xs:integer) {
    let $x := response:set-status-code($status)
    return
        map {'message': $message}
};

declare function local:addFollowingFws($node) {
    let $next := $node/following-sibling::*[1]
    
    return
        if ($next/local-name() ne 'fw') then
            ($node)
        else
            ($node, local:addFollowingFws($next))
};

declare function local:page-numbers() {
    let $uri := concat('/db/apps/sade-architrave/textgrid/data/', $edition, '.xml')
    let $doc := doc($uri)
    
    for $pb in $doc//tei:pb
    let $view := xs:integer($pb/@n)
    let $fws := local:addFollowingFws($pb)[position() > 1]
    let $noCorrFws := functx:remove-elements-deep($fws, 'tei:corr')
    let $noExpanFws := functx:remove-elements-deep($noCorrFws, 'tei:expan')

(:    let $x := util:log-system-out(' '):)
(:    let $a := util:log-system-out($pb):)
(:    let $y := util:log-system-out($noExpanFws):)
    let $drawNums := $noExpanFws[@type = 'drawNum' and not(@resp = '#Archivist')]
    let $page :=
        if (count($drawNums) > 0) then
            string($drawNums[1])
        else
            let $pageNums :=
              for $pageNum in $noExpanFws[@type = 'pageNum']
              let $all := string($pageNum)
              let $unprinted := string($pageNum/tei:supplied[@reason = 'unprinted'])
              return
                  if ($all = $unprinted) then
                      ()
                  else
                      $all
            return
                if (count($pageNums) > 0) then
                    $pageNums[1]
                else
                    ''
    let $result :=
        if ($page ne '') then
            map {
                'view': $view,
                'page': $page
            }
        else
            map {
                'view': $view
            }
            
    return $result
};

declare function local:refData() {
    let $split := tokenize($targets, '\s*,\s*')
    let $target-list := if (count($split) > 1) then $split else [$split]
    let $filters := [
        map {"terms": map {"ref_target": $target-list}},
        map {"term": map {"locale": $locale}}
    ]
    let $response := elastic:search("refs", map {
        "from": 0,
        "size": 500,
        "query": map {
            "bool": map {
                "filter": $filters
            }
        }
    })
    return $response
};

declare function local:metadata() {
    let $path := concat($config:app-root, '/textgrid/data/', $edition, '.xml')
    let $doc := doc($path)
    
    let $meta-path := concat($config:app-root, '/textgrid/meta/', $edition, '.xml')
    let $meta := doc($meta-path)
    
    let $header := $doc//tei:teiHeader
    
    return map {
        'title': local:textFor($header//tei:titleStmt/tei:title[1]),
        'subtitle': local:textFor($header//tei:titleStmt/tei:title[@type='sub'][1]),
        'lastModified': local:textFor($meta//tg:lastModified),
        'textgridUri': local:textFor($meta//tg:textgridUri),
        'author': local:textFor($header//tei:sourceDesc//tei:author),
        'lang': elastic:langFor($edition)
    }
};

declare function local:textFor($node) {
    fn:normalize-space(data($node))
};

declare function local:reindex() {
    let $indexed := elastic:setup()
    return map {
        'message': 'the data has been reindexed'
    }
};

declare function local:route() {
    if ($action eq 'search-people') then
        local:search-register-elastic('people')
    else if ($action eq 'search-works') then
        local:search-register-elastic('works')
    else if ($action eq 'search-places') then
        local:search-register-elastic('places')
    else if ($action eq 'search-editions') then
        local:search-editions-elastic()
    else if ($action eq 'search-pages') then
        local:search-pages-elastic()
    else if ($action eq 'reindex') then
        local:reindex()
    else if ($action eq 'ref-data') then
        local:refData()
    else if ($action eq 'metadata') then
        local:metadata()
        
    else if ($action eq 'page-numbers') then
        local:page-numbers()
    else
        local:error('unknown action', 400)
};

local:route()
