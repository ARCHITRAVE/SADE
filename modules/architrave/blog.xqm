xquery version "3.1";

module namespace blog="https://sade.textgrid.de/ns/blog";
declare namespace content="http://purl.org/rss/1.0/modules/content/";
declare namespace http="http://expath.org/ns/http-client";

declare option exist:serialize "method=xhtml media-type=text/html";
declare variable $blog:titleLength := 200;
declare variable $blog:descriptionLength := 120;
declare variable $blog:maxNumberItems := 3;

declare function local:getTitle($title as xs:string) {
    let $titleTooLong := if (fn:string-length($title) > $blog:titleLength) then true() else false()
    let $result := if ($titleTooLong) then substring($title,1,$blog:titleLength) || "..." else $title
    return $result
};

declare function local:getDescription($desc as xs:string) {
    let $result := local:parseCharErrors($desc)
    let $descTooLong := if (fn:string-length($result) > $blog:descriptionLength) then true() else false()
    let $result := if ($descTooLong) then substring($result,1,$blog:descriptionLength) || "..." else $result
    return $result
};

declare function blog:readBlog($node as node(), $model as map(*)) as node() {
    let $reqUrl := "https://architrave.hypotheses.org/feed"
    let $reqGet :=
      <http:request method="get">
        <http:header name="connection" value="close" />
      </http:request>

    let $news := http:send-request($reqGet,$reqUrl)/*
    let $dateTime := $news/channel/lastBuildDate

    return element div {
        for $newsItem in $news/channel/item[position() < ($blog:maxNumberItems + 1)]
            let $content := $newsItem//content:encoded
            let $content := util:parse-html( "<top>" || $content || "</top>")
            let $imgCount := count($content//img)
            let $img := ($content//img)[1]/@src
            let $space := " "
            return if ($imgCount > 0) then (
                <div class="arch-post-teaser">
                    <div class="p-md-breakout bg-light">
                        <div class="p-breakout-content">
                            <div class="arch-post-teaser">
                                <div class="p-md-breakout bg-light">
                                    <div class="p-breakout-content">
                                        <div class="row pt-6 pb-6 p-lg-6 pt-lg-7">
                                            <div class="col-3 col-lg-12">
                                                <img src="{$img}" class="d-block"/>
                                            </div>
                                            <div class="col-8 offset-1 col-lg-12 offset-lg-0">
                                                <a href="{$newsItem/link}">
                                                    <p class="font-weight-bolder">{local:getTitle($newsItem/title)}</p>
                                                </a>
                                                <p>{local:getRSSDate($newsItem/pubDate/text())}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                )
            else (
                <div class="arch-post-teaser">
                    <div class="p-md-breakout bg-light">
                        <div class="p-breakout-content">
                            <div class="arch-post-teaser">
                                <div class="p-md-breakout bg-light">
                                    <div class="p-breakout-content">
                                        <div class="row pt-6 pb-6 p-lg-6 pt-lg-7">
                                            <div class="col-8 offset-1 col-lg-12 offset-lg-0">
                                                <a href="{$newsItem/link}">
                                                    <p class="font-weight-bolder">{local:getTitle($newsItem/title)}</p>
                                                </a>
                                                <p>{local:getRSSDate($newsItem/pubDate/text())}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
    }
};

declare function local:parseCharErrors($text) {
    let $result := replace($text,"&amp;#8217;", "'")
    return $result
};

declare function local:getRSSDate($rssInput as xs:string) as xs:string {
    let $rssInput := substring-before($rssInput,":")
    let $rssInput := substring($rssInput,1,(string-length($rssInput)-2))
    return $rssInput
};
