xquery version "3.1";

module namespace registerapi="https://sade.textgrid.de/ns/registerapi";

declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace tei="http://www.tei-c.org/ns/1.0";

(:~
 : query to find a person by textgridID
 : :)
declare
    %rest:GET
    %rest:path("/get-person/{$id}")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
function registerapi:get-person($id as xs:string*) as element(response) {
    <resp>{doc("/db/apps/sade-architrave/templates/register/persons.xml")//tei:person[@xml:id='textgrid:'||$id]}</resp>
};

(:~
 : query to find a place by textgridID
 : :)
declare
    %rest:GET
    %rest:path("/get-place/{$id}")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
function registerapi:get-place($id as xs:string*) as element(response) {
    <resp>{doc("/db/apps/sade-architrave/templates/register/places.xml")//tei:place[@xml:id='textgrid:'||$id]}</resp>
};

(:~
 : query to find a work by textgridID
 : :)
declare
    %rest:GET
    %rest:path("/get-work/{$id}")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
function registerapi:get-work($id as xs:string*) as element(response) {
    <resp>{doc("/db/apps/sade-architrave/templates/register/works.xml")//tei:item[@xml:id='textgrid:'||$id]}</resp>
};

(:~
 : query to find an artist by textgridID
 : :)
declare
    %rest:GET
    %rest:path("/get-artist/{$id}")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
function registerapi:get-artist($id as xs:string*) as element(response) {
    <resp>{doc("/db/apps/sade-architrave/templates/register/persons.xml")//tei:person[@xml:id='textgrid:'||$id]//tei:persName}</resp>
};

(:~
 : query to find a location by textgridID
 : :)
declare
    %rest:GET
    %rest:path("/get-location/{$id}")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
function registerapi:get-location($id as xs:string*) as element(response) {
    <resp>{doc("/db/apps/sade-architrave/templates/register/places.xml")//tei:place[@xml:id='textgrid:'||$id]//tei:placeName[@type='current']}</resp>
};

(:~
 : query to find occurences of persons in places by textgridID
 : :)
declare
    %rest:GET
    %rest:path("/get-persons-in-places/{$id}")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
function registerapi:get-persons-in-places($id as xs:string*) as element(response) {
    let $doc := doc("/db/apps/sade-architrave/templates/register/places.xml")
    let $persons := $doc//tei:text//tei:persName[@ref='psn:textgrid:'||$id]
    return <resp>
        {for $node in $persons return
            let $ancestorNameDE := string($node/[ancestor::tei:place/tei:placeName[@xml:lang='de'][@type='current']]
                || " (" || $node/[ancestor::tei:place/tei:placeName[@xml:lang='de'][@type='historical']] || ")") => replace("( \(\))", "")
            let $ancestorNameFRA := string($node/[ancestor::tei:place/tei:placeName[@xml:lang='fra'][@type='current']]
                || " (" || $node/[ancestor::tei:place/tei:placeName[@xml:lang='fra'][@type='historical']] || ")") => replace("( \(\))", "")
            return
            element tei:persName {
                attribute ref {$node/@ref},
                attribute ancestor {$node/[ancestor::tei:place/@xml:id]},
                attribute ancestorNameCurrentDE {$ancestorNameDE},
                attribute ancestorNameCurrentFRA {$ancestorNameFRA}
                }
        }
    </resp>
};

(:~
 : query to find occurences of persons in works by textgridID
 : :)
declare
    %rest:GET
    %rest:path("/get-persons-in-works/{$id}")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
function registerapi:get-persons-in-works($id as xs:string*) as element(response) {
    let $doc := doc("/db/apps/sade-architrave/templates/register/works.xml")
    let $persons := $doc//tei:text//tei:persName[@ref='psn:textgrid:'||$id]
    return <resp>
        {for $node in $persons return
            element tei:persName {
                attribute ref {$node/@ref},
                attribute ancestor {$node/[ancestor::tei:item/@xml:id]},
                attribute ancestorNameCurrentDE {$node/[ancestor::tei:item/tei:name[@xml:lang='de']]},
                attribute ancestorNameCurrentFRA {$node/[ancestor::tei:item/tei:name[@xml:lang='fra']]}
                }
        }
    </resp>
};

(:~
 : query to find occurences of places in works by textgridID
 : :)
declare
    %rest:GET
    %rest:path("/get-places-in-works/{$id}")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
function registerapi:get-places-in-works($id as xs:string*) as element(response) {
    let $doc := doc("/db/apps/sade-architrave/templates/register/works.xml")
    let $places := $doc//tei:text//tei:placeName[@ref='plc:textgrid:'||$id]
        return <resp>
        {for $node in $places return
            element tei:placeName {
                attribute ref {$node/@ref},
                attribute ancestor {$node/[ancestor::tei:item/@xml:id]},
                attribute ancestorNameCurrentDE {$node/[ancestor::tei:item/tei:name[@xml:lang='de']]},
                attribute ancestorNameCurrentFRA {$node/[ancestor::tei:item/tei:name[@xml:lang='fra']]}
                }
        }
    </resp>
};

(:~
 : query to find occurences of places in persons by textgridID
 : :)
declare
    %rest:GET
    %rest:path("/get-places-in-persons/{$id}")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
function registerapi:get-places-in-persons($id as xs:string*) as element(response) {
    let $doc := doc("/db/apps/sade-architrave/templates/register/persons.xml")
    let $places := $doc//tei:text//tei:placeName[@ref='textgrid:'||$id]
        return <resp>
        {for $node in $places return
            element tei:placeName {
                attribute ref {$node/@ref},
                attribute ancestor {$node/[ancestor::tei:item/@xml:id]},
                attribute ancestorNameCurrentDE {$node/[ancestor::tei:item/tei:name[@xml:lang='de']]},
                attribute ancestorNameCurrentFRA {$node/[ancestor::tei:item/tei:name[@xml:lang='fra']]}
                }
        }
    </resp>
};

(:~
 : query to find occurences of places in works by textgridID
 : :)
declare
    %rest:GET
    %rest:path("/get-persons-in-text/{$text}/{$id}")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
function registerapi:get-persons-in-text($text as xs:string, $id as xs:string) as element(response) {
    let $doc :=
        try {doc("/db/apps/sade-architrave/textgrid/data/" || $text || ".xml")}
        catch * {""}
    let $personsXML := doc("/db/apps/sade-architrave/templates/register/persons.xml")
    
    let $persons := $doc//tei:persName[contains(@ref, 'psn:textgrid:'||$id)]
    return <resp>
        {for $node in $persons return
            element tei:persName {
                attribute ref {$node/@ref},
                attribute refNameDE {
                    $personsXML//tei:person[@xml:id='textgrid:'||$id]/tei:persName[@xml:lang='de']
                },
                attribute refNameFR {
                    $personsXML//tei:person[@xml:id='textgrid:'||$id]/tei:persName[@xml:lang='fra']
                },
                attribute editionID {$text},
                attribute editionPage {$node/[preceding::tei:pb[1]/@n]}
            }
        }
    </resp>
};

(:~
 : query to find occurences of places in works by textgridID
 : :)
declare
    %rest:GET
    %rest:path("/get-places-in-text/{$text}/{$id}")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
function registerapi:get-places-in-text($text as xs:string, $id as xs:string) as element(response) {
    let $doc :=
        try {doc("/db/apps/sade-architrave/textgrid/data/" || $text || ".xml")}
        catch * {""}
    let $placesXML := doc("/db/apps/sade-architrave/templates/register/places.xml")

    let $places := $doc//tei:placeName[contains(@ref, 'plc:textgrid:'||$id)]
    let $geogNames := $doc//tei:geogName[contains(@ref, 'plc:textgrid:'||$id)]
    return <resp>
        {for $node in $places return
            element tei:placeName {
                attribute ref {$node/@ref},
                attribute refNameDE {
                    $placesXML//tei:place[@xml:id='textgrid:'||$id]/tei:placeName[@xml:lang='de']
                },
                attribute refNameFR {
                    $placesXML//tei:place[@xml:id='textgrid:'||$id]/tei:placeName[@xml:lang='fra']
                },
                attribute editionID {$text},
                attribute editionPage {$node/[preceding::tei:pb[1]/@n]}
            }
        }
        {for $node in $geogNames return
            element tei:placeName {
                attribute ref {$node/@ref},
                attribute refNameDE {
                    $placesXML//tei:place[@xml:id='textgrid:'||$id]/tei:placeName[@xml:lang='de']
                },
                attribute refNameFR {
                    $placesXML//tei:place[@xml:id='textgrid:'||$id]/tei:placeName[@xml:lang='fra']
                },
                attribute editionID {$text},
                attribute editionPage {$node/[preceding::tei:pb[1]/@n]}
            }
        }
    </resp>
};

(:~
 : query to find occurences of places in works by textgridID
 : :)
declare
    %rest:GET
    %rest:path("/get-works-in-text/{$text}/{$id}")
    %rest:produces("application/json")
    %output:media-type("application/json")
    %output:method("json")
function registerapi:get-works-in-text($text as xs:string, $id as xs:string) as element(response) {
    let $doc :=
        try {doc("/db/apps/sade-architrave/textgrid/data/" || $text || ".xml")}
        catch * {""}
    let $worksXML := doc("/db/apps/sade-architrave/templates/register/works.xml")

    let $works := $doc//tei:rs[contains(@ref, 'wrk:textgrid:'||$id)]
    return <resp>
        {for $node in $works return
            element tei:item {
                attribute ref {$node/@ref},
                attribute refNameDE {
                    $worksXML//tei:item[@xml:id='textgrid:'||$id]/tei:name[@xml:lang='de']
                },
                attribute refNameFR {
                    $worksXML//tei:item[@xml:id='textgrid:'||$id]/tei:name[@xml:lang='fra']
                },
                attribute editionID {$text},
                attribute editionPage {$node/[preceding::tei:pb[1]/@n]}
            }
        }
    </resp>
};
