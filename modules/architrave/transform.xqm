xquery version "3.1";

module namespace transformXML="https://sade.textgrid.de/ns/transform";
import module namespace code-view="http://sepia.io/code-viewer/main";

declare boundary-space strip;

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace xhtml="http://www.w3.org/1999/xhtml";
declare namespace tg="http://textgrid.info/namespaces/metadata/core/2010";
declare namespace functx = "http://www.functx.com";
declare default element namespace "http://www.w3.org/1999/xhtml";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:indent "no";

declare function transformXML:importEdition($tguri as xs:string) {
(:getEdition itself and store in html pages:)
(:    transformXML:storeInPages('34zmq','34zmq'):)

(:getManifest?:)


(:getEdition and prepare xml files in pages? Module&Function still missing:)

let $output := 'done'
return $output
};

declare function local:milestone-chunk-ns(
  $ms1 as element(),
  $ms2 as element(),
  $node as node()*
) as node()*
{
  typeswitch ($node)
    case element() return
      if ($node is $ms1) then $node
      else if ( some $n in $node/descendant::* satisfies ($n is $ms1 or $n is $ms2) )
      then
        (: element { name($node) } :)
           element {QName (namespace-uri($node), name($node))}
                { for $i in ( $node/node() | $node/@* )
                  return local:milestone-chunk-ns($ms1, $ms2, $i) }
      else if ( $node >> $ms1 and $node << $ms2 ) then $node
      else ()
    case attribute() return $node (: will never match attributes outside non-returned elements :)
    default return
      if ( $node >> $ms1 and $node << $ms2 ) then $node
      else ()
};

declare function local:milestone-chunk-ns-last(
  $ms1 as element(),
  $ms2 as element(),
  $node as node()*
) as node()*
{
  typeswitch ($node)
    case element() return
      if ($node is $ms1) then $node
      else if ( some $n in $node/descendant::* satisfies ($n is $ms1 or $n is $ms2) )
      then
        (: element { name($node) } :)
           element {QName (namespace-uri($node), name($node))}
                { for $i in ( $node/node() | $node/@* )
                  return local:milestone-chunk-ns-last($ms1, $ms2, $i) }
      else if ( $node >> $ms1 and $node << $ms2 ) then ($node)
      else if ($node is $ms2) then ( $ms2 )
      else ()
    case attribute() return $node (: will never match attributes outside non-returned elements :)
    default return 
(:      let $x := util:log-system-out($node):)
(:      let $y := util:log-system-out($ms1):)
(:      let $z := util:log-system-out($ms2):)
      let $x := ()
      return
      if ( $node >> $ms1 and $node << $ms2 ) then $node
      else ()
};

declare function local:countPB($length) {
    let $pbNumbers := for $i in 1 to $length
    return $i

    return string-join($pbNumbers,',')
};

declare function local:countFW($length,$body) {
    let $fwNumbers := for $i in 1 to $length
    return $body//tei:pb[@n=$i]/following::tei:fw[1]/node()

    return string-join($fwNumbers,',')
};

declare function transformXML:cacheFullXmlAndMetadata($tgURI as xs:string) {
    let $doc-name := $tgURI || '.xml'
    let $tglab-base-uri := 'https://textgridlab.org/1.0/tgcrud-public/rest/textgrid:'
    let $target-base-path := '/db/apps/sade-architrave/textgrid/'
    
    let $doc := doc($tglab-base-uri || $tgURI || '/data')
    let $doc-meta := doc($tglab-base-uri || $tgURI || '/metadata')
    
    let $store-result := xmldb:store($target-base-path || '/data', $tgURI || '.xml', $doc, "text/xml")
    let $store-meta-result := xmldb:store($target-base-path || '/meta', $tgURI || '.xml', $doc-meta, "text/xml")
    
    return true()
};

declare function transformXML:cacheAllEditions() {
    let $result := (
        (: de :)
        transformXML:cacheFullXmlAndMetadata('34zmq'), (: harrach :)
        transformXML:cacheFullXmlAndMetadata('34zs7'), (: sturm :)
        transformXML:cacheFullXmlAndMetadata('3ptwg'), (: corfey :)
        transformXML:cacheFullXmlAndMetadata('3qr4f'), (: neumann :)
        transformXML:cacheFullXmlAndMetadata('34znb'), (: pitzler :)
        transformXML:cacheFullXmlAndMetadata('3c0m2'), (: knesebeck :)
        
        (: fr :)
        transformXML:cacheFullXmlAndMetadata('3czfj'), (: harrach :)
        transformXML:cacheFullXmlAndMetadata('3q4rq'), (: sturm :)
        transformXML:cacheFullXmlAndMetadata('3r0fv'), (: corfey :)
        transformXML:cacheFullXmlAndMetadata('3r3nn'), (: neumann :)
        transformXML:cacheFullXmlAndMetadata('350mg'), (: pitzler :)
        transformXML:cacheFullXmlAndMetadata('3czn9')  (: knesebeck :)
    )
    
    return $result
};

declare function transformXML:importFromTGRep($tgURI as xs:string, $filenameOutput as xs:string) {
    let $login := xmldb:login("/db", "admin", "" )
    let $pathInput := 'https://textgridlab.org/1.0/tgcrud-public/rest/textgrid:'
    let $pathInputSuffix := '/data'
    let $pathInputSuffixForMetadata := '/metadata'
    let $doc := doc($pathInput || $tgURI || $pathInputSuffix)
    let $docForHeader := doc($pathInput || $tgURI || $pathInputSuffix)
    let $docForMetadata := doc($pathInput || $tgURI || $pathInputSuffixForMetadata)
    let $pathOutput := '/db/apps/sade-architrave/templates/'|| $filenameOutput

    let $header := transformXML:magicForHeader($docForHeader//tei:TEI/*)
    let $metadata := local:magicForMetadata($docForMetadata/*)

    let $input := $doc//tei:TEI/tei:text//tei:body
    let $counterPages := count($input//tei:pb)
    let $pbNumbers as xs:string := local:countPB($counterPages)
    let $fwNumbers as xs:string := local:countFW($counterPages,$input)

    let $tgURIshort := substring-before($tgURI,'.')
    let $storeInDB := xmldb:store('/db/apps/sade-architrave/textgrid/data/', $tgURIshort || '.xml' , $input )

    for $i in 1 to $counterPages
            return
                if ($i < $counterPages) then (
                    let $first := $i
                    let $next := $i+1
                    let $output := local:milestone-chunk-ns($input//tei:pb[@n=string($first)],$input//tei:pb[@n=string($next)],$input)
                    let $outputXML := code-view:main($output)
                    let $output := local:magic($output, "")
                    let $filenameOutputPage := $filenameOutput || '_p' || string($i)
                    let $output := <div>{$metadata}<div id="tei-header"><span style="display:none" id="pbNumbers">{$pbNumbers}</span><span style="display:none" id="fwNumbers">{$fwNumbers}</span>{$header}</div>{$output}</div>
                    let $store := xmldb:store($pathOutput, $filenameOutputPage || '.html' , $output )
                    let $store := xmldb:store($pathOutput, $filenameOutputPage || '_xml.html' , $outputXML )
                    return $output
                )
                else (
                    let $from := $input//tei:pb[@n=string($counterPages)]
(:                    let $to := ($input//tei:pb[@n="1"]/following-sibling::*[last()]):)
                    let $to := if ($tgURI = '3qr4f.2') then
                            let $elements := $input//tei:closer
                            let $amount := count($elements)
                            return $elements[$amount]
                        else if ($tgURI = '3r3nn.3') then
                            let $elements := $input//tei:closer
                            let $amount := count($elements)
                            return $elements[$amount]
                        else if ($tgURI = '3r0fv') then
                            let $elements := $input//tei:ab
                            let $amount := count($elements)
                            return $elements[$amount]
                        else if ($tgURI = '34zs7.3') then
                            let $x := 12 (: makes the editor syntax error go away :)
                            return $from
                        else if ($tgURI = '3q4rq.3') then
                            let $x := 12 (: makes the editor syntax error go away :)
                            return $from
                        else
                            $input//*[last()]
                    let $output := local:milestone-chunk-ns-last($from, $to, $input)
                            
                    (: let $output := local:milestone-chunk-ns-last($input//tei:pb[@n=string($counterPages)], ($input//tei:pb[@n="1"]/following-sibling::*[last()]),$input) :)
                    let $outputXML := code-view:main($output)
                    let $output := local:magic($output, "")
                    let $filenameOutputPage := $filenameOutput || '_p' || string($i)
                    let $output := <div>{$metadata}<div id="tei-header"><span style="display:none" id="pbNumbers">{$pbNumbers}</span><span style="display:none" id="fwNumbers">{$fwNumbers}</span>{$header}</div>{$output}</div>
                    let $store := xmldb:store($pathOutput, $filenameOutputPage || '.html' , $output )
                    let $store := xmldb:store($pathOutput, $filenameOutputPage || '_xml.html' , $outputXML )
                    return $output
                )
};

declare function transformXML:importFromTGLab($tgURI as xs:string) {
    let $login := xmldb:login("/db", "admin", "" )
    let $pathInput := '/db/apps/sade-architrave/textgrid/data/'
    let $pathInputMetadata := '/db/apps/sade-architrave/textgrid/meta/'
    let $filenameOutput := $tgURI
    let $pathInputSuffix := '.xml'
    let $pathInputSuffixForMetadata := '/metadata'
    let $doc := doc($pathInput || $tgURI || $pathInputSuffix)
    let $docForHeader := doc($pathInput || $tgURI || $pathInputSuffix)
    let $docForMetadata := doc($pathInputMetadata || $tgURI || $pathInputSuffix)
    let $pathOutput := '/db/apps/sade-architrave/templates/'|| $filenameOutput

    let $header := transformXML:magicForHeader($docForHeader//tei:TEI/*)
    let $metadata := local:magicForMetadata($docForMetadata/*)

    let $input := $doc//tei:TEI/tei:text//tei:body
    let $input := local:special-magic($input, $tgURI)
    (: let $x := util:log-system-out($input) :)
    let $counterPages := count($input//tei:pb)
    let $pbNumbers as xs:string := local:countPB($counterPages)
    let $fwNumbers as xs:string := local:countFW($counterPages,$input)

    for $i in 1 to $counterPages
            return
                if ($i < $counterPages) then (
                    (: let $x := util:log-system-out($i) :)
                    let $first := $i
                    let $next := $i+1
                    let $from := $input//tei:pb[@n=string($first)]
                    let $to := $input//tei:pb[@n=string($next)]
                    let $output := local:milestone-chunk-ns($from, $to, $input)
                    let $outputXML := code-view:main($output)
                    let $output := local:magic($output, "")
                    let $filenameOutputPage := $filenameOutput || '_p' || string($i)
                    let $output := <div>{$metadata}<div id="tei-header"><span style="display:none" id="pbNumbers">{$pbNumbers}</span><span style="display:none" id="fwNumbers">{$fwNumbers}</span>{$header}</div>{$output}</div>
                    let $store := xmldb:store($pathOutput, $filenameOutputPage || '.html' , $output)
                    let $store := xmldb:store($pathOutput, $filenameOutputPage || '_xml.html' , $outputXML )
                    return $output
                )
                else (
                    let $from := $input//tei:pb[@n=string($counterPages)]
(:                    let $to := ($input//tei:pb[@n="1"]/following-sibling::*[last()]):)
                    let $to := if ($tgURI = '3qr4f') then
                            let $elements := $input//tei:date
                            let $amount := count($elements)
                            return $elements[$amount]
                        else if ($tgURI = '3r0fv') then
                            let $elements := $input//tei:ab
                            let $amount := count($elements)
                            return $elements[$amount]
                        else
                            $input//*[last()]
                    let $output := local:milestone-chunk-ns-last($from, $to, $input)
                    let $outputXML := code-view:main($output)
                    let $output := local:magic($output, "")
                    let $filenameOutputPage := $filenameOutput || '_p' || string($i)
                    let $output := <div>{$metadata}<div id="tei-header"><span style="display:none" id="pbNumbers">{$pbNumbers}</span><span style="display:none" id="fwNumbers">{$fwNumbers}</span>{$header}</div>{$output}</div>
                    let $store := xmldb:store($pathOutput, $filenameOutputPage || '.html' , $output )
                    let $store := xmldb:store($pathOutput, $filenameOutputPage || '_xml.html' , $outputXML )
                    return $output
                )
};

declare function transformXML:storeInPages($filenameInput as xs:string, $filenameOutput as xs:string) {
    let $login := xmldb:login("/db", "admin", "" )
    let $pathInput := '/db/apps/sade-architrave/textgrid/data'
    let $pathOutput := '/db/apps/sade-architrave/templates/'||$filenameInput
    let $doc := doc($pathInput||'/'||$filenameInput||'.xml')

    let $doc3 := doc($pathInput||'/'||$filenameInput||'.xml')
    let $header := transformXML:magicForHeader($doc3//tei:TEI/*)

    let $pathInput := '/db/apps/sade-architrave/textgrid/meta'
    let $doc2 := doc($pathInput||'/'||$filenameInput||'.xml')
    let $metadata := local:magicForMetadata($doc2/*)

    let $input := $doc//tei:TEI/tei:text//tei:body
    let $counterPages := count($input//tei:pb)
    let $pbNumbers := ''
    let $fwNumbers := ''

    let $pbNumbers := local:countPB($counterPages)
    let $fwNumbers := local:countFW($counterPages,$input)

    for $i in 1 to $counterPages
        return
            if ($i < $counterPages) then (
                let $first := $i
                let $next := $i+1
                let $output := local:milestone-chunk-ns($input//tei:pb[@n=string($first)],$input//tei:pb[@n=string($next)],$input)
                let $outputXML := code-view:main($output)
                let $output := local:magic($output, "")
                let $filenameOutputPage := $filenameOutput || '_p' || string($i)
                let $output := <div>{$metadata}<div id="tei-header"><span style="display:none" id="pbNumbers">{$pbNumbers}</span><span style="display:none" id="fwNumbers">{$fwNumbers}</span>{$header}</div>{$output}</div>
                let $store := xmldb:store($pathOutput, $filenameOutputPage || '.html' , $output )
                let $store := xmldb:store($pathOutput, $filenameOutputPage || '_xml.html' , $outputXML )
                return $output
            )
            else (
                let $output := local:milestone-chunk-ns-last($input//tei:pb[@n=string($counterPages)], ($input//tei:pb[@n="1"]/following-sibling::*[last()]),$input)
                let $outputXML := code-view:main($output)
                let $output := local:magic($output, "")
                let $filenameOutputPage := $filenameOutput || '_p' || string($i)
                let $output := <div>{$metadata}<div id="tei-header"><span style="display:none" id="pbNumbers">{$pbNumbers}</span><span style="display:none" id="fwNumbers">{$fwNumbers}</span>{$header}</div>{$output}</div>
                let $store := xmldb:store($pathOutput, $filenameOutputPage || '.html' , $output )
                let $store := xmldb:store($pathOutput, $filenameOutputPage || '_xml.html' , $outputXML )
                return $output
            )
};

declare function transformXML:store($filenameInput as xs:string, $filenameOutput as xs:string) {

    let $pathInput := '/db/apps/sade-architrave/textgrid/data'
    let $pathOutput := '/db/apps/sade-architrave/templates'
    let $doc := doc($pathInput||'/'||$filenameInput||'.xml')
    let $xml := local:magic($doc/*, "")

    let $numberOfPages := $doc//tei:pb

    let $pathInput := '/db/apps/sade-architrave/textgrid/meta'
    let $doc2 := doc($pathInput||'/'||$filenameInput||'.xml')
    let $metadata := local:magicForMetadata($doc2/*)

    let $result := <div data-template="templates:surround" data-template-with="templates/edition_surround.html" data-template-at="edition-container">{$metadata}{$xml}</div>

    let $login := xmldb:login("/db", "admin", "" )
    let $store := xmldb:store($pathOutput, $filenameOutput || '.html' , $result )

    return $result
};

declare function local:magicForMetadata($nodes) {
    for $node in $nodes
return
typeswitch ( $node )
    case element( tg:MetadataContainerType ) return ( local:magicForMetadata($node/node()) )
    case element( tg:object ) return ( local:magicForMetadata($node/node()) )
    case element( tg:generic ) return ( local:magicForMetadata($node/node()) )
    case element( tg:generated ) return ( local:magicForMetadata($node/node()) )
    case element( tg:created ) return
        <span style="display:none" id="tei-meta-dateCreated">{ string($node) }</span>
    case element( tg:lastModified ) return
        <span style="display:none" id="tei-meta-dateModified">{ string($node) }</span>
    case element( tg:textgridUri ) return
        <span style="display:none" id="tei-meta-textGridURI">{ string($node) }</span>
    case element( tg:pid ) return
        <span style="display:none" id="tei-meta-pid">{ string($node) }</span>
    default return ()
};

declare function transformXML:toHTML($file as xs:string) {

    let $doc := doc($file)
    let $result := local:magic($doc/*, "")

    return $result
};
declare function transformXML:MetadataToHTML($file as xs:string) {

    let $doc := doc($file)
    let $result := local:magicForMetadata($doc/*)

    return $result
};

declare function transformXML:magicForHeader($nodes) {
for $node in $nodes
return
typeswitch ( $node )
    case element( tei:teiHeader ) return ( transformXML:magicForHeader($node/node()) )
    case element( tei:fileDesc ) return ( transformXML:magicForHeader($node/node()) )
    case element( tei:titleStmt ) return ( transformXML:magicForHeader($node/node()) )
    case element( tei:sourceDesc ) return ( transformXML:magicForHeader($node/node()) )
    case element( tei:msDesc ) return ( transformXML:magicForHeader($node/node()) )
    case element( tei:msContents ) return ( transformXML:magicForHeader($node/node()) )
    case element( tei:msItem ) return ( transformXML:magicForHeader($node/node()) )
    case element( tei:title ) return
        if (string($node/@type) = '') then (
        <h4 id="tei-title-main-{string($node/@xml:lang)}" style="display:none" class="maintitle lang-{string($node/@xml:lang)} type-{string($node/@type)}" type="{string($node/@type)}" xml:lang="{string($node/@xml:lang)}">{ string($node) }</h4> )
        else (
        <h4 id="tei-title-{string($node/@type)}-{string($node/@xml:lang)}" style="display:none" class="maintitle lang-{string($node/@xml:lang)} type-{string($node/@type)}" type="{string($node/@type)}" xml:lang="{string($node/@xml:lang)}">{ string($node) }</h4>
        )
    case element( tei:author ) return
        <span style="display:none" id="tei-author">{ string($node) }</span>
    default return ()
};

declare function local:magic-wrapper($nodes, $mode) {
    for $node in $nodes return
        (:
        if ($node/preceding-sibling::tei:cb and $mode != "column") then
            ()
        else
            :)
            local:magic($node, $mode)
};

declare function local:magic($nodes, $mode) {
for $node in $nodes
return
    try 
    {
        typeswitch ( $node )
            case element( tei:TEI ) return
                <div class="{$node/local-name()}">
                    { local:magic-wrapper($node/node(), $mode) }
                </div>
            case element( tei:text ) return
                <div class="{$node/local-name()}">
                    { local:magic-wrapper($node/node(), $mode) }
                </div>
            case element( tei:body ) return
                <div class="{$node/local-name()}">
                    { local:magic-wrapper($node/node(), $mode) }
                </div>
            case element ( tei:div ) return
                <div class="{$node/local-name()}" type="{string($node/@type)}">
                    { local:magic-wrapper($node/node(), $mode) }
                </div>
            case element ( tei:p ) return
                <p>{ local:magic-wrapper($node/node(), $mode) }</p>
            case element ( tei:ab ) return
                (: let $x := util:log-system-out($node) :)
                let $rend := $node/@rend
                return
                    <ab rend="{$rend}">{ local:magic-wrapper($node/node(), $mode) }</ab>
            case element ( tei:pb ) return
                <span id="page{string($node/@n)}" class="pb" facs="{ string($node/@facs) }">{ string($node/@n) }</span>
            case element ( tei:fw ) return $node
            case element ( tei:choice ) return $node
                (:
                if ($node/tei:abbr) then (
                    <abbr class="abbr-yes" style="display:none" title="{$node/tei:expan}">{ local:magic-wrapper($node/tei:abbr, $mode) }</abbr>,
                    <abbr class="abbr-no" style="display:inline" title="{$node/tei:abbr}">{ local:magic-wrapper($node/tei:expan, $mode) }</abbr>
                )
                else if ($node/tei:sic) then (
                    <abbr class="corr-yes" style="display:inline" title="{$node/tei:corr}">{ local:magic-wrapper($node/tei:sic, $mode) }</abbr>,
                    <abbr class="corr-no" style="display:none" title="{$node/tei:sic}">{ local:magic-wrapper($node/tei:corr, $mode) }</abbr>
                )
                else
                    ()
                :)
            case element ( tei:date ) return $node
            case element ( tei:lb ) return $node
            case element ( tei:hi ) return $node
            case element ( tei:supplied ) return $node
            case element ( tei:placeName ) return $node
            case element ( tei:geogName ) return $node
            case element ( tei:persName ) return $node
            case element ( tei:rs ) return $node
            case element ( tei:del ) return $node
                (: <span class="del"><strike>{string($node)}</strike></span> :)
            case element ( tei:unclear ) return
                <span class="unclear">{string($node)}[?]</span>
            case element ( tei:space ) return
                $node
            case element ( tei:num ) return $node
                (:
                if (string($node/@type) = 'fraction') then (
                    if (string($node/@rend) = 'horizontal') then (
                        <span class="tei-fraction-horizontal">
                            <math xmlns="http://www.w3.org/1998/Math/MathML" display="inline">
                            <mrow>
                                <mfrac>
                                <mrow>
                                    <mn>{fn:substring-before(string($node),'/')}</mn>
                                </mrow>
                                <mrow>
                                    <mn>{fn:substring-after(string($node),'/')}</mn>
                                </mrow>
                                </mfrac>
                            </mrow>
                            </math>
                        </span>
                    )
                    else if (string($node/@rend) = 'oblique') then (
                        local:magic-wrapper($node/node(), $mode)
                    )
                    else if (string($node/@rend) = 'diagonal') then (
                        <span class="tei-fraction-oblique">
                            <sup>{fn:substring-before(string($node),'/')}</sup>/<sub>{fn:substring-after(string($node),'/')}</sub>
                        </span>
                    )
                    else ()
                )
                else if ((string($node/@type) = 'fractionLike') and (string($node/@rend) = 'horizontal')) then (
                    $node 
                    <span class="abbr-yes tei-fraction-horizontal" style="display:inline">
                        <math xmlns="http://www.w3.org/1998/Math/MathML" display="inline">
                        <mrow>
                            <mfrac>
                            <mrow>
                                <mn>{fn:substring-before($node/text(),'/')}</mn>
                            </mrow>
                            <mrow>
                                <mn>{fn:substring-after($node/text(),'/')}</mn>
                            </mrow>
                            </mfrac>
                        </mrow>
                        </math>
                    </span>,
                    <span class="abbr-no tei-fraction-horizontal" style="display:none">
                        <math xmlns="http://www.w3.org/1998/Math/MathML" display="inline">
                        <mrow>
                            <mfrac>
                            <mrow>
                                <mn>{string($node/tei:choice/tei:expan/node())}</mn>
                            </mrow>
                            <mrow>
                                <mn>{fn:substring-after($node/text(),'/')}</mn>
                            </mrow>
                            </mfrac>
                        </mrow>
                        </math>
                    </span>
                ) else if ($node/@rend = "center") then
                    $node
                else ( local:magic-wrapper($node/node(), $mode) )
                :)
            case element ( tei:q ) return $node
(:                <span class="tei-quote">{string($node)}</span>:)
            case element ( tei:add ) return $node
            case element ( tei:teiHeader ) return (transformXML:magicForHeader($node), $mode)
            case element ( tei:subst ) return $node
            case text() return
                if (matches($node, "&#11840;")) then (
                    <span>{fn:substring-before($node,"&#11840;")}</span>,
                    <span class="double-hyphen">&#61;</span>,
                    <span>{fn:substring-after($node,"&#11840;")}</span>
                ) else $node
            case element ( tei:note ) return $node
            case element ( tei:table ) return 
                local:parse-table($node)

            case element ( tei:metamark ) return $node
            case element ( tei:list ) return $node
            case element ( tei:item ) return $node
            case element ( tei:label ) return $node
            case element ( tei:head ) return $node
            case element ( tei:w ) return $node
            case element ( tei:figure) return $node
            case element ( tei:figDesc ) return $node
            case element ( tei:cb ) return $node
                (:
                if ($node/@* = ($node/parent::*//tei:cb/@*)[1]) then
                    local:parse-column($node)
                else ()
                :)

            default return $node
(:                local:magic-wrapper($node/node(), $mode)}:)
            }
    catch * {
        element xhtml:span {
            attribute class {"error"},
            ($err:description)
        }
    }
};

(: special changes for specfic xml docs :)
declare function local:special-magic($node, $tgURI) {
    let $output :=
        if ($tgURI = '34zs7' or $tgURI = '3q4rq') then
            <tei:body>
                {$node/tei:div[@type='front']/node()}
                {$node/tei:div[@type='text']/node()}
                {$node/tei:div[@type='back']/node()}
            </tei:body>
        else
            $node
    
    return $output
};

declare function local:parse-table($table as element(tei:table)) as element(div) {
    let $head := $table/tei:head
    let $foot := $table/tei:trailer

    return
        element div {
            attribute class { "table-container" },
            element table {
                if ( $head ) then
                    element caption {
                        attribute rend { string($head/@rend) },
                        local:magic-wrapper($head/node(), "")
                    }
                else (),
                element tbody { 
                    let $rows := $table/tei:row
                    for $row in $rows 
                    let $cells := $row/tei:cell
                    return
                        element tr {
                            for $cell in $cells 
                                return <td colspan="{$cell/@cols}">{local:magic-wrapper($cell/node(), "")}</td>
                        }    
                }
            },
            if ( $foot ) then local:magic-wrapper($foot/node(), "") else ()
        }
};

declare function local:parse-column($node) {
    let $number := count($node/following-sibling::tei:cb) + 1
    return
        element div {
            attribute class { "column-wrapper" },
            (for $i in (1 to $number)
            order by $i
            return
                element div {
                    attribute class { "column" },
                    local:magic-wrapper($node/following-sibling::*[count(preceding-sibling::tei:cb) = $i], "column")
                }
            )
        }
};

(: recursively get all following tei:w elements :)
declare function local:getConsecutiveW($node) {
    let $next := $node/following-sibling::*[1]
    return
        if ($next/local-name() = 'w') then
            if (string($next/@join) = 'both') then
                ($node, local:getConsecutiveW($next))
            else
                (: -> left :)
                ($node, $next)
        else
            (: -> not a tei:w :)
            let $nested := $next/*[1]
            return
                if ($nested/local-name() = 'w') then
                    (: -> nested node is tei:w :)
                    if (string($nested/@join) = 'both') then
                        ($nested, local:getConsecutiveW($nested))
                    else
                        (: -> left :)
                        ($node, $nested)
                else
                    ()
};
