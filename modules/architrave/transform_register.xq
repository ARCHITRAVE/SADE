xquery version "3.1";
(:~
 : This skript is launched on each click on a button of the register.html
 : @author Florian Barth
:)

declare namespace tei = "http://www.tei-c.org/ns/1.0";
declare namespace xhtml="http://www.w3.org/1999/xhtml";


declare option exist:serialize "method=xhtml media-type=text/html";
(:  
declare variable $button_parameter := "persons";
declare variable $language_parameter := "de";
declare variable $letter_value := "button_unnamed";
declare variable $letter_text := "unnamed";
:)
declare variable $button_parameter := request:get-parameter("button_parameter", ());
declare variable $language_parameter := request:get-parameter("language_parameter", ());
declare variable $letter_value := request:get-parameter("letter_value_parameter", ());
declare variable $letter_text := request:get-parameter("letter_text_parameter", ());

declare variable $language :=
if ($language_parameter = "de") then "de"
else if  ($language_parameter = "fr") then "fra"
else ();

(:  
declare variable $Text4notIdentified :=
if ($button_parameter = "persons") then <span data-template="lang:translate" data-template-content="unidentifiedPerson"/>
else if ($button_parameter = "places") then <span data-template="lang:translate" data-template-content="unidentifiedPlace"/>
else if ($button_parameter = "works") then <span data-template="lang:translate" data-template-content="unidentifiedWork"/>
else();
:)


declare variable $entityClass :=
if ($button_parameter = "persons") then "persName"
else if ($button_parameter = "places") then "placeName"
else if ($button_parameter = "works") then "ArtWork"
else();

declare variable $entityAbbr :=
if ($button_parameter = "persons") then "psn"
else if ($button_parameter = "places") then "plc"
else if ($button_parameter = "works") then "wrk"
else();

declare variable $openEntity :=
if ($button_parameter = "persons") then "openPersName"
else if ($button_parameter = "places") then "openPlaceName"
else if ($button_parameter = "works") then "openArtWork"
else();


declare variable $entityElements :=
if ($button_parameter = "persons") then doc("/db/apps/sade-architrave/templates/register/persons.xml")//tei:person
else if ($button_parameter = "places") then doc("/db/apps/sade-architrave/templates/register/places.xml")//tei:place
else if ($button_parameter = "works") then doc("/db/apps/sade-architrave/templates/register/works.xml")//tei:item
else();

declare variable $EntityNames :=
if ($button_parameter = "persons") then $entityElements//tei:persName[@xml:lang=$language]
else if ($button_parameter = "places") then $entityElements//tei:placeName[@xml:lang=$language and @type='current']
else if ($button_parameter = "works") then $entityElements/tei:name[@xml:lang=$language]
else();

declare variable $letter_query :=
if ($letter_value = "button_other") then "^[^A-Za-zÄäÖöÜüÉéÎîÈè]"
else "^[" || $letter_value || "]";

declare function local:get-unidentified-label($item) {
    if ($button_parameter = 'persons') then
        let $names := $item/tei:persName[@xml:lang=$language]
        let $occupations := $item/tei:occupation[@xml:lang=$language]
        return
            if ($names[1]) then
                $names[1]
            else
                $occupations[1]
    else if ($button_parameter = 'works') then
        $item/tei:name[@xml:lang=$language]
    else if ($button_parameter = 'places') then
        $item/tei:placeName[@type='current' and @xml:lang=$language]
    else ''
};

declare function local:get-label($item) {
    if ($button_parameter = 'persons') then
        let $names := $item/tei:persName[@xml:lang=$language]
        return $names[1]
    else if ($button_parameter = 'works') then
        let $names := $item/tei:name[@xml:lang=$language]
        return $names[1]
    else if ($button_parameter = 'places') then
        let $names := $item/tei:placeName[@xml:lang=$language and @type='current']
        return $names[1]
    else ''
};

declare function local:get-distinct-name($item) {
    if ($button_parameter = 'works') then
        let $persons := $item/tei:note[@type='artist']/tei:persName
        let $strings := for $person in $persons
            let $name_01 := replace(data($person), ' \(http[^\)]+\)', '')
            let $name_02 := replace($name_01, '&lt;|&gt;', '')
            let $name_03 := replace($name_02, ' \(psn:[^\)]+\)', '')
            return $name_03
        let $separator := if ($language = 'de') then '; ' else '&#xa0;; '
        return string-join($strings, $separator)
    else ''
};

<div id="register_ul_css">
    
<ul>
<h2>{string($letter_text)}</h2>

{
if ($letter_value = "button_unnamed") then
    for $entity in $entityElements[@type='unidentified']
    let $label := local:get-unidentified-label($entity)
    let $normalized-label := normalize-unicode($label, 'NFKD') (: this is actually case-folding :)
    order by $normalized-label collation 'http://exist-db.org/collation?lang=fr' (: it seems we can get away with the French collation for both languages :)
    return
        element xhtml:li
        {
            element xhtml:a
            {
                attribute class {$entityClass},
                attribute href {"#/"},
                attribute onclick {$openEntity || "('" || $entityAbbr || ":" || data($entity/@xml:id) || "')"},
                attribute textgriduri {"('" || $entityAbbr || ":" || data($entity/@xml:id) || "')"},
                attribute distinct-name { local:get-distinct-name($entity) },
                (:  $Text4notIdentified :)
                $label
            }
        }

else
    (: the collation argument just allows literals (!), so ... :)
    if ($language_parameter = 'de') then
        for $entity at $pos in $entityElements[not(@type='unidentified')]
        let $tgUri := data($entity/@xml:id)
        let $label := local:get-label($entity)
        where matches(string($label), $letter_query) 
        order by $label/text() collation 'http://exist-db.org/collation?lang=de'
        return
            element xhtml:li
            {
                element xhtml:a
                {
                    attribute class {$entityClass},
                    attribute href {"#/"},
                    attribute onclick {$openEntity || "('" || $entityAbbr || ":" || $tgUri || "')"},
                    attribute textgriduri {"('" || $entityAbbr || ":" || $tgUri || "')"},
                    attribute distinct-name { local:get-distinct-name($entity) },
                    $label 
                }
            }
    else
        for $entity at $pos in $entityElements[not(@type='unidentified')]
        let $tgUri := data($entity/@xml:id)
        let $label := local:get-label($entity)
        where matches(string($label), $letter_query) 
        order by $label/text() collation 'http://exist-db.org/collation?lang=fr'
        return
            element xhtml:li
            {
                element xhtml:a
                {
                    attribute class {$entityClass},
                    attribute href {"#/"},
                    attribute onclick {$openEntity || "('" || $entityAbbr || ":" || $tgUri || "')"},
                    attribute textgriduri {"('" || $entityAbbr || ":" || $tgUri || "')"},
                    attribute distinct-name { local:get-distinct-name($entity) },
                    $label 
                }
            }
}
</ul>
</div>