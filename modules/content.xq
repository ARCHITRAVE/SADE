xquery version "3.1";

declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
import module namespace functx="http://www.functx.com";

declare boundary-space preserve;

declare option output:method "html5";
declare option output:media-type "application/xml";
declare option output:indent "no";

declare variable $edition := request:get-parameter('edition', '');
declare variable $page := xs:integer(request:get-parameter('page', 1));
declare variable $xml := request:get-parameter('xml', 'no');

declare function local:edition-page() {
    let $uri := if ($xml eq 'no') then
        concat('/db/apps/sade-architrave/templates/', $edition, '/', $edition, '_p', $page, '.html')
    else 
        concat('/db/apps/sade-architrave/templates/', $edition, '/', $edition, '_p', $page, '_xml.html')
        
    let $doc := doc($uri)
    return $doc
};

functx:change-element-ns-deep(local:edition-page(), "", '')
