xquery version "3.1";

module namespace elastic="http://elastic.io";

import module namespace config="https://sade.textgrid.de/ns/config" at "config.xqm";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace xhtml="http://www.w3.org/1999/xhtml";
declare namespace http="http://expath.org/ns/http-client";

declare namespace elastic="http://elastic.io";

declare variable $elastic:uri := 'http://127.0.0.1:9200';
declare variable $elastic:prefix := 'at_';

(:
  Defines generic functions for searching, indexing and rebuilding several
  elasticsearch indices. The elasticsearch uri can be configured above, you may
  also set a prefix for index names in case the namespace is shared.

  elastic:setup() serves to (re)build all elasticsearch indices from register
  files, edition pages and wiki pages. Changes to wiki pages are immediately
  re-indexed, see /modules/wiki/confluence.xqm.
:)

declare function elastic:search($index, $query) {
  let $json := elastic:to_json($query)
  let $response := http:send-request(
      <http:request method="POST" href="{$elastic:uri}/{$elastic:prefix}{$index}/_search">
          <http:body media-type="application/json" method="text">{$json}</http:body>
      </http:request>
  )
  let $str := util:base64-decode($response[2])
  let $data := parse-json($str)
  return $data
};

declare function elastic:drop-index($index) {
    let $response := http:send-request(
        <http:request method="DELETE" href="{$elastic:uri}/{$elastic:prefix}{$index}" />
    )
    return $response
};

declare function elastic:create-index($index, $data) {
    let $json := elastic:to_json($data)
    let $response := http:send-request(
        <http:request method="PUT" href="{$elastic:uri}/{$elastic:prefix}{$index}">
            <http:body media-type="application/json" method="text">{$json}</http:body>
        </http:request>
    )
    let $ok := elastic:require_ok($response)
    return $response
};

declare function elastic:index-doc($index, $doc) {
    let $json := elastic:to_json($doc)
    let $response := http:send-request(
        <http:request method="POST" href="{$elastic:uri}/{$elastic:prefix}{$index}/_doc">
            <http:body media-type="application/json" method="text">{$json}</http:body>
        </http:request>
    )
    let $ok := elastic:require_ok($response)
    return $response
};

declare function elastic:index-doc($index, $id, $doc) {
    let $json := elastic:to_json($doc)
    let $response := http:send-request(
        <http:request method="POST" href="{$elastic:uri}/{$elastic:prefix}{$index}/_doc/{$id}">
            <http:body media-type="application/json" method="text">{$json}</http:body>
        </http:request>
    )
    let $ok := elastic:require_ok($response)
    return $response
};

declare function elastic:refresh() {
    let $response := http:send-request(
        <http:request method="GET" href="{$elastic:uri}/_refresh" />
    )
    let $ok := elastic:require_ok($response)
    return $response
};

declare function elastic:index-person($person) {
    let $doc := map {
        "id": string($person/@xml:id),
        "name": map {
            "de": elastic:textFrom($person/tei:persName[@xml:lang='de']),
            "fr": elastic:textFrom($person/tei:persName[@xml:lang='fra'])
        },
        "search_data": map {
            "de": elastic:textFrom($person/*[@xml:lang='de'] | $person/*[not(@xml:lang)]),
            "fr": elastic:textFrom($person/*[@xml:lang='fra'] | $person/*[not(@xml:lang)])
        }
    }
    return elastic:index-doc('people', $doc)
};

declare function elastic:index-place($place) {
    let $doc := map {
        "id": string($place/@xml:id),
        "name": map {
            "de": elastic:textFrom($place/tei:placeName[@type='current' and @xml:lang='de']),
            "fr": elastic:textFrom($place/tei:placeName[@type='current' and @xml:lang='fra'])
        },
        "search_data": map {
            "de": elastic:textFrom($place/*[@xml:lang='de']),
            "fr": elastic:textFrom($place/*[@xml:lang='fra'])
        }
    }
    return elastic:index-doc('places', $doc)
};

declare function elastic:index-work($work) {
    let $doc := map {
        "id": string($work/@xml:id),
        "name": map {
            "de": elastic:textFrom($work/tei:name[@xml:lang='de']),
            "fr": elastic:textFrom($work/tei:name[@xml:lang='fra'])
        },
        "search_data": map {
            "de": elastic:textFrom($work/*[@xml:lang='de']),
            "fr": elastic:textFrom($work/*[@xml:lang='fra'])
        }
    }
    return elastic:index-doc('works', $doc)
};

declare function elastic:index-edition-page($page, $locale) {
    let $uri := string($page//xhtml:span[@id='tei-meta-textGridURI']/text())
    let $edition := fn:tokenize($uri, '[:\.]')[2]
    let $page_number := xs:integer($page//xhtml:span[@class='pb']/text())
    let $page_number_fallback := if ($page_number) then
            $page_number 
        else
            xs:integer($page//tei:pb/@n)
    let $body := elastic:textFrom($page/xhtml:div[@class='body'])
    let $doc := map {
        "id": concat('edition-', $edition, '-', $page_number),
        "edition": $edition,
        "page_number": $page_number_fallback,
        "locale": $locale,
        "name": string($page//xhtml:h4[starts-with(@id, 'tei-title-main-')][1]),
        "search_data": fn:normalize-space($body)
    }
    (: let $x := util:log-system-out($page_number_fallback) :)
    return elastic:index-doc('edition_pages', $doc)
};

declare function elastic:index-edition($id) {
    let $doc := collection(concat($config:app-root, '/templates/', $id))
    return 
        for $page in $doc/xhtml:div
        let $lang := elastic:lang-for-edition-id($id)
        (: let $x := util:log-system-out(concat('indexing page ', $page, $lang)) :)
        return elastic:index-edition-page($page, $lang)
};

declare function elastic:index-wiki-page($page, $locale) {
    let $uri := string(base-uri($page))
    let $doc_name := fn:tokenize($uri, '/')[6]
    let $parts := fn:tokenize($doc_name, '[_\.]')
    let $id := $parts[1]
    let $locale := $parts[2]
    let $elastic_doc_id := concat($id, '_', $locale)
    let $doc := map {
        "uri": $uri,
        "id": $id,
        "locale": $locale,
        "name": elastic:textFrom($page//h1[1]),
        "search_data": fn:normalize-space(elastic:textFrom($page))
    }
    return elastic:index-doc('wiki_pages', $elastic_doc_id, $doc)
};

declare function elastic:build-ref-map($locale) {
    let $editions := collection(concat($config:app-root, '/textgrid/data'))
    let $locale-editions := $editions//tei:text[@xml:lang=$locale]
    
    let $refs := $locale-editions//tei:ref[not(starts-with(@target, 'http'))]
    let $targets :=
        for $r in $refs
        let $target := data($r/@target)
        let $clean-target := fn:replace($target, ' .+', '')
(:        let $x := util:log-system-out($clean-target):)
        let $id := substring($clean-target, 2)
        return $id
    
    for $target-id in $targets
    return
        let $target-element := $locale-editions//*[@xml:id=$target-id]
        let $pb := ($target-element/preceding::tei:pb)[last()]
        let $doc-uri := base-uri($pb)
        let $edition-id := substring($doc-uri, string-length($doc-uri) - 8, 5)
        
        let $elastic_doc_id := ()
        let $doc :=
            if ($locale = 'de') then
                map {
                    'ref_target': $target-id,
                    'view': data($pb/@n),
                    'edition': $edition-id,
                    'translation': elastic:translationFor($edition-id),
                    'locale': $locale
                }
            else
                map {
                    'ref_target': $target-id,
                    'view': data($pb/@n),
                    'translation': $edition-id,
                    'edition': elastic:editionFor($edition-id),
                    'locale': $locale
                }
        return elastic:index-doc('refs', $elastic_doc_id, $doc)
};

declare function elastic:require_ok($response) {
    let $code := xs:integer($response[1]/@status)
    return 
        if ($code < 200 or $code > 299) then
            let $body := util:base64-decode($response[2])
            let $name := QName('http://datypic.com/err', 'ProdNumReq')
            let $msg := concat("(", string($code), ") elasticsearch responded with:", $body)
            return fn:error($name, $msg)
        else
            fn:true()
};

declare function elastic:translationFor($edition-id) {
  let $mapping := map {
    '34zmq': '3czfj',
    '3ptwg': '3r0fv',
    '3qr4f': '3r3nn',
    '34znb': '350mg',
    '3c0m2': '3czn9',
    '34zs7': '3q4rq'
  }
  return $mapping($edition-id)
};

declare function elastic:editionFor($translation-id) {
  let $mapping := map {
    '3czfj': '34zmq',
    '3r0fv': '3ptwg',
    '3r3nn': '3qr4f',
    '350mg': '34znb',
    '3czn9': '3c0m2',
    '3q4rq': '34zs7'
  }
  return $mapping($translation-id)
};

declare function elastic:langFor($id) {
  let $mapping := map {
    '3czfj': 'fr',
    '3r0fv': 'fr',
    '3r3nn': 'fr',
    '350mg': 'fr',
    '3czn9': 'fr',
    '3q4rq': 'fr',
    '34zmq': 'de',
    '3ptwg': 'de',
    '3qr4f': 'de',
    '34znb': 'de',
    '3c0m2': 'de',
    '34zs7': 'de'
  }
  return $mapping($id)
};

declare function elastic:textFrom($e) {
    let $raw := string-join($e/descendant-or-self::*/text(), ' ')
    let $fixed_punctuation := replace($raw, ' ([\\.\\,])', '$1')
    return $fixed_punctuation
};

declare function elastic:to_json($data) {
  let $result := fn:serialize($data, map { 'method': 'json'})
  return $result
};

declare function elastic:setup() {
    let $editions_de := collection(
        concat($config:app-root, '/templates/34zmq'), (: harrach :)
        concat($config:app-root, '/templates/34zs7'), (: sturm :)
        concat($config:app-root, '/templates/3ptwg'), (: corfey :)
        concat($config:app-root, '/templates/3qr4f'), (: neumann :)
        concat($config:app-root, '/templates/34znb'), (: pitzler :)
        concat($config:app-root, '/templates/3c0m2')  (: knesebeck :)
    )
    let $editions_fr := collection(
        concat($config:app-root, '/templates/3czfj'), (: harrach :)
        concat($config:app-root, '/templates/3q4rq'), (: sturm :)
        concat($config:app-root, '/templates/3r0fv'), (: corfey :)
        concat($config:app-root, '/templates/3r3nn'), (: neumann :)
        concat($config:app-root, '/templates/350mg'), (: pitzler :)
        concat($config:app-root, '/templates/3czn9')  (: knesebeck :)
    )
    let $settings := map {
      "analysis": map {
        "filter": map {
          "french_elision": map {
            "type": "elision",
            "articles_case": fn:true(),
            "articles": [
              "l", "m", "t", "qu", "n", "s",
              "j", "d", "c", "jusqu", "quoiqu",
              "lorsqu", "puisqu"
            ]
          },
          "french_stop": map {
            "type": "stop",
            "stopwords": "_french_"
          },
          "french_stemmer": map {
            "type": "stemmer",
            "language": "light_french"
          }
        },
        "analyzer": map {
          "at_french": map {
            "tokenizer": "standard",
            "filter": [
              "french_elision",
              "lowercase",
              "asciifolding",
              "french_stemmer"
            ]
          }
        }
      }
    }
    let $register_mappings := map {
        "_doc": map {
            "properties": map {
                "id": map {"type": "keyword", "index": fn:false()},
                "name": map {
                    "properties": map {
                        "de": map {"type": "text"},
                        "fr": map {"type": "text", "analyzer": "at_french"}
                    }
                },
                "search_data": map {
                    "properties": map {
                        "de": map {"type": "text"},
                        "fr": map {"type": "text", "analyzer": "at_french"}
                    }
                }
            }
        }
    }
    let $refs-mapping := map {
        "_doc": map {
            "properties": map {
                "ref_target": map {"type": "keyword"},
                "edition": map {"type": "keyword"},
                "translation": map {"type": "keyword"},
                "view": map {"type": "keyword"},
                "locale": map {"type": "keyword"}
            }
        }
    }
    
    let $r := [
        elastic:drop-index("people"),
        elastic:drop-index("places"),
        elastic:drop-index("works"),
        elastic:drop-index("wiki_pages"),
        elastic:drop-index("edition_pages"),
        elastic:drop-index("refs"),
        
        elastic:create-index("people", map {"settings": $settings, "mappings": $register_mappings}),
        elastic:create-index("places", map {"settings": $settings, "mappings": $register_mappings}),
        elastic:create-index("works", map {"settings": $settings, "mappings": $register_mappings}),
        elastic:create-index("wiki_pages", map {"settings": $settings, "mappings": map {
            "_doc": map {
                "properties": map {
                    "id": map {"type": "keyword", "index": fn:false()},
                    "locale": map {"type": "keyword"},
                    "name": map {"type": "text", "analyzer": "at_french"},
                    "search_data": map {"type": "text", "analyzer": "at_french"}
                }
            }
        }}),
        elastic:create-index("edition_pages", map {"settings": $settings, "mappings": map {
            "_doc": map {
                "properties": map {
                    "id": map {"type": "keyword", "index": fn:false()},
                    "edition": map {"type": "keyword", "index": fn:false()},
                    "page_number": map {"type": "integer"},
                    "locale": map {"type": "keyword"},
                    "name": map {"type": "text", "analyzer": "at_french"},
                    "search_data": map {"type": "text", "analyzer": "at_french"}
                }
            }
        }}),
        elastic:create-index('refs', map {"settings": $settings, "mappings": $refs-mapping}),
        
        for $doc in collection(concat($config:app-root, '/templates/register'))//tei:person
            return elastic:index-person($doc),
            
        for $doc in collection(concat($config:app-root, '/templates/register'))//tei:place
            return elastic:index-place($doc),
            
        for $doc in collection(concat($config:app-root, '/templates/register'))//tei:item
            return elastic:index-work($doc),
    
        for $doc in $editions_de/xhtml:div
            return elastic:index-edition-page($doc, "de"),
          
        for $doc in $editions_fr/xhtml:div
            return elastic:index-edition-page($doc, "fr"),
            
        elastic:build-ref-map('de'),
        elastic:build-ref-map('fr'),
        
        elastic:refresh()
    ]
    
    return $r
};

declare function elastic:lang-for-edition-id($id) {
    let $des := (
        '34zmq',
        '34zs7',
        '3ptwg',
        '3qr4f',
        '34znb',
        '3c0m2'
    )
    
    return
        if (fn:index-of($des, $id) = ()) then 'fr' else 'de'
};
