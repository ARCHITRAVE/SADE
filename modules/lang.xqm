xquery version "3.1";

module namespace lang="https://sade.textgrid.de/ns/lang";

import module namespace config="https://sade.textgrid.de/ns/config" at "config.xqm";
import module namespace app="https://sade.textgrid.de/ns/app" at "app.xql";
import module namespace functx="http://www.functx.com";

declare namespace lf="https://sade.textgrid.de/ns/langfile";

declare variable $lang:lang := app:getLanguage();

declare function lang:translate( $node as node(), $model as map(*), $content as xs:string ) as node() {

let $langconfig := doc( $config:app-root || "/lang.xml" )
return
    typeswitch($node)
        (: checkbox things in the navbar of edition :)
        case element (h0) return 
            <input type="checkbox" name="" checked="">
                <span/>{$langconfig//lf:word[@key=$content]/lf:lang[@key=$lang:lang]}
            </input>
        case element (h1) return 
            <h1>{$langconfig//lf:word[@key=$content]/lf:lang[@key=$lang:lang]}</h1>
        case element (h0false) return 
            <input type="checkbox" name="">
                <span/>{$langconfig//lf:word[@key=$content]/lf:lang[@key=$lang:lang]}
            </input>
        case element (h5) return
            <h5 class="{$node/@class}">{$langconfig//lf:word[@key=$content]/lf:lang[@key=$lang:lang]}</h5>
        case element (span) return 
            <span id="{$node/@id}" class="{$node/@class}">
                {$langconfig//lf:word[@key=$content]/lf:lang[@key=$lang:lang]}
            </span>
        (: stuff for tooltips :)
        case element (a0) return element a {
            attribute title {$langconfig//lf:word[@key=$content]/lf:lang[@key=$lang:lang]},
            for $att in $node/(@* except (@title,@data-template-content,@data-template))
                    let $att-name := name($att)
                    return attribute {$att-name} {$att},
            for $node in $node/*
                return $node
        }
        case element (input) return element input {
            (: Language stuff for search in results.html :)
            if ($content = "SearchResults") then (
                attribute id {"searchLang"},
                attribute type {"hidden"},
                attribute name {"lang"},
                attribute value {$lang:lang}
            )
            (: Placeholder stuff for input items :)
            else (
                attribute placeholder {$langconfig//lf:word[@key=$content]/lf:lang[@key=$lang:lang]},
                for $att in $node/@*
                    let $att-name := name($att)
                    return if ($att-name != "placeholder") then attribute {$att-name} {$att}
                    else ()
            )
        }
        case element (textarea) return element textarea {
            attribute placeholder {$langconfig//lf:word[@key=$content]/lf:lang[@key=$lang:lang]},
            for $att in $node/@*
                let $att-name := name($att)
                return if ($att-name != "placeholder") then attribute {$att-name} {$att}
                else ()
        }
        case element (button) return element button {
            for $att in $node/@*
                let $att-name := name($att)
                return if ($att-name != "placeholder") then attribute {$att-name} {$att}
                else (),
            $langconfig//lf:word[@key=$content]/lf:lang[@key=$lang:lang]
        }
        case element (a) return element a {
            attribute data-html {"true"},
            attribute title {
                $langconfig//lf:word[@key=$content]/lf:lang[@key=$lang:lang],
                "&lt;br /&gt; &lt;br /&gt;",
                $langconfig//lf:word[@key='tooltipClick']/lf:lang[@key=$lang:lang]
            },
            for $att in $node/@*
                let $att-name := name($att)
                return if (($att-name != "title") and ($att-name != "data-template") and ($att-name != "data-template-content")) then attribute {$att-name} {$att}
                else ()
        }
        default return element {name($node)} {}
};

