xquery version "3.1";
module namespace nav="https://sade.textgrid.de/ns/navigation";

import module namespace app="https://sade.textgrid.de/ns/app" at "app.xqm";
import module namespace config="https://sade.textgrid.de/ns/config" at "config.xqm";
import module namespace templates="http://exist-db.org/xquery/templates";

declare variable $nav:module-key := "navigation";
declare variable $nav:lang := app:getLanguage();
declare variable $nav:langDefault := (config:get("lang.default", "multilanguage"));
declare variable $nav:langTest :=   if($nav:langDefault = "")
                                    then true()
                                    else ($nav:lang = $nav:langDefault);

declare
    %templates:wrap
function nav:navitems($node as node(), $model as map(*)) as map(*) {
    let $confLocation := config:get("location", $nav:module-key)
    let $navitems := doc( $config:app-root || "/" || $confLocation)//navigation/*
    return
        map { "navitems" : $navitems }
};

declare function nav:head($node as node(), $model as map(*)) {
    switch(local-name($model("item")))
        case "submenu" return
            element { node-name($node) } {
                    $node/@*,
                    if ($nav:langTest)
                    then (string($model("item")/@label))
                    else string($model("item")/@*[local-name( . ) = "label-" || $nav:lang]),
                    $node/node()
            }
        case "item" return
            element { node-name($node) } {
                    attribute href {$model("item")/@link},
                    if ($nav:langTest or not($model("item")/@*[local-name() ="label-" || $nav:lang]))
                    then string($model("item")/@label)
                    else string($model("item")/@*[local-name( . ) = "label-" || $nav:lang])
            }
        default return
            <b>not defined: {node-name($model("item"))}</b>
};

declare function nav:subitems($node as node(), $model as map(*)) as map(*) {
  map{ "subitems" : $model("item")//*}
};

declare function nav:subitem($node as node(), $model as map(*)) {
    for $node in $model("subitems")
        return
            if($node/@class) then (
                element div {
                    attribute class {"dropdown-divider"}
                }
            )
            else (
                element a {
                    attribute class {"dropdown-item arch-ul"},
                    attribute href {string($node/@link)},

                    if ($nav:langTest or not($node/@*[local-name() ="label-" || $nav:lang]))then (
                        <span class="arch-white-ul-on-pink">{string($node/@label)}</span>
                    )
                    else (
                        <span class="arch-white-ul-on-pink">{string($node/@*[local-name( . ) = "label-" || $nav:lang])}</span>
                    )
                }
            )
};
