xquery version "3.1";
(:~
 : Provides the endpoint for SADE Publish process
 :)

import module namespace tgconnect="https://sade.textgrid.de/ns/connect" at "connect.xqm";

let $uri as xs:string := request:get-parameter("uri", ""),
    $sid as xs:string := request:get-parameter("sid", ""),
    $target as xs:string := request:get-parameter("target", "data"),
    $user as xs:string := request:get-parameter("user", ""),
    $password as xs:string := request:get-parameter("password", ""),
    $project as xs:string := request:get-parameter("project", ""),
    $surface as xs:string := request:get-parameter("sf", ""),
    $login as xs:boolean := false()

return
    if( $uri = "" ) then
        error(QName("https://sade.textgrid.de/ns/error", "PUBLISH01"), "no URI provided")
    else if (contains($uri, '34zmq') or contains($uri, '3r3nn') or contains($uri, '34zs7') or contains($uri, '3ptwg') or contains($uri, '3qr4f') or contains($uri, '34znb') or contains($uri, '3czfj') or contains($uri, '3q4rq') or contains($uri, '3r0fv') or contains($uri, '350mg') or contains($uri, '3c0m2') or contains($uri, '3czn9') ) then
        <div>{
            for $i in tgconnect:publish($uri,$sid,$target,$user,$password,$project,$surface,$login)
            return
                <ok>{ $uri } » { $i }</ok>
        }</div>
    else
        error(QName("https://sade.textgrid.de/ns/error", "PUBLISH09"), "Please publish only the xml files of the 12 editions/translations.")
