xquery version "3.1";

(:~                   
 : This XQ indexes all transformed HTML content for the search.
 : It can be executed manually or better be triggered by existdb cron job to run periodically.
 : The web page content is transformed via the Sepia Content Library when the web page is accessed.
 : The Elastic Search Index can then be updated via this XQ.
 : @author mmarkus1
 : @version 1.0
 :)

import module namespace elastic="http://elastic.io" at "elastic.xqm";
import module namespace config="https://sade.textgrid.de/ns/config" at "config.xqm";
import module namespace functx = 'http://www.functx.com';

declare variable $contentPath := concat($config:app-root, '/docs');

let $rm := elastic:drop-index("wiki_pages")
let $listOfHTMLDocs := xmldb:xcollection($contentPath)
return
    
    for $doc in $listOfHTMLDocs return
        if (contains(util:document-name($doc),".html")) then
            let $lang := util:document-name($doc) => fn:substring-before(".") => fn:substring-after("_")
            return 
                elastic:index-wiki-page($doc, $lang)
        else ()