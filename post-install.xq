xquery version "3.1";

import module namespace transformXML="https://sade.textgrid.de/ns/transform" at "modules/architrave/transform.xqm";
import module namespace elastic="http://elastic.io" at "modules/elastic.xqm";

declare namespace repo="http://exist-db.org/xquery/repo";
declare namespace xconf="http://exist-db.org/collection-config/1.0";
declare namespace cpcf="http://sepia.io/content-parser/ns/configfile";

(: the target collection into which the app is deployed :)
declare variable $target := "/db/apps/sade-architrave/";

(:~
 : Helper function to get all function names declared in the application.
 : Uses externally declared variable $target.
 :
 : @author Mathias Göbel
 : @since 3.1.0
 :)
declare function local:get-function-names()
as xs:string* {
for $modulePath in collection($target)/base-uri()[ends-with(., ".xqm")]
let $functions := inspect:module-functions( xs:anyURI($modulePath) ) (: QNames :)
return
  for $function in $functions
  let $functionQName := function-name($function)
  return
      prefix-from-QName($functionQName) || ":" || local-name-from-QName($functionQName)
};

(:~
 : Formats the test coverage output - mainly used by GitLab test coverage parser.
 :
 : @author Mathias Göbel
 : @since 3.1.0
 : @see https://docs.gitlab.com/ee/user/project/pipelines/settings.html#test-coverage-parsing
 :   :)
declare function local:format-test-rate-output($value as xs:decimal)
as xs:string {
    "coverage: " || ($value * 100) => format-number("###.00") || "%"
};

(:~
 : Returns the relative amount of tests for all function in this package
 : @param $tests – a set of unit test results in the JUnit XML format
 :
 : @author Mathias Göbel
 : @since 3.1.0
 : @see https://www.ibm.com/support/knowledgecenter/en/SSQ2R2_14.1.0/com.ibm.rsar.analysis.codereview.cobol.doc/topics/cac_useresults_junit.html
 :  :)
declare function local:test-function-rate($tests as element(testsuites)*)
as xs:string {
    (: is function cardinality an issue? thats why disstinct-value is used :)
    let $functionNames := (local:get-function-names()) => distinct-values() => count()
    let $testedFunctionNames := ($tests//testcase/string(@class)) => distinct-values() => count()
    let $coverage :=
        if($testedFunctionNames = 0) then 0 else $testedFunctionNames div $functionNames
    return
        local:format-test-rate-output($coverage)
};

(:~
 : Returns the relative amount of successfull tests
 : @param $tests – a set of unit test results in the JUnit XML format
 :
 : @author Mathias Göbel
 : @since 3.1.0
 : @see https://www.ibm.com/support/knowledgecenter/en/SSQ2R2_14.1.0/com.ibm.rsar.analysis.codereview.cobol.doc/topics/cac_useresults_junit.html
 :  :)
declare function local:test-success-rate($tests as element(testsuites)*)
as xs:string {
  let $allFailures := $tests//testsuite/@failures => sum()
  let $failsTriggered := $tests//testcase[@name=("FAIL", "fail")] => count()
  let $failures := $allFailures - $failsTriggered
  let $errors := $tests//testsuite/@errors => sum()
  let $pending := $tests//testsuite/@pending => sum()

  let $testsDone := $tests//testsuite/@tests => sum()

  let $testCoverage := ($testsDone - sum( ($failures, $errors, $pending) )) div $testsDone
  return
    local:format-test-rate-output($coverage)
};

declare function local:inject-map-tiles($base-path) {
  let $target-uri := '/db/apps/sade-architrave/templates/itinerary'
  let $c := xmldb:create-collection($target-uri, 'raster')
  let $collection-uri := concat($target-uri, '/', 'raster')
  let $results := xmldb:store-files-from-pattern(
    $collection-uri, $base-path, '**/*', 'image/png', fn:true()
  )
  return ()
};

let $fileSeparator := util:system-property("file.separator")
let $system-path := system:get-exist-home() || $fileSeparator
let $use-template :=
    if(doc-available($target || "/textgrid/data/collection-template.xconf"))
    then xmldb:rename($target || "/textgrid/data", "collection-template.xconf", "collection.xconf")
    else true()
let $data-xconf := $target || "/textgrid/data/collection.xconf"
let $store-xconf := xmldb:store("/db/system/config" || $target || "/textgrid/data", "collection.xconf", doc( $data-xconf ))
let $cache := transformXML:cacheAllEditions()
let $reindex := xmldb:reindex($target || "/textgrid/data")

(: import XML files from TGRep and transform them into HTML :)
let $log := util:log-system-out("Importing XML files from TGRep")
let $harrach := transformXML:importFromTGRep('34zmq.6','34zmq')
let $harrachFR := transformXML:importFromTGRep('3czfj.3','3czfj')
let $pitzler := transformXML:importFromTGRep('34znb.3','34znb')
let $pitzlerFR := transformXML:importFromTGRep('350mg.4','350mg')
let $sturm := transformXML:importFromTGRep('34zs7.3','34zs7')
let $sturmFR := transformXML:importFromTGRep('3q4rq.3','3q4rq')
let $knesebeck := transformXML:importFromTGRep('3c0m2.5','3c0m2')
let $knesebeckFR := transformXML:importFromTGRep('3czn9.7','3czn9')
let $corfey := transformXML:importFromTGRep('3ptwg.3','3ptwg')
let $corfeyFR := transformXML:importFromTGRep('3r0fv.2','3r0fv')
let $neumann := transformXML:importFromTGRep('3qr4f.2','3qr4f')
let $neumannFR := transformXML:importFromTGRep('3r3nn.3','3r3nn')
let $log := util:log-system-out("DONE: Importing XML files from TGRep")

let $store-xconf := xmldb:store("/db/system/config"||$target||"/textgrid/data", "collection.xconf", doc( $data-xconf ))
let $reindex := xmldb:reindex($target||"/textgrid/data")
(: we have to test for writable path :)
let $path := $system-path
let $user := util:system-property("user.name")
let $message1 := $path || " is not available. Create it and make sure " || $user || " can write there."
let $message2 := "Could not write to " || $path || "."
let $tei-cache-msg := util:log-system-out("caching edition TEI files in '/textgrid' ...")
let $tei-cache := transformXML:cacheAllEditions()

(:~ let $map-tiles-msg := util:log-system-out("importing paris map tiles ...")
let $map-tiles := local:inject-map-tiles('/var/www/map-tiles.git/raster') ~:)

let $project-name := tokenize($target, "/")[last()]
let $log := util:log-system-out("installing " || $project-name)

(: update content parser config :)
let $log := util:log-system-out("Updating content parser config")
let $cp-path := collection("/db/system/repo")//cpcf:config//cpcf:param
let $contentapi-repo := "29903"
let $inject-repo := update value $cp-path[@key="contentapi.repo"] with text {$contentapi-repo}
let $contentapi-branch := "main"
let $inject-branch := update value $cp-path[@key="contentapi.branch"] with text {$contentapi-branch}
let $contentapi-secret := 
    if (file:exists("/opt/contentapi.env"))
    then file:read("/opt/contentapi.env")
    else "error: could not read contentapi.env - file does not exist"
let $log :=
    if (file:exists("/opt/contentapi.env"))
    then util:log-system-out("Succesfully got gitlab secret for contentapi")
    else util:log-system-out("Error in applying contentapi config: could not read contentapi.env - file does not exist. Please update config manually.")
let $inject-secret := update value $cp-path[@key="contentapi.secret"] with text {$contentapi-secret}
let $contentapi-target := "/db/apps/sade-architrave/docs"
let $inject-target := update value $cp-path[@key="contentapi.target"] with text {$contentapi-target}
let $contentapi-multilang-enabled := "true"
let $inject-multilang-enabled := update value $cp-path[@key="contentapi.multilang.enabled"] with text {$contentapi-multilang-enabled}
let $contentapi-multilang-default := "fr"
let $inject-multilang-default := update value $cp-path[@key="contentapi.multilang.default"] with text {$contentapi-multilang-default}
let $contentapi-multilang-alt := "de"
let $inject-multilang-alt := update value $cp-path[@key="contentapi.multilang.alt"] with text {$contentapi-multilang-alt}

let $indexing-msg := util:log-system-out("indexing edition pages and registers ...")
let $indexing := elastic:setup()

(: we have to test for writable path :)
let $path := $system-path
let $user := util:system-property("user.name")
let $message1 := $path || " is not available. Create it and make sure "
                || $user || " can write there."
let $message2 := "Could not write to " || $path || "."
return (
if
    (file:is-directory($path))
then
    try { file:serialize-binary( xs:base64Binary(util:base64-encode("beacon")) , $path || "beacon.txt") }
    catch * { util:log-system-out( "&#27;[48;2;"|| "255;0;0" ||"m&#27;[38;2;0;0;0m " ||
    $message2 || " &#27;[0m" ) }
else
    util:log-system-out( "&#27;[48;2;"|| "255;0;0" ||"m&#27;[38;2;0;0;0m " ||
    $message1 || " &#27;[0m" )
,

let $parameters :=
    <output:serialization-parameters
        xmlns:output="http://www.w3.org/2010/xslt-xquery-serialization">
        <output:method value="xml"/>
        <output:indent value="yes"/>
    </output:serialization-parameters>

let $tests := util:eval(xs:anyURI('test.xq'))
let $print := util:log-system-out(serialize($tests, $parameters) )
let $print := util:log-system-out( local:test-function-rate($tests) )
let $file-name := $system-path || ".."|| $fileSeparator || "tests-"
    || $project-name || "_job-" || environment-variable("CI_JOB_ID") || ".log.xml"

let $file := file:serialize(<tests time="{current-dateTime()}">{ $tests }</tests>, $file-name, ())
  return
    util:log-system-out($file-name || " saved.")
)
