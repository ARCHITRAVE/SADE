# Search interface

The Search on the Architrave platform is implemented as a set of React
components. The components request search results and translations via AJAX from
eXist-db endpoints. A few lines of CSS is included within the components.

The entrypoints are two components: ArchSearchForm and ArchSearchResults are
both loaded onto the `search.html` page into `.arch-search-form` and
`.arch-search-results` respectively. All changes to the search criteria are
first pushed to the url hash fragment which in turn triggers fetching the
results. This makes searches "linkable".

## Backend

The endpoints are

* /api.json?action=search-editions
* /api.json?action=search-people
* /api.json?action=search-works
* /api.json?action=search-places
* /api.json?action=search-pages

Each allowing additional parameters for `page`, `per_page`, `terms` and
`locale`. You can find the implementation in `/modules/api.xq`.

## Frontend development

The components are built with webpack so a reasonably new nodejs is required.
Required npm modules need to be installed with

    npm install

after which the dev server can be run with

    webpack-dev-server

You may want to change search.html to include the dev server's app.js file so
that changes are reflected immediately within eXist-db.

## Frontend deployment

Changes can be deployed to the architrave project by building the frontend part
and copying it to the templates directory

    webpack --mode=production
    cp public/app.js ../templates/js/search.js

The the architrave app needs to be re-deployed as usual.
