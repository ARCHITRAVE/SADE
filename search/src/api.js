// A simple singleton encapsulating the AJAX requests

const Url = require('@wendig/lib').Url

class Api {
  // loads the translations from lang.xml
  translations() {
    console.log('FETCHING translations')

    return fetch('lang.xml').
      then((response) => response.text()).
      then((data) => {
        if (window.DOMParser) {
          let parser = new DOMParser()
          return parser.parseFromString(data, "text/xml");
        }
      }) 
  }

  // fetches search results via several eXist-db endpoints and combines them 
  // into a single response, returns a promise
  call(params, aspects) {
    console.log('API: ' + params)
    let results = {}
    let promises = []
    aspects = aspects || ['pages', 'people', 'works', 'places', 'editions']

    params['per_page'] = 10

    for (const aspect of aspects) {
      let u = new Url({
        path: 'api.json',
        params: params
      })
      params['action'] = 'search-' + aspect
      let promise = fetch(u.url()).
        then((response) => response.json()).
        then(response => results[aspect] = response)
      promises.push(promise)
    }

    return new Promise((resolve, reject) => {
      Promise.all(promises).then(() => {
        resolve(results)
      })
    })
  }
}

const api = new Api();

export default api;
