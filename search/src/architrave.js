import api from './api'

const Url = require('@wendig/lib').Url

// A set of helper methods for various contexts

let translations = {}

function isEmptyObject(obj) {
  return !!Object.keys(obj).length
}

// returns the current interface locale from the browser url or a default
function locale() {
  const url = document.location.href;
  let m = null
  if (m = url.match(/lang=(de|fr|en)/)) {
    return m[1]
  }

  return 'fr';
}

// returns the complementary interface locale to the current locale
function otherLocale() {
  return (locale() == 'fr' ? 'de' : 'fr')
}

// returns the content locale (which results to search in)
function cLocale() {
  return Url.current().hashParams()['locale'] || 'all'
}

// loads translations and caches them in a lookup map
function loadTranslations() {
  return api.translations().then((data) => {
    for (const word of data.querySelectorAll('word')) {
      const key = word.attributes['key'].value
      translations[key] = {}
      for (const lang of word.querySelectorAll('lang')) {
        const lang_key = lang.attributes['key'].value
        translations[key][lang_key] = lang.innerHTML
      }
    }
  })
}

// returns a translation for a given key
function translate(key, l=null) {
  l = l || locale()
  let msg = 'TRANSLATING ' + key + ' for ' + l

  let result = key

  if (translations[key]) {
    if (translations[key][l]) {
      result = translations[key][l]
    } else {
      msg += ' (LOCALE NOT FOUND)'
    }
  } else {
    msg += ' (KEY NOT FOUND)'
  }

  return result
}

// capitalizes the first letter in a string
function capitalize(string) {
  return string.charAt(0).toUpperCase() + string.slice(1)
}

function tcap(key) {
  return capitalize(translate(key));
}

function buildUrl(path, opts = {}) {
  let url = new Url(opts)
  url.setPath(path)
  return url.url()
}

export {
  buildUrl,
  locale,
  otherLocale,
  cLocale,
  translate as t,
  tcap, 
  loadTranslations,
  isEmptyObject
}
