import React from 'react'
import {locale, cLocale, t, tcap} from './architrave'

const Url = require('@wendig/lib').Url

// This component serves to toggle searching for results irrespective of the
// current locale.

class ContentLocaleSelector extends React.Component {
  constructor(props) {
    super(props)

    this.changed = this.changed.bind(this)
  }

  changed(event) {
    let u = Url.current()
    
    if (event.target.checked) {
      u.updateHashParams({locale: 'any'}).apply()
    } else {
      u.updateHashParams({locale: null}).apply()
    }
    this.forceUpdate()
  }

  render() {
    return(
      <div className="arch-content-locale-selector mt-4 mb-4">
        <label className="d-block d-md-inline">
          <span className={cLocale() == 'any' ? 'checked' : null}>
            <i className="fa fa-check-square" />
            <i className="fa fa-square" />
          </span>
          <input
            className="mr-2"
            type="checkbox"
            name="clocale"
            value='any'
            checked={cLocale() == 'any'}
            onChange={this.changed}
          />
          {t('show_results_in_other_languages')}
        </label>
      </div>
    )
  }
}

export {ContentLocaleSelector}