import React from 'react'
import Mark from 'mark.js'

import {locale, otherLocale, tease, t} from './architrave'

const Url = require('@wendig/lib').Url

class ArchHighlighter extends React.Component {
  constructor(props) {
    super(props)

    this.update = this.update.bind(this)
    this.toggle = this.toggle.bind(this)
    this.transLoaded = this.transLoaded.bind(this)

    this.active = true
    this.highlights = Url.current().hashParams()['highlight']
    if (this.highlights) {
      this.highlights = decodeURIComponent(this.highlights).split('|')
    }

    this.mark = new Mark(this.target())
  }

  componentDidMount() {
    // the synoptic text is not available on page load, the code loading it
    // emits the trans-loaded event on window when the text is avaible
    window.addEventListener('trans-loaded', this.transLoaded)
    this.buttonTarget().addEventListener('click', this.toggle)

    this.update()
  }

  componentWillUnmount() {
    this.buttonTarget().removeEventListener('click', this.toggle)
    window.removeEventListener('trans-loaded', this.transLoaded)
  }

  //activates the button and does the actual highlighting when active and when
  update() {
    // console.log(this.highlights)

    this.buttonTarget().classList.toggle('d-none', !this.highlights)

    if (this.highlights && this.active) {
      this.mark.mark(this.highlights, {
        element: 'span',
        className: 'hl',
        acrossElements: true,
        separateWordSearch: false,
        diacritics: false
        // debug: true
      })
    } else {
      this.mark.unmark()
    }
  }

  toggle(event) {
    event.preventDefault()
    this.active = !this.active

    this.buttonTarget().querySelector('a').classList.toggle('text-muted')
    this.update()
  }

  transLoaded(event) {
    this.mark = new Mark(this.target())
    this.update()
  }

  target() {
    return document.querySelector('div.body')
  }

  buttonTarget() {
    return document.querySelector('.arch-highlight-toggle')
  }

  render() {
    return ''
  }
}

export default ArchHighlighter
