import './style.scss'

import React from 'react'
import ReactDOM from 'react-dom'

import {t, loadTranslations} from './architrave'
import ArchSearchForm from './search_form'
import ArchSearchResults from './search_results'
import ArchHighlighter from './highlighter'

const Url = require('@wendig/lib').Url

// a helper method to mount a given component on all elements specified by a
// css selector
function mount(selector, componentClass) {
  const targets = document.querySelectorAll(selector)
  for (let i = 0; i < targets.length; i++) {
    const instance = React.createElement(componentClass)
    ReactDOM.render(instance, targets[i])
  }
}

// load the translations, once they are available, mount the actual search
// components
loadTranslations().then(() => {
  mount('.arch-search-form', ArchSearchForm)
  mount('.arch-search-results', ArchSearchResults)
  mount('.arch-highlighter', ArchHighlighter)
})

// make the translate method available globally
window.translate = t
window.AtUrl = Url
window.searchBus = new EventTarget()
