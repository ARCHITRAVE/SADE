import React from 'react'
import {tcap, t} from './architrave'

const Url = require('@wendig/lib').Url

// The pagination widget for search results

export class Pagination extends React.Component {
  constructor(props) {
    super(props)

    this.firstPage = this.firstPage.bind(this)
    this.previousPage = this.previousPage.bind(this)
    this.nextPage = this.nextPage.bind(this)
    this.lastPage = this.lastPage.bind(this)
    this.currentPage = this.currentPage.bind(this)
    this.pages = this.pages.bind(this)
  }

  currentPage() {
    return parseInt(Url.current().hashParams()['page'] || 1)
  }

  pages() {
    return Math.ceil(this.props.total / this.props.perPage)
  }

  firstPage(event) {
    event.preventDefault()
    Url.current().updateHashParams({page: 1}).apply()
  }

  previousPage(event) {
    event.preventDefault()
    Url.current().updateHashParams({page: this.currentPage() - 1}).apply()
  }

  nextPage(event) {
    event.preventDefault()
    Url.current().updateHashParams({page: this.currentPage() + 1}).apply()
  }

  lastPage(event) {
    event.preventDefault()
    Url.current().updateHashParams({page: this.pages}).apply()
  }

  render() {
    if (this.pages() == 1) {return ''}

    const pageElements = Array(this.pages()).fill(0).map((e, i) => {
      return <Page key={i} number={i + 1} current={this.currentPage()} pages={this.pages()} />
    })

    let firstCn = ['page-item']
    let prevCn = ['page-item']
    let nextCn = ['page-item']
    let lastCn = ['page-item']

    if (this.currentPage() == 1) {
      prevCn.push('disabled')
      firstCn.push('disabled')
    }

    if (this.currentPage() == this.pages()) {
      nextCn.push('disabled')
      lastCn.push('disabled')
    }

    return(
      <nav className="mt-4">
        <ul className="pagination">
          <li className={firstCn.join(' ')}>
            <a className="page-link" href="#" onClick={this.firstPage}>
              <i className="fa fa-angle-double-left" />
            </a>
          </li>
          <li className={prevCn.join(' ')}>
            <a className="page-link" href="#" onClick={this.previousPage}>
              <i className="fa fa-angle-left" />
            </a>
          </li>
          <li className="page-item">
            <a className="page-link">
              {t('page')} {this.currentPage()} / {this.pages()}
            </a>
          </li>
          <li className={nextCn.join(' ')}>
            <a className="page-link" href="#" onClick={this.nextPage}>
              <i className="fa fa-angle-right" />
            </a>
          </li>
          <li className={lastCn.join(' ')}>
            <a className="page-link" href="#" onClick={this.lastPage}>
              <i className="fa fa-angle-double-right" />
            </a>
          </li>
        </ul>
      </nav>
    )
  }
}

function Page(props) {
  let cn = ['page-item']
  if (props.current == props.number) {
    cn.push('active')
  }

  return(
    <li className={cn.join(' ')}>
      <a className="page-link" href="#">{props.number}</a>
    </li>
  )
}
