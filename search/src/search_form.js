import React from 'react'
import {locale, t, tcap} from './architrave'
import {
  HashRouter as Router,
  Route,
} from 'react-router-dom'
import {ContentLocaleSelector} from './content_locale_selector'

const Url = require('@wendig/lib').Url

// The component representing the search form. On one side, it listens to hash
// fragment changes to update the form fields on a fragment change. On the other
// side, it updates the hash fragment when the user changes the search criteria.

export default class ArchSearchForm extends React.Component {
  constructor(props) {
    super(props)

    this.submit = this.submit.bind(this)
    this.newHash = this.newHash.bind(this)
    this.inputChanged = this.inputChanged.bind(this)

    this.form = React.createRef()

    // transfer params from url query to hash query (except "lang")
    let u = Url.current()
    let update = {...u.params()}
    let hashUpdate = {...u.hashParams()}
    for (const [key, value] of Object.entries(u.params())) {
      if (key != 'lang') {
        let sanitized = value.replaceAll(/\+/g, ' ')
        sanitized = decodeURIComponent(sanitized)
        hashUpdate[key] = sanitized
        update[key] = null
      }
    }
    u.
      updateParams(update).
      updateHashParams(hashUpdate).
      replaceState()

    this.newHash()
    window.addEventListener('hashchange', this.newHash, false);
  }

  componentWillUnmount() {
    window.removeEventListener('hashchange', this.newHash);
  }

  newHash(event) {
    let newState = {}
    for (const [key, value] of Object.entries(Url.current().hashParams())) {
      newState[key] = decodeURIComponent(value)
    }

    if (event) {
      this.setState(newState)
    } else {
      // initial invocation from constructor
      this.state = newState
    }
  }

  inputChanged(event) {
    this.setState({terms: event.target.value})
  }

  render() {
    return (
      <form onSubmit={this.submit} ref={this.form}>
        <div className="row">
          <div className="col-12 col-md-6">
            <input
              className="form-control arch-value"
              name="terms"
              onChange={this.inputChanged}
              value={this.state.terms}
            />
          </div>
          <div className="col-12">
            <ContentLocaleSelector />
          </div>
        </div>
        <input className="btn btn-primary mt-3" type="submit" value={tcap('search')} />
      </form>
    )
  }

  submit(event) {
    event.preventDefault()

    let hashUpdate = {}
    this.form.current.querySelectorAll('.arch-value').forEach((item) => {
      hashUpdate[item.name] = (item.value ? item.value : null)
    })
    Url.
      current().
      updateHashParams(hashUpdate).
      apply()
  }
}
