import React from 'react'
import {locale, otherLocale, tease, t, buildUrl} from './architrave'

export class ArchSearchResult extends React.Component {
  constructor(props) {
    super(props)

    this.clickMe = this.clickMe.bind(this)
  }

  isType(type) {
    const index = this.props.data._index

    if (type == 'register') {return !!index.match(/_(people|works|places)$/)}
    if (type == 'page') {return !!index.match(/_wiki_pages$/)}
    if (type == 'edition') {return !!index.match(/_edition_pages$/)}

    if (type == 'person') {return !!index.match(/_people$/)}
    if (type == 'work') {return !!index.match(/_works$/)}
    if (type == 'place') {return !!index.match(/_places$/)}
    
    return false
  }

  clickMe(event) {
    if (this.isType('register')) {
      event.preventDefault()
      this.openResult()
    }
  }

  editionFor(translation) {
    return {
      '3czfj': '34zmq', // harrach
      '3q4rq': '34zs7', // sturm
      '3r0fv': '3ptwg', // corfey
      '3r3nn': '3qr4f', // neumann
      '350mg': '34znb', // pitzler
      '3czn9': '3c0m2'  // knesebeck
    }[translation] || translation
  }

  translationFor(edition) {
    return {
      '34zmq': '3czfj', // harrach
      '34zs7': '3q4rq', // sturm
      '3ptwg': '3r0fv', // corfey
      '3qr4f': '3r3nn', // neumann
      '34znb': '350mg', // pitzler
      '3c0m2': '3czn9'  // knesebeck
    }[edition] || edition
  }

  resultUrl() {
    const source = this.props.data._source

    if (this.isType('edition')) {
      // console.log(source)

      let params = {
        page: source.page_number,
        lang: source.locale || 'de'
      }

      if (params.lang == 'de') {
        Object.assign(params, {
          edition: source.edition,
          translation: this.translationFor(source.edition)
        })
      } else {
        Object.assign(params, {
          edition: this.editionFor(source.edition),
          translation: source.edition
        })
      }

      return buildUrl('view.html', {
        params: params,
        hashPath: '/',
        hashParams: {
          highlight: this.highlightPatterns().join('|')
        }
      })
    }

    if (this.isType('page')) {
      return buildUrl('content.html', {
        params: {
          id: source.id,
          lang: source.locale
        }
      })
    }

    return '#'
  }

  openResult() {
    // console.log('source', this.props.data._source)
    if (this.isType('person')) {openPersName(this.props.data._source.id)}
    if (this.isType('work')) {openArtWork(this.props.data._source.id)}
    if (this.isType('place')) {openPlaceName(this.props.data._source.id)}
  }

  highlights() {
    const list = this.props.data.highlight.search_data || []
    if (list.length == 0) {return ''}

    const inner = list.map((e, i) => {
      return <div
        key={i}
        className="pt-2 arch-text"
        dangerouslySetInnerHTML={{__html: e}}
      />
    })

    return <div className="arch-highlights">{inner}</div>
  }

  highlightPatterns() {
    let results = []
    const all = this.props.data.highlight
    for (const [k, v] of Object.entries(all)) {
      for (const hl of v) {
        // a serious hash to somewhat work around
        // https://github.com/elastic/elasticsearch/issues/29561, we use
        // combine adjacent matches so that the singluar parts are not carried
        // over as matches to the synoptic view
        const hl_combined = hl.matchAll(
          /<span class="hl">[^<]+<\/span>([\s\-\,\.]+<span class="hl">[^<]+<\/span>)*/g
        )
        for (const snippet of hl_combined) {
          const clean = snippet[0].replaceAll(/<[^>]+>/g, '')
          
          if (!results.includes(clean)) {
            results.push(clean)
          }
        }
      }
    }
    return results
  }

  name() {
    if (this.isType('register')) {
      const d = this.props.data

      // console.log(d, locale())

      const hl = 
        d.highlight[`name.${locale()}`] ||
        d._source.name[locale()] ||
        d.highlight[`name.${otherLocale()}`] ||
        d._source.name[otherLocale()] ||
        []

      let result = hl

      // let result = hl[0] ||
      //   d._source.name[locale()] ||
      //   d._source.name[otherLocale()]

      // its an array if there are e.g. historical names (or empty tags for
      // historical names)
      if (Array.isArray(result)) {
        result = result.filter(x => !!x).join(', ')
      }

      return result
    }

    if (this.isType('edition')) {
      let page = this.props.data._source.page_number
      let base = (
        (this.props.data.highlight.name || [])[0] ||
        this.props.data._source.name ||
        `${this.props.data._source.id}`
      )

      return `${base} (${t('view')} ${page})`
    }

    if (this.isType('page')) {
      return this.props.data._source.name
    }
  }

  dataId() {
    return this.props.data._source.id
  }

  render() {
    let name = this.name() || t('button_unnamed')

    return(
      <a
        className="arch-search-result d-block mb-3 p-3"
        href={this.resultUrl()}
        data-id={this.dataId()}
        target="_blank"
        rel="noopener"
        onClick={this.clickMe}
      >
        <em
          className="arch-title"
          dangerouslySetInnerHTML={{__html: name}}
        />
        {this.highlights()}
      </a>
    )
  }
}
