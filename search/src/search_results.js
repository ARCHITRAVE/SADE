import React from 'react'
import {locale, t, tcap, tease} from './architrave'
import {
  HashRouter as Router,
  Route,
} from 'react-router-dom'
import api from './api'
import {ArchSearchResult} from './search_result'
import {Pagination} from './pagination'
import {ContentLocaleSelector} from './content_locale_selector'

const Url = require('@wendig/lib').Url

// This component fetches the actual search results from the api and shows the
// results in the result widget. It listens to hash fragment changes to gather
// the search criteria before fetching results.

export default class ArchSearchResults extends React.Component {
  constructor(props) {
    super(props)

    this.newHash = this.newHash.bind(this)
    this.tabTo = this.tabTo.bind(this)
    this.updateResults = this.updateResults.bind(this)

    this.state = {}

    this.newHash()
    window.addEventListener('hashchange', this.newHash, false);
  }

  componentWillUnmount() {
    window.removeEventListener('hashchange', this.newHash);
  }

  // notify the outside world that new search results have been rendered
  componentDidUpdate() {
    if (window && window.searchBus) {
      const params = Url.current().hashParams()
      const tab = params['tab'] || 'editions'
      const results = this.state.results[tab]
      const event = new CustomEvent('results_rendered', {detail: {
        total: results.hits.total
      }})
      window.searchBus.dispatchEvent(event)
    }
  }

  tabTo(event, newTab) {
    event.preventDefault()
    Url.current().updateHashParams({tab: newTab, page: 1}).apply()
  }

  render() {
    const params = Url.current().hashParams()

    const tab = params['tab'] || 'editions'
    const page = parseInt(params['page'] || 1)

    if (!this.state.results) {
      return ''
    }

    let results = this.state.results[tab]
    const content = results.hits.hits.map((hit) => {
      return <ArchSearchResult key={hit._id} data={hit} />
    })


    const body = 
      <React.Fragment>
        <div className="pt-4">
          {content}
        </div>
        <div className="d-flex justify-content-center">
          <Pagination total={results.hits.total} perPage={10} />
        </div>
      </React.Fragment>
    const noResults = <p className="pt-5">{tcap('no_results')}</p>

    return(
      <div className="mb-8">
        <ul className="nav nav-pills nav-fill">
          <TabSwitcher
            tab="editions"
            label={tcap('editions')}
            active={tab}
            clickHandler={(e) => this.tabTo(e, 'editions')}
            total={this.state.results.editions.hits.total}
            icon="book"
          />
          <TabSwitcher
            tab="people"
            label={tcap('register_persons')}
            active={tab}
            clickHandler={(e) => this.tabTo(e, 'people')}
            total={this.state.results.people.hits.total}
            icon="user"
          />
          <TabSwitcher
            tab="places"
            label={tcap('register_places')}
            active={tab}
            clickHandler={(e) => this.tabTo(e, 'places')}
            total={this.state.results.places.hits.total}
            icon="map-marker-alt"
          />
          <TabSwitcher
            tab="works"
            label={tcap('register_works')}
            active={tab}
            clickHandler={(e) => this.tabTo(e, 'works')}
            total={this.state.results.works.hits.total}
            icon="gem"
          />
          <TabSwitcher
            tab="pages"
            label={tcap('other_sections')}
            active={tab}
            clickHandler={(e) => this.tabTo(e, 'pages')}
            total={this.state.results.pages.hits.total}
            icon="file"
          />
        </ul>
        {results.hits.total > 0 ? body : noResults}
      </div>
    )
  }

  newHash(event) {
    if (event) {
      this.setState(this.delayedApi)
    } else {
      // initial call from constructor
      this.state = this.delayedApi({})
    }
  }

  delayedApi(state, props) {
    if (state.timeout) {
      clearTimeout(state.timeout);
    }

    return {
      timeout: setTimeout(() => {
        let searchParams = Url.current().hashParams()
        if (searchParams['locale'] != 'any') {
          searchParams['locale'] = locale()
        }
        api.call(searchParams).then(this.updateResults)
      }, 300)
    }
  }

  updateResults(results) {
    this.setState({results: results})
  }
}

function TabSwitcher(props) {
  let cn = ['nav-link']
  if (props.tab == props.active) {
    cn.push('active')
  }

  let icon = (
    props.icon ?
      <i className={'fa fa-' + props.icon} /> :
      null
  )

  return(
    <li className="nav-item">
      <a
        className={cn.join(' ')}
        href="#"
        onClick={props.clickHandler}
      >{icon} {props.label} ({props.total})</a>
    </li>
  )
}
