const path = require('path')

module.exports = (env, argv) => {
  const mode = argv.mode || 'development'

  return {
    mode: 'development',
    entry: './src/main.js',
    module: {
      rules: [
        {
          test: /\.js/,
          include: [path.resolve(__dirname, 'src')],
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-react']
          }
        },
        {
          test: /\.scss/,
          include: [path.resolve(__dirname, 'src')],
          use: ['style-loader', 'css-loader', 'sass-loader']
        }
      ]
    },
    output: {
      path: path.resolve(__dirname, "public"),
      filename: 'app.js',
      publicPath: '/'
    },
    devtool: mode == 'development' ? 'eval-source-map' : false,
    devServer: {
      contentBase: path.join(__dirname, 'public'),
      port: 4000
    }
  }
}
