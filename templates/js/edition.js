/* This file contains JS function for displaying editions!
    This includes the synoptic view and the textual view.
    Functions that relate to only one of them are outsourced
    to edition_surround.js and edition_synoptic.js */

/* Test browser and version because of compatibility of
-URLSearchparams
-XMLHttpRequest
*/
function doFinalBrowserTest() {
    if ($.browser.desktop) {
        if ($.browser.chrome) {
            if ($.browser.chrome.version <= 50) {errorOldVersion();}
        }
        else if ($.browser.firefox) {
            if ($.browser.firefox.version <= 45) {errorOldVersion();}
        }
    else if ($.browser.mozilla) {
            if ($.browser.mozilla.version <= 45) {errorOldVersion();}
        }
        else if ($.browser.safari) {
            if ($.browser.safari.version <= 10) {errorOldVersion();}
        }
        else {errorUnsupportedBrowser()}
    }
    else if ($.browser.mobile) {
        /* does it work for mobile?? */
        /* migrate docs here https://github.com/gabceb/jquery-browser-plugin/blob/master/dist/jquery.browser.js and here https://github.com/gabceb/jquery-browser-plugin */
    }
    else {errorUnsupportedBrowser()}
}
doFinalBrowserTest();

function isProd() {
  return !!document.location.href.match(/architrave\.eu(?!\/dev\/)/)
}

function isStaging() {
  return !!document.location.href.match(/architrave\.eu\/dev\//)
}

function isDev() {
  const isLocalhost = !!document.location.href.match(/localhost/)
  const isNonRouted = !!document.location.href.match(/192\.168\./)
}

/* Dropdown menu: Deactivate close function when clicking on checkbox */
$("#dropdown-menu").click(function(e) {
  e.stopPropagation()
});

/* Checkbox functions for display options to trigger
visivbility of icions inside the edition text */
function toggleSVGVisibility(object) {
  if (event) {event.preventDefault()}
    
  var id = $(object).attr("id")
  var checkValue = $(object)
    .find("input")
    .is(":checked")
  let container = null
  switch (id) {
    case 'placeNames':
      container = document.querySelector('body')
      container.classList.toggle('hide-places')
      // triggerSVGVisibility(".placeName svg");
      break;
    case 'persNames':
      container = document.querySelector('body')
      container.classList.toggle('hide-persons')
      // triggerSVGVisibility(".persName svg");
      break;
    case 'workNames':
      container = document.querySelector('body')
      container.classList.toggle('hide-works')
      // triggerSVGVisibility(".ArtWork svg");
      break;
    case 'scientificComments':
      container = document.querySelector('body')
      container.classList.toggle('hide-notes')
      //triggerSVGVisibility(".scientificComment svg");
      //triggerSVGVisibility(".translationComment svg");
      //triggerSVGVisibility(".editorialComment svg");
      break;
    case 'joinWords':
      container = document.querySelector('body')
      container.classList.toggle('show-combined')
      break;
    case 'origSpelling':
      container = document.querySelector('body')
      container.classList.toggle('show-abbreviations')
    
      //triggerSVGVisibility(".abbr-yes");
      //triggerSVGVisibility(".abbr-no");
      break;
    case 'abbreviations':
      container = document.querySelector('body')
      container.classList.toggle('hide-supplied')
    
      // triggerSVGVisibility(".tei-add");
      // triggerSVGVisibility("div.body supplied");
      break;
    case 'dates':
      container = document.querySelector('body')
      container.classList.toggle('hide-dates')
      
      //triggerSVGVisibility(".tei-date svg");
      break;
    case 'refs':
      container = document.querySelector('body')
      container.classList.toggle('hide-refs')
      
      //triggerSVGVisibility(".tei-date svg");
      break;
    default:
      break;
  }
};

function showAll() {
    event.preventDefault()
        
  if ($('#placeNames .fa-check-square').css("display") != "inline-block") {
    $('#placeNames').click();
  }
  if ($('#persNames .fa-check-square').css("display") != "inline-block") {
    $('#persNames').click();
  }
  if ($('#workNames .fa-check-square').css("display") != "inline-block") {
    $('#workNames').click();
  }
  if ($('#origSpelling .fa-check-square').css("display") != "inline-block") {
    $('#origSpelling').click();
  }
  if ($('#abbreviations .fa-check-square').css("display") != "inline-block") {
    $('#abbreviations').click();
  }
  if ($('#dates .fa-check-square').css("display") != "inline-block") {
    $('#dates').click();
  }
  if ($('#refs .fa-check-square').css("display") != "inline-block") {
    $('#refs').click();
  }
  if ($('#scientificComments .fa-check-square').css("display") != "inline-block") {
    $('#scientificComments').click();
  }
    if ($('#joinWords .fa-check-square').css("display") != "inline-block") {
        console.log($('#joinWords .fa-check-square').css("display"))
    $('#joinWords').click();
  }
}

function showNone() {
    event.preventDefault()
    
    if ($('#placeNames .fa-square').css("display") != "inline-block") {
    $('#placeNames').click();
  }
  if ($('#persNames .fa-square').css("display") != "inline-block") {
    $('#persNames').click();
  }
  if ($('#workNames .fa-square').css("display") != "inline-block") {
    $('#workNames').click();
  }
  if ($('#origSpelling .fa-square').css("display") != "inline-block") {
    $('#origSpelling').click();
  }
  if ($('#abbreviations .fa-square').css("display") != "inline-block") {
    $('#abbreviations').click();
  }
  if ($('#dates .fa-square').css("display") != "inline-block") {
    $('#dates').click();
  }
  if ($('#refs .fa-square').css("display") != "inline-block") {
    $('#refs').click();
  }
  if ($('#scientificComments .fa-square').css("display") != "inline-block") {
    $('#scientificComments').click();
  }
    if ($('#joinWords .fa-square').css("display") != "inline-block") {
    $('#joinWords').click();
  }
}

/* General function to trigger visibility of an SVG */
function triggerSVGVisibility(classtype) {
  if (
    $(classtype).css("display") == "none"
  ) {
    $(classtype).css("display", "inline")
  } else {
    $(classtype).css("display", "none")
  }
}
function countCheckedCheckboxes() {
    var countCheckedCheckboxes=0;
            for (var i=0; i < ($("#filterDropdown a").length-1); i++) {
                if ($("#filterDropdown a")[i].children[1].classList.contains("checkbox-invisible")) {
                    countCheckedCheckboxes++;
                }
            }
    return countCheckedCheckboxes
}
function checkFilterShowAll(object,isChecked) {
    if ((object.id != "showAll")) {
        if ((isChecked) && (document.getElementById("showAll").children[1].classList.contains("checkbox-invisible"))) {
            document.getElementById("showAll").children[0].classList.toggle("checkbox-invisible");
            document.getElementById("showAll").children[1].classList.toggle("checkbox-invisible");
        }
        else if ((!isChecked) && (document.getElementById("showAll").children[0].classList.contains("checkbox-invisible"))) {
            var switchShowAllOn = true;
            var counterCheckedCheckboxes = countCheckedCheckboxes();
            if ((counterCheckedCheckboxes+2) == $("#filterDropdown a").length) {
                document.getElementById("showAll").children[0].classList.toggle("checkbox-invisible");
                document.getElementById("showAll").children[1].classList.toggle("checkbox-invisible");
            }
        }
    }
    else {
        console.info("show all button triggert!");
    }
}
function toggleDropdownCheckbox(object) {
    var isChecked = object.children[1].classList.contains("checkbox-invisible");
    //checkFilterShowAll(object,isChecked);
    toggleSVGVisibility(object);
    object.children[0].classList.toggle("checkbox-invisible");
    object.children[1].classList.toggle("checkbox-invisible");
}

function triggerPageIcon(status) {

  if (status) {
    $('#pageNumberLoader').css('display', 'none');
    $('#pageNumberError').css('display', 'none');
    $('#pageNumber').css('display', 'block');
  } else {
    $('#pageNumberLoader').css('display', 'block');
    $('#pageNumberError').css('display', 'none');
    $('#pageNumber').css('display', 'none');
  }

}

function triggerPageIconError(status) {
  if (status) {
    $('#pageNumberLoader').css('display', 'none');
    $('#pageNumber').css('display', 'none');
    $('#pageNumberError').css('display', 'block');
  }
}

/* Activate tooltips (only hovering!) */
$(function() {
  $('[data-toggle="tooltip"]').tooltip({
    trigger: "hover"
  });
});

/* general function to get getParameters out of URL */
function getParameter(parameterName) {
  var result = null,
    tmp = [];
  location.search
    .substr(1)
    .split("&")
    .forEach(function(item) {
      tmp = item.split("=");
      if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    });
  return result;
}

/* general function to set getParameters into URL */
function setParameter(key, value) {
  const params = new URLSearchParams(location.search);
  params.set(key, value);
  const newUrl = `${location.pathname}?${params}`
  window.history.pushState({}, '', newUrl);
  oldSearch = window.location.search
}

function space() {
    const result = document.createElement('span')
    result.append(' ')
    result.classList.add('w-white-space')
    return result
}

function replaceWhiteSpace(w) {
    const join = w.getAttribute('join')
    
    if (join == 'right' || join == 'both') {
        const n = w.nextSibling
        if (n && n.nodeType == Node.TEXT_NODE) {
            n.replaceWith(space())
        }
    }
    
    if (join == 'left' || join == 'both') {
        const p = w.previousSibling
        if (p && p.nodeType == Node.TEXT_NODE) {
            p.replaceWith(space())
        }
    }
}

function handleW(containerSelector) {
    const container = document.querySelector(containerSelector)
    
    // index tei:w to find their neighbors and icons for highlighting, attach
    // event handlers
    let pack = []
    for (const w of container.querySelectorAll('w')) {
        replaceWhiteSpace(w)
        
        if (w.getAttribute('join') == 'left') {
            pack.push(w)
            
            // add the icon
            const icon = document.createElement('span')
            const inner = document.createElement('i')
            icon.append(inner)
            inner.classList.add('fas', 'fa-arrows-alt-h')
            pack.push(icon)
            w.after(icon)
            
            // add the event listeners for hovering
            for (const e of pack) {
                const sequence = pack
                
                e.addEventListener('mouseover', () => {
                    // set the title according to global settings
                    const showCombined = document.
                        querySelector('body').
                        classList.contains('show-combined')
                    const title = (
                        showCombined ?
                        sequence.map(e => e.textContent).join(' ') :
                        sequence[0].getAttribute('n')
                    )
                    const tooltipOptions = {
                        title: title,
                        placement: 'bottom',
                        html: true,
                        boundary: 'viewport'
                    }

                    $(w).
                        tooltip('hide').
                        data('bs.tooltip', false).
                        tooltip(tooltipOptions).
                        tooltip('show')
                    
                    for (const f of sequence) {
                        f.classList.add('hover')
                    }
                })
                
                e.addEventListener('mouseout', () => {
                    $(w).tooltip('hide')
                    
                    for (const f of sequence) {
                        f.classList.remove('hover')
                    }
                })
            }
            
            pack = []
        } else {
            pack.push(w)
        }
    }
}

function handlePageFw(containerSelector) {
    const container = document.querySelector(containerSelector)
    
    const fws = container.querySelectorAll("fw[type='pageNum'], fw[type='drawNum']")
    let topPages = $(`
        <div class="row pages top">
            <div class="col left"></div>
            <div class="col center text-center"></div>
            <div class="col right text-right"></div>
        </div>
    `)[0]
    let bottomPages = $(`
        <div class="row pages bottom">
            <div class="col left"></div>
            <div class="col center text-center"></div>
            <div class="col right text-right"></div>
        </div>
    `)[0]
    
    for (const fw of fws) {
        const place = fw.getAttribute('place')
        const [v, h] = place.split(/(?=[A-Z])/).map(e => e.toLowerCase())
        const target = (v == 'top' ?
          topPages.querySelector(`.col.${h}`) :
          bottomPages.querySelector(`.col.${h}`)
        )
        
        if (target) {
            fw.remove()
            target.append(fw)
        }
    }
    
    if (!topPages.textContent.match(/^\s*$/)) {
        container.prepend(topPages)
    }
    
    if (!bottomPages.textContent.match(/^\s*$/)) {
        container.append(bottomPages)
    }
}

function handleAbRunon(containerSelector) {
    const container = document.querySelector(containerSelector)
    
    for (const abRunon of container.querySelectorAll("ab[rend='runon']")) {
        const prev = abRunon.previousElementSibling
        const rend = prev.getAttribute('rend')
        
        if (!rend) {
            prev.append(' ', abRunon)
        }
    }
}

function wrapOuterDiv(containerSelector) {
    const container = document.querySelector(containerSelector)
    container.children[0].classList.add('edition-body')
}

function modalNote(element, locale, type, typeIndex) {
    if (type == null) {return element}
    
    const iconClasses = {
        'authorial': ['far', 'fa-comment'],
        'scientific': ['fas', 'fa-comment'],
        'translation': ['fas', 'fa-comment'],
        'editorial': ['far', 'fa-comment']
    }
    
    // wrap the note element and add the icon
    const bubble = document.createElement('span')
    bubble.setAttribute('type-index', typeIndex)
    bubble.setAttribute('type', type)
    bubble.classList.add('note', 'note-' + type)
    const isvg = document.createElement('i')
    isvg.classList.add(...iconClasses[type])
    element.replaceWith(bubble)
    bubble.append(element)
    bubble.append(isvg)
    
    bubble.note = element
    
    bubble.addEventListener('click', event => {
        event.preventDefault()
        event.stopPropagation()
        
        showNote(locale, type, typeIndex)
    })
    
    return bubble
}

function showNote(locale, type, typeIndex, updateUrl=true) {
    const container = document.querySelector(
        locale === 'de' ?
        '#edition-container' :
        '#translation-container'
    )
    
    const wrapper = container.querySelector(`[type='${type}'][type-index='${typeIndex}']`)
    
    // the edition containers are loaded several times on page load, so if we
    // can't find the note, do nothing
    if (!wrapper) {return}
    
    // const note = wrapper.querySelector('note').cloneNode(true)
    const note = wrapper.note
    
    if (updateUrl) {
        const url = AtUrl.current()
        url.updateHashParams({modal: 'note', type: type, id: typeIndex, locale: locale})
        document.location.hash = url.formatHash()
    }
    
    if (type == 'authorial') {
        const title = `${translate('authorialNote')} ${typeIndex}`
        $("#ModalCommentAuthorial .modal-title").text(title)
        $("#ModalCommentAuthorialText").html(note)
        $("#ModalCommentAuthorial").modal("show")
    }
    
    if (type == 'scientific') {
        const title = `${translate('scientificNote')} ${typeIndex}`
        $("#ModalCommentScientific .modal-title").text(title)
        $("#ModalCommentScientificText").html(note)
        $("#ModalCommentScientific").modal("show")
    }
    
    if (type == 'translation') {
        const title = `${translate('translationNote')} ${typeIndex}`
        $("#ModalCommentTranslation .modal-title").text(title)
        $("#ModalCommentTranslationText").html(note)
        $("#ModalCommentTranslation").modal("show")
    }
    
    if (type == 'editorial') {
        const title = `${translate('authorialNote')} ${typeIndex}`
        $("#ModalCommentEditorial .modal-title").text(title)
        $("#ModalCommentEditorialText").html(note)
        $("#ModalCommentEditorial").modal("show")
    }
}

function tooltipNote(element, locale, type) {
  $(element).tooltip({
      title: translate(`${type}Note`),
      placement: 'bottom',
      container: '#arch-tooltips'
  })
}

function handleNotes(containerSelector) {
    const container = document.querySelector(containerSelector)
    const locale = (container.id === 'translation-container' ? 'fr' : 'de')
    const notes = container.querySelectorAll("note")
    const counts = {
        'authorial': 1,
        'scientific': 1,
        'editorial': 1,
        'translation': 1,
        null: 1
    }
    
    for (const note of notes) {
        const type = note.getAttribute('type')
        
        if (['authorial', 'scientific', 'editorial', 'translation'].includes(type)) {
            // handle as modal
            modalNote(note, locale, type, counts[type])
          counts[type] += 1
        }
        
        if (['archivist', 'secretary'].includes(type)) {
          tooltipNote(note, locale, type)
        }
    }
}

function handleSubstWhiteSpace(containerSelector) {
    const container = document.querySelector(containerSelector)
    
    const substs = container.querySelectorAll('subst')
    for (const s of substs) {
        for (const c of s.childNodes) {
            if (c.nodeType == 3) {
                c.remove()
            }
        }
    }
}

function handleChoiceWhiteSpace(containerSelector) {
    const container = document.querySelector(containerSelector)
    
    const choices = container.querySelectorAll('choice')
    for (const choice of choices) {
        for (const c of choice.childNodes) {
            if (c.nodeType == 3) {
                c.remove()
            }
        }
    }
}

function handlePersons(containerSelector) {
    const container = document.querySelector(containerSelector)
    
    const persons = container.querySelectorAll('persName')
    for (const person of persons) {
        const refs = person.getAttribute('ref').split(/\s+/)
        
        for (const ref of refs) {
            const icon = document.createElement('i')
            icon.classList.add('fas', 'fa-user')
            const iconWrapper = document.createElement('span')
            iconWrapper.append(icon)
            person.append(' ', iconWrapper)
            
            const handler = event => {
                event.stopPropagation()
                event.stopImmediatePropagation()
                
                const cleanRef = ref.replace(/^psn:/, '')
                openPersName(cleanRef)
            }
            
            if (refs.length > 1) {
                iconWrapper.addEventListener('click', handler)
            } else {
                person.addEventListener('click', handler)
            }
        }
    }
}

function handlePlaces(containerSelector) {
    const container = document.querySelector(containerSelector)
    
    const places = container.querySelectorAll('placename, geogname')
    for (const place of places) {
        const refs = place.getAttribute('ref').split(/\s+/)
        
        for (const ref of refs) {
            const icon = document.createElement('i')
            icon.classList.add('fas', 'fa-map-marker-alt')
            const iconWrapper = document.createElement('span')
            iconWrapper.append(icon)
            place.append(' ', iconWrapper)
            
            const handler = event => {
                event.stopPropagation()
                event.stopImmediatePropagation()
                
                const cleanRef = ref.replace(/^plc:/, '')
                openPlaceName(cleanRef)
            }
            
            if (refs.length > 1) {
                iconWrapper.addEventListener('click', handler)
            } else {
                place.addEventListener('click', handler)
            }
        }
    }
}

function handleWorks(containerSelector) {
    const container = document.querySelector(containerSelector)
    
    const works = container.querySelectorAll("rs[type='artwork']")
    for (const work of works) {
        const refs = work.getAttribute('ref').split(/\s+/)
        
        for (const ref of refs) {
            const icon = document.createElement('i')
            icon.classList.add('far', 'fa-gem')
            const iconWrapper = document.createElement('span')
            iconWrapper.append(icon)
            work.append(' ', iconWrapper)
            
            const handler = event => {
                event.stopPropagation()
                event.stopImmediatePropagation()
                
                const cleanRef = ref.replace(/^wrk:/, '')
                openArtWork(cleanRef)
            }
            
            if (refs.length > 1) {
                iconWrapper.addEventListener('click', handler)
            } else {
                work.addEventListener('click', handler)
            }
        }
    }
}

function handleUrlRef(ref) {
  let href = ref.getAttribute('target').
    replace('https://collections.chateauversailles.fr', 'http://collections.chateauversailles.fr').
    replace('https://balat.kikirpa.be', 'http://balat.kikirpa.be/') .
    replace('https://comitehistoire.bnf.fr', 'http://comitehistoire.bnf.fr').
    replace('https://medaillesetantiques.bnf.fr', 'http://medaillesetantiques.bnf.fr').
    replace(/^https:\/\/architrave\.eu(\/dev)?\//, '')
    
  const target = '_blank'
  
  wrapInLink(ref, href, target)
}

function wrapInLink(element, href, target = '', icon = null) {
  const a = document.createElement('a')
  a.setAttribute('href', href)
  a.setAttribute('target', target)
  
  if (icon) {
    a.append(icon)
    element.append(a)
  } else {
    element.replaceWith(a)
    a.append(...element.childNodes)
  }
}

function handleRefs(containerSelector) {
  const container = document.querySelector(containerSelector)
  const refs = container.querySelectorAll('ref')
    
  // we first fetch the data from the api and then iterate all refs
  const render = (data) => {
    for (const ref of refs) {
      const refTarget = ref.getAttribute('target').replace(/^#/, '')
      
      if (refTarget && refTarget.match(/^http/)) { // there should be a full url in the target attribute
        handleUrlRef(ref)
        continue
      }
      
      //add icon unless we are in a note
      let icon = null
      if (!ref.closest('note')) {
        icon = document.createElement('i')
        icon.classList.add('fa', 'fa-external-link-square-alt')
      }
      
      const d = data[refTarget]
      if (d) { // we are referring to some ref[target=id] in the editions
        const href = `view.html?lang=${d.locale}&edition=${d.edition}&translation=${d.translation}&page=${d.view}`
        wrapInLink(ref, href, '_blank', icon)
      }
    }
  }
    
  const url = AtUrl.current()
  const locale = url.params()['lang']
  const targets = [...refs].map(e => {
      return e.getAttribute('target').replace(/^[#\s]/, '').replace(/\s$/, '')
  }).join(',')
  
  if (targets == '') {return}
  
  fetch(`api.json?action=ref-data&locale=${locale}&targets=${encodeURIComponent(targets)}`).
    then(response => response.json()).
    then(data => {
      const result = {}
      if (data.hits.total == 0) {return result}
      
      // build a lookup map
      for (const record of data.hits.hits) {
        result[record['_source']['ref_target']] = record['_source']
      }
      return result
    }).
    then(render)
}

function handleAdds(containerSelector) {
  const container = document.querySelector(containerSelector)
    
  const adds = container.querySelectorAll('add')
  for (const add of adds) {
    const place = add.getAttribute('place')
    
    if (place != 'across') {
        const widget = $('<span> <i class="fas fa-comment-medical"/></span>')[0]
        add.append(widget)
    }
    
    $(add).tooltip({
        placement: 'bottom',
        title: translate(`add_tooltip_${place}`),
        container: '#arch-tooltips'
    })
  }
}

function handleSupplieds(containerSelector) {
    const container = document.querySelector(containerSelector)
    
    const supplieds = container.querySelectorAll('supplied')
    for (const supplied of supplieds) {
        for (const c of supplied.childNodes) {
            if (c.nodeType == 3 && c.textContent.match(/^\s*$/)) {
                c.remove()
            }
        }
    }
}

function handleLb(containerSelector) {
    const container = document.querySelector(containerSelector)
    
    const lbs = container.querySelectorAll('lb')
    for (const lb of lbs) {
        const br = document.createElement('br')
        lb.replaceWith(br)
    }
}

// function handleModalNotes(containerSelector) {
//     const container = document.querySelector(containerSelector)
//     const locale = (container.id === 'translation-container' ? 'fr' : 'de')
//     const notes = container.querySelectorAll("note")
//     const counts = {
//         'authorial': 1,
//         'scientific': 1,
//         'editorial': 1,
//         'translation': 1,
//         null: 1
//     }
    
//     for (const note of notes) {
//         const type = note.getAttribute('type')
//         wrapNote(note, locale, type, counts[type])
//         counts[type] += 1
//     }
// }

function handleMath(containerSelector) {
    const container = document.querySelector(containerSelector)
    
    const nums = container.querySelectorAll('num')
    for (const num of nums) {
        const type = num.getAttribute('type')
        const rend = num.getAttribute('rend')
        
        // replace illegeble stuff
        const illegibles = num.querySelectorAll('unclear[cert=unknown][reason=illegible]')
        for (const illegible of illegibles) {
            illegible.replaceWith('[?]')
        }
        
        if (type == 'fraction' && rend == 'horizontal') {
            const wrapper = document.createElement('span')
            wrapper.classList.add('tei-fraction-horizontal')
            wrapper.innerHTML = `
                <math xmlns="http://www.w3.org/1998/Math/MathML" display="inline">
                    <mrow>
                        <mfrac>
                            <mrow>
                                <mn>${num.textContent.split('/')[0]}</mn>
                            </mrow>
                            <mrow>
                                <mn>${num.textContent.split('/')[1]}</mn>
                            </mrow>
                        </mfrac>
                    </mrow>
                </math>
            `
            
            num.replaceWith(wrapper)
            continue
        }
        
        if (type == 'fraction' && rend == 'diagonal') {
            const wrapper = document.createElement('span')
            wrapper.classList.add('tei-fraction-oblique')
            wrapper.innerHTML = `
                <sup>${num.textContent.split('/')[0]}</sup>/<sup>${num.textContent.split('/')[1]}</sup>
            `
            
            num.replaceWith(wrapper)
            continue
        }
    }
    
    // it keeps track of what it already transformed, so it can be called
    // repeatedly
    MathJax.typeset()
}

function handleColumns(containerSelector) {
    const container = document.querySelector(containerSelector)
    
    const cbs = container.querySelectorAll("cb:first-of-type")
    for (const cb of cbs) {
        const parent = document.createElement('div')
        parent.classList.add('parent')
        
        let e
        while (e = cb.nextSibling) {
            parent.append(e)
        }
        cb.replaceWith(parent)
        parent.prepend(cb)
        
        // get an array of all children, so that we can move the elements around
        // in the dom without loosing reference to them, also find the indices
        // of all the connected cb elements in the list
        // const parent = cb.parentElement
        const children = [...parent.childNodes.values()]
        const otherCbs = parent.querySelectorAll('cb')
        const indices = [...otherCbs].map(otherCb => {
            return children.indexOf(otherCb)
        })
        
        // if we can't find any of the cbs in the children elements, we do
        // nothing
        const hasMissingIndices = indices.some(e => e == -1)
        if (hasMissingIndices) {return}
        
        // build the row
        const row = document.createElement('div')
        row.classList.add('row')
        
        for (let i = 0; i < indices.length; i++) {
            const elements = children.slice(indices[i], indices[i + 1])
            
            // add the column
            const col = document.createElement('div')
            col.classList.add('col')
            col.append(...elements)
            row.append(col)
        }
        
        // add left-over items before the new row
        if (indices[0] != 0) {
            row.prepend(...children.slice(0, indices[0]))
        }
        
        parent.replaceWith(row)
    }
}

function attachDateTooltip(date) {
    $(date).tooltip({
        title: date.getAttribute('when').split('-').reverse().join('-'),
        placement: 'bottom',
        trigger: 'hover focus',
        container: '#arch-tooltips'
    })
}

function handleDates(containerSelector) {
    const container = document.querySelector(containerSelector)
    
    const dates = container.querySelectorAll("date")
    for (const date of dates) {
        const icon = document.createElement('i')
        icon.classList.add('far', 'fa-calendar-alt')
        date.append(' ', icon)
        
        // is there a br element in the date?
        let beforeBr = []
        let br = null
        let afterBr = []
        for (const node of date.childNodes) {
            if (br) {
                afterBr.push(node)
                continue
            }
            
            if (node && node.tagName == 'BR') {
                br = node
            } else {
                beforeBr.push(node)
            }
        }
        
        // if there is a br, remove all preceding nodes from the date and push
        // them into their own date element. Add the same tooltip as for the
        // original
        if (br) {
            const pseudoDate = document.createElement('date')
            pseudoDate.setAttribute('when', date.getAttribute('when'))
            pseudoDate.append(...beforeBr)
            date.parentElement.insertBefore(pseudoDate, date)
            date.parentElement.insertBefore(br, date)
            
            attachDateTooltip(pseudoDate)
        }
        
        attachDateTooltip(date)
    }
}

function handleFigDescNotes(containerSelector) {
    const container = document.querySelector(containerSelector)
    
    const figDescs = container.querySelectorAll('figure figDesc')
    for (const figDesc of figDescs) {
        const note = figDesc.nextElementSibling
        if (note && note.tagName == 'NOTE') {
            figDesc.append(note)
        }
    }
}

function handleChoiceOutSideFw(containerSelector) {
    const container = document.querySelector(containerSelector)
    
    const choices = container.querySelectorAll(':not(fw) > choice')
    for (const choice of choices) {
        const sic = choice.querySelector('sic')
        const corr = choice.querySelector('corr')
        if (!corr) {continue}
        
        $(sic).tooltip({
            title: corr.textContent,
            placement: 'bottom',
            container: '#arch-tooltips'
        })
    }
}

function handleFrenchWhiteSpace(container) {
    if (typeof container == 'string') {
        const element = document.querySelector(container)
        return handleFrenchWhiteSpace(element)
    }
    
    for (const node of container.childNodes) {
        if (node && node.nodeType == Node.TEXT_NODE) {
            node.textContent = fixNonBreakingWhitespace(node.textContent)
        } else {
            handleFrenchWhiteSpace(node)
        }
    }
}

function dropTooltips() {
  const element = document.querySelector('#arch-tooltips')
  element.innerHTML = ''
}

function handleSpecial(containerSelector) {
    const container = document.querySelector(containerSelector)
    
    const url = AtUrl.current()
    const edition = url.params()['edition']
    const view = url.params()['page']
    const locale = url.params()['lang']
    
    if (edition == '34zs7') { // sturm, this code is run for both containers
        if (view == '101') {
            // see #296
            const ref = container.querySelector('ref')
            ref.setAttribute('target', ref.getAttribute('target').split(' ')[0])
        }
        
        // see #295
        if (locale == 'de') {
            if (view == '73') {
                const hi = container.
                    querySelector("rs[ref='wrk:textgrid:3q7d5 wrk:textgrid:3qkv8']").
                    nextElementSibling
                const br = document.createElement('br')
                hi.before(br)
            }
            
            if (view == '84') {
                const placename = container.
                    querySelector("placename[ref='plc:textgrid:3pg0p']")
                const br = document.createElement('br')
                placename.after(br)
            }
        }
    }
}

function augment(containerSelector) {
    handleSpecial(containerSelector)
    
    dropTooltips()
    wrapOuterDiv(containerSelector)
    
    handleW(containerSelector)
    handleLb(containerSelector)
    handlePageFw(containerSelector) // #145
    handleAbRunon(containerSelector) // #147
    handleSubstWhiteSpace(containerSelector) // #119 and many others
    handleChoiceWhiteSpace(containerSelector)
    handleChoiceOutSideFw(containerSelector)
    handleDates(containerSelector)
    handleRefs(containerSelector)
    handlePersons(containerSelector)
    handlePlaces(containerSelector)
    handleWorks(containerSelector)
    handleAdds(containerSelector)
    handleSupplieds(containerSelector)
    handleColumns(containerSelector)

    handleFigDescNotes(containerSelector)    
    handleNotes(containerSelector) // #153, #175, #185
    
    handleMath(containerSelector)
    
    handleFrenchWhiteSpace(containerSelector)
    
    recreateModals()
}

function augmentInterface() {
    const optionsElement = document.querySelector('#options-dropdown-control')
    
    if (optionsElement) {
        optionsElement.addEventListener('mouseenter', event => {
            $(optionsElement).tooltip({
                title: translate('displayOptions'),
                placement: 'bottom'
            }).tooltip('show')
        })
        optionsElement.addEventListener('mouseleave', event => {
            $(optionsElement).tooltip('hide')
        })
    }
}
$(document).ready(event => {
    augmentInterface()
})

/* The following functions relate to pagination */

function setContent(container, data) {
  // replace tei head elements, as the browser replaces it when inserted outside
  // of its predefined position
  const htmlSafe = data.replaceAll(/<(\/)?head( |>)/g, '<$1tei-head$2')
  $(container).html(htmlSafe)
  
  triggerPageIcon(true);
  
  var number = sessionStorage.getItem('number');
  document.getElementById('pageNumber').innerHTML = number;
  sessionStorage.setItem('pbNumbers', document.getElementById('pbNumbers').innerHTML);
  sessionStorage.setItem('fwNumbers', document.getElementById('fwNumbers').innerHTML);
  checkForPossibleNeighbourPages(number);

  pastTransLoad(container,number);
}

function setEditionContent(data) {
    setContent("#edition-container",data);
    
    // augment html for display, adding functionality and structure that would
    // be difficult to add with xquery
    augment("#edition-container")
}
function setTranslationContent(data) {
    setContent("#translation-container", data);
    
    // augment html for display, adding functionality and structure that would
    // be difficult to add with xquery
    augment("#translation-container")
}
function setCodeContent(data) {
    $('#code-container').html(data);
  $('#code-container').parent().css("overflow-y", "scroll");
}

function fetchContent(url, functionAfterResponse) {
    fetch(url, {
        method: 'GET',
        credentials: 'include',
        mode: 'no-cors'
    })
    .then(function(response) {
        if (response.ok) {
            return response.text();
        }
        else if (response.status == 404){
            triggerPageIconError(true);
            console.log("reponse 404");
            errorFile404();
        }
        else {
            triggerPageIconError(true);
            console.log("reponse error");
            errorFileLoad(response.status);
        }
    }).then(function(string) {
        functionAfterResponse(string);
    }).catch(function(err) {
        console.log('Fetch Error', err);
    });
}

function getContent(url, functionAfterResponse, timeoutDuration) {
    /* TODO: handle timeout */

    new Promise(function(resolve, reject) {
        fetchContent(url, functionAfterResponse);
    });
}

function loadTrans(source, number, container) {
  sessionStorage.setItem('number', number);
  var url = `content.xml?edition=${source}&page=${number}`
  if (container == "#edition-container") {
      getContent(url, setEditionContent, 5000);
      sessionStorage.setItem('edition', source);
  }
  else if (container == "#translation-container") {
      getContent(url, setTranslationContent, 5000);
      sessionStorage.setItem('translation', source);
  }
  else {console.log("error: no such container")}
}

/* Check if the pagenumber coming from
    getParamter is a real number */
function checkNumber(value) {
  var er = /^-?[0-9]+$/
  return er.test(value)
}

/* Sets the new page in URL and triggers updatePage() to
    load contents if page check was OK*/
function setNewPage(newPage) {
  if (checkNumber(newPage)) {
    setParameter('page', newPage);
    triggerPageIcon(false);
    setBtnPage('forward', false);
        setBtnPage('back', false);
    updatePage();
  } else {
    console.log("Error: Page number is not a valid number");
  }
}

/* Updates page contents. Triggers loadPage() which is defined
    in edition_synoptic and edition_surround differently */
function updatePage() {
  var currentPage = getParameter('page');
  loadPage(getParameter('edition'), currentPage);
}

/* function to browse a page forward */
function pageForward() {
  var currentPage = getParameter('page');
  var temp = parseInt(currentPage) + 1;
  var newPage = temp.toString();
  setNewPage(newPage);
}

/* function to browse a page back */
function pageBack() {
  var currentPage = getParameter('page');
  var temp = parseInt(currentPage) - 1;
  var newPage = temp.toString();
  setNewPage(newPage);
}

/* Do not show buttons (Arrows) for
    next/before page if its the last or first page.
    checkForPossibleNeighbourPages triggers this
    function*/
function setBtnPage(btn, status) {
  if (btn == 'forward') {
    btn = '#btn-page-forward'
  } else if (btn == 'back') {
    btn = '#btn-page-back'
  } else {
    console.log('error: no such button')
  }

  if (status == true) {
    $(btn).removeClass("arch-icon-disabled");
  } else if (status == false) {
    $(btn).addClass("arch-icon-disabled");
  } else {
    console.log('error: no such status')
  }
}

/* checks if were on the first or last page.
    If so it should not be possible to go back
    or forward*/
function checkForPossibleNeighbourPages(number) {
    setTimeout(function() {
        try {
          var allNumbers = sessionStorage.getItem('pbNumbers').split(',');
          var first = allNumbers[0];
          var last = allNumbers[allNumbers.length - 1];

          setBtnPage('forward', true);
          setBtnPage('back', true);

          if (number == first) {
            setBtnPage('back', false);
          } else if (number == last) {
            setBtnPage('forward', false);
          } else {}
        }
        catch (e) {
            errorGeneral(e);
        }
    }, 200);
}

/* Adds all page numbers to the dropdownmenu */

/* Adds one page element */
function addLiElement(number, fw) {
    const suffix = (fw ? ` (${fw})` : '' )
    
  $('#ulPageNumbers').append('<li class="pageElement d-flex justify-content-center"><a href="#" onclick="setNewPage(' + number + ');this.blur();return false;"><span>' + number + suffix + '</span></a></li>');
}

/* deletes all page numbers to enable adding new ones */
function deleteAllLis() {
  var ul = document.getElementById('ulPageNumbers');
  if (ul) {
    while (ul.firstChild) {
      ul.removeChild(ul.firstChild);
    }
  }
}

/* get Items (page numbers) from the json api and
    and adds for each page a element to the
    dropdown menu. */
function addAllPages() {
    /* in both language interfaces, the fr version is the "translation" while
       the de version is the "edition", when the interface language is changed
       to de, the "edition" is simply shown as by default */
    const edition = getParameter('lang') == 'de' ?
        getParameter('edition') :
        getParameter('translation')
    
    fetch(`api.json?action=page-numbers&edition=${edition}`).
        then(response => response.json()).
        then(data => {
            // console.log(data)
            
            deleteAllLis()
            for (const page of data) {
                // index=pb/@n, original=fw
                addLiElement(page['view'], page['page'])
            }
        })
    
    /* timeout is important, because it takes
    some time because page numbers are loaded afterwards*/
  /* setTimeout(function() {
    deleteAllLis();
    var allNumbers;
    var allFWNumbers;
    var lang = getParameter("lang");
    var pages = "Pages";
    if (lang == "de") {pages = "Seiten"}
    else if (lang == "fr") {pages = "Pages"}
    try {
        allNumbers = sessionStorage.getItem('pbNumbers').split(',');
        allFWNumbers = sessionStorage.getItem('fwNumbers').split(',');
        // $('#ulPageNumbers').append('<li class="pageElement dropdown-header"><strong><span>'+pages+'</span></strong></li>');
        for (var i = 0; i < allNumbers.length; i++) {
          addLiElement(allNumbers[i], allFWNumbers[i])
        }
    }
    catch (e) {
        errorFile404();
    }
  }, 1000);*/
}

/* View in fullscreen */
var elem = document.documentElement;

function openFullscreen() {
  if (elem.requestFullscreen) {
    elem.requestFullscreen();
  } else if (elem.mozRequestFullScreen) {
    /* Firefox */
    elem.mozRequestFullScreen();
  } else if (elem.webkitRequestFullscreen) {
    /* Chrome, Safari and Opera */
    elem.webkitRequestFullscreen();
  } else if (elem.msRequestFullscreen) {
    /* IE/Edge */
    elem.msRequestFullscreen();
  }
}

/* Close fullscreen */
function closeFullscreen() {
  if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.mozCancelFullScreen) {
    /* Firefox */
    document.mozCancelFullScreen();
  } else if (document.webkitExitFullscreen) {
    /* Chrome, Safari and Opera */
    document.webkitExitFullscreen();
  } else if (document.msExitFullscreen) {
    /* IE/Edge */
    document.msExitFullscreen();
  }
}
/* Fullscreen button: toggles top navbar and footer + container-fluid */
function triggerFullscreenButton() {
  $('#icon-fullscreen svg').toggleClass("fa-expand");
  $('#icon-fullscreen svg').toggleClass("fa-compress");

  /* Hide/Show footer, top navbar and wrapper element */
  var footer = document.getElementById("footer");
  footer.style.display = footer.style.display === "none" ? "" : "none";
  var navbar = document.getElementById("header");
  navbar.style.display = navbar.style.display === "none" ? "" : "none";
}

function fullscreen() {
  if ($('#icon-fullscreen svg').hasClass("fa-expand")) {
    openFullscreen();
  } else {
    closeFullscreen();
  }
  if ($("#goldenlayout").length > 0) {
      setTimeout(function() {
            setGoldenHeight();
        }, 400);
  }
}
if (document.addEventListener) {
  document.addEventListener('webkitfullscreenchange', exitHandler, false);
  document.addEventListener('mozfullscreenchange', exitHandler, false);
  document.addEventListener('fullscreenchange', exitHandler, false);
  document.addEventListener('MSFullscreenChange', exitHandler, false);
}

function exitHandler() {
  if (document.webkitIsFullScreen === false) {
    triggerFullscreenButton();
  } else if (document.mozFullScreen === false) {
    triggerFullscreenButton();
  } else if (document.msFullscreenElement === false) {
    triggerFullscreenButton();
  } else if (document.webkitIsFullScreen === true) {
    triggerFullscreenButton();
  } else if (document.mozFullScreen === true) {
    triggerFullscreenButton();
  } else if (document.msFullscreenElement === true) {
    triggerFullscreenButton();
  }
}

function toggleLoader() {
  var yourUl = document.getElementById("loader");
  yourUl.style.display = yourUl.style.display === 'none' ? '' : 'none';
  yourUl = document.getElementById("loaderWrapper");
  yourUl.style.display = yourUl.style.display === 'none' ? '' : 'none';
}

/* Increase/Decrease Font-Size Buttons */
function getCurrentFontSize() {
    var currentFontSize;
  if ($("#goldenlayout").length == 0) {
     currentFontSize = localStorage.getItem("fontSizeSurround");
  }
  else {
      currentFontSize = localStorage.getItem("fontSizeSynoptic");
  }
  return currentFontSize;
}
function setCurrentFontSize(input) {
  if ($("#goldenlayout").length == 0) {
     localStorage.setItem("fontSizeSurround",input);
  }
  else {
      localStorage.setItem("fontSizeSynoptic",input);
  }
}
function changeFont(change) {
  var currentFontSize = getCurrentFontSize();
  if ($("#goldenlayout").length == 0) {
     currentFontSize = localStorage.getItem("fontSizeSurround");
  }
  else {
      currentFontSize = localStorage.getItem("fontSizeSynoptic");
  }
  var newFontSize;
  if (currentFontSize == null) {
    currentFontSize = "140"
  }

  newFontSize = (parseInt(currentFontSize) + change).toString();
  setCurrentFontSize(newFontSize);
  newFontSize = newFontSize + "%"
  if ($("#translation-container").length > 0) {
      $("#translation-container").css("font-size", newFontSize);
  }
  if ($("#edition-container").length > 0) {
      $("#edition-container").css("font-size", newFontSize);
  }
}
function setFontSize() {
    try {
        var fontSize = getCurrentFontSize();
        fontSize = fontSize + "%"
        if ($("#translation-container").length > 0) {
          $("#translation-container").css("font-size", fontSize);
      }
      if ($("#edition-container").length > 0) {
          $("#edition-container").css("font-size", fontSize);
      }
    }
    catch (e) {console.log(e)}
}

function resetFont() {
    var defaultvalue;
    if ($("#goldenlayout").length > 0) {
        defaultvalue = 120;
    }
    else {
        defaultvalue = 140;
    }
    defaultvalue = defaultvalue + "%"
  if ($("#edition-container").length > 0) {
      $("#edition-container").css("font-size", defaultvalue);
  }
  if ($("#translation-container").length > 0) {
      $("#translation-container").css("font-size", defaultvalue);
    }
  setCurrentFontSize(defaultvalue);
}

function increaseFont() {
  changeFont(10);
}

function decreaseFont() {
  changeFont(-10);
}
resetFont();

function loadBrowserHistory() {
  updatePage();
}

/* A click on FA-icons and -links displays a modal
function openRegister(textGridURI) {
  $("#myModalLabel").text(textGridURI);
  $("#myModal").modal("show");
}
*/

function openPersName(textGridURI, updateUrl=true) {
  if (event) {
      event.preventDefault()
  }
  
  if (textGridURI.includes('textgrid')) {
      textGridURI = textGridURI.split('textgrid:')[1]
  }
  
  resetModalRegisterPersonsDefault()
  getRestAPIPerson(textGridURI)
  
  if (updateUrl) {
      const url = AtUrl.current()
      url.updateHashParams({modal: 'person', id: textGridURI})
      document.location.hash = url.formatHash()
  }
  
  $('#modalRegisterPersons').modal('show')
}

function openPlaceName(textGridURI, updateUrl=true) {
  if (event) {
      event.preventDefault()
  }
    
  if (textGridURI.includes('textgrid')) {
      textGridURI = textGridURI.split('textgrid:')[1]
  }
  
  resetModalRegisterPlacesDefault()
  getRestAPIPlaces(textGridURI)
  
  if (updateUrl) {
      const url = AtUrl.current()
      url.updateHashParams({modal: 'place', id: textGridURI})
      document.location.hash = url.formatHash()
  }
  
  $('#modalRegisterPlaces').modal('show');
}

function openArtWork(textGridURI, updateUrl=true) {
  if (event) {
      event.preventDefault()
  }    

  if (textGridURI.includes('textgrid')) {
      textGridURI = textGridURI.split('textgrid:')[1]
  }
  
  resetModalRegisterWorksDefault()
  getRestAPIWorks(textGridURI)
  
  if (updateUrl) {
      const url = AtUrl.current()
      url.updateHashParams({modal: 'work', id: textGridURI})
      document.location.hash = url.formatHash()
  }
  
  $('#modalRegisterWorks').modal('show')
}

/*
function openScientificComment(event) {
  const comment = $(event.currentTarget).find('.text')
    
  $("#ModalCommentScientificText").html(comment.html());
    $("#ModalCommentScientific").modal("show");
}

function openTranslationComment(comment) {
  $("#ModalCommentTranslationText").text(comment);
    $("#ModalCommentTranslation").modal("show");
}

function openEditorialComment(comment) {
  $("#ModalCommentEditorialText").text(comment);
    $("#ModalCommentEditorial").modal("show");
}
*/

/* add tooltip to one of the edition navbar icons */
$(document).ready(event => {
    const url = AtUrl.current()
    const locale = url.params()['lang']
    const title = {
        fr: "Télécharger l’édition",
        de: "Download Edition"
    }[locale]
    
    const tooltip = $('.arch-download-dropdown > a').tooltip({
        title: title,
        placement: 'bottom',
        trigger: 'hover'
    })
})

/* for modal re-recreation on page load */

// remove url hash params when dialog is closed
$('.modal').on('hide.bs.modal', event => {
    const url = AtUrl.current()
    url.updateHashParams({modal: null, type: null, id: null, locale: null})
    history.replaceState(null, null, url.formatHash())
})

// don't scroll when closing the modal
$('.modal [data-dismiss=modal]').on('click', event => {
    event.preventDefault()
})


function recreateModals() {
    const url = AtUrl.current()
    const params = url.hashParams()
    
    if (params['modal'] == 'place') {
        openPlaceName(params['id'], false)
    }
    
    if (params['modal'] == 'person') {
        openPersName(params['id'], false)
    }
    
    if (params['modal'] == 'work') {
        openArtWork(params['id'], false)
    }
    
    if (params['modal'] == 'note') {
        const locale = url.params()['lang']
        const noteLocale = url.hashParams()['locale']
        
        // when the user used the second edition panel and opens a note from
        // there, we are redirecting to the other language
        if (locale != noteLocale) {
            url.updateParams({lang: noteLocale})
            document.location.href = url.url()
        } else {
            showNote(noteLocale, params['type'], params['id'], false)
        }
    }
}
/* end modal handling */

/* Key events */
function checkValidPage(change) {
    change = parseInt(change);
    var availablePages = sessionStorage.getItem("pbNumbers").split(',');
    var currentPage = sessionStorage.getItem("number");
    try { currentPage = parseInt(currentPage); }
    catch (e) { console.log(e) }
    currentPage = (currentPage + change).toString();
    if (availablePages.includes(currentPage)) {
        return true;
    }
    else { return false; }
}
document.onkeydown = checkKey;
function checkKey(e) {

    e = e || window.event;

    /* For pageination */
    if (e.keyCode == '37') {
        if (checkValidPage(-1)) {
            pageBack();
        }
        else {}
    }
    else if (e.keyCode == '39') {
        if (checkValidPage(+1)) {
            pageForward();
        }
        else {}
    }
    /* else if (e.keyCode == '38') {
        console.log("up arrow");
    }
    else if (e.keyCode == '40') {
        console.log("down arrow");
    }*/

}
function getMetadata() {
  try {
    document.querySelector("#edition-dateCreated").innerText = document.querySelector("#tei-meta-dateCreated").innerText;
  } catch (e) {
    console.log('error: no edition-dateCreated');
  }
  try {
    document.querySelector("#edition-dateModified").innerText = document.querySelector("#tei-meta-dateModified").innerText;
  } catch (e) {
    console.log('error: no edition-dateModified');
  }
  try {
    const tg_uri = document.querySelector("#tei-meta-textGridURI").innerText
    const tg_uri_link = "https://textgridrep.org/"+tg_uri
    document.querySelector("#edition-textGridURI").innerHTML = '<a target="_blank" href="'+tg_uri_link+'">'+tg_uri+'</a>';
  } catch (e) {
    console.log('error: no edition-textGridURI');
  }
  try {
    const hdl = document.querySelector("#tei-meta-pid").innerText
    const hdl_link = "https://hdl.handle.net/" + hdl.split("hdl:")[1]
    document.querySelector("#edition-PID").innerHTML = '<a target="_blank" href="'+hdl_link+'">'+hdl+'</a>';
  } catch (e) {
    console.log('error: no edition-PID');
  }
}


function copyElementTextFrom(sourceElementSelector, targetElementSelector) {
  const source = document.querySelector(sourceElementSelector)
  if (!source) {
    console.log(`couldn't find element with id '${sourceElementSelector}'`)
    return
  }
  
  const target = document.querySelector(targetElementSelector)
  if (!target) {
    console.log(`couldn't find element for selector '${targetElementSelector}'`)
    return
  }
  
  target.innerText = source.innerText
}

function setMainTitle() {
  const url = AtUrl.current()
  const locale = url.params()['lang']
  
  document.querySelector('#editionMainTitle').textContent = window.metadata.title
  document.querySelector('#editionMainAuthor').textContent = window.metadata.author
  
  //copyElementTextFrom(`#tei-title-main-${locale}`, '#editionMainTitle')
  //copyElementTextFrom("#tei-author", '#editionMainAuthor')
}

function setCitation() {
    var citePre = "ARCHITRAVE";
    var citeTitle = window.metadata.title;
    var citePost = "";
    var citefinal = "";
    var citeHandle = "Error: no handle";
    var citeYear = ' ' + (new Date(window.metadata.lastModified)).getUTCFullYear()
    var citeModified = "Error: no modification date";
    var lang = window.metadata.lang;
    var citeTEMP = '';
//     try {
//         if (getParameter("lang") == 'de') {
//             lang = 'de';
//         }
//     } catch (e) {
//     console.log('error: could not check for lang');
//   }
//     try {
//     citeModified = document.querySelector("#tei-meta-dateModified").innerText;
//   } catch (e) {
//     console.log('error: no edition-dateModified');
//   }

//     try {
//     citeHandle = document.querySelector("#tei-meta-pid").innerText;
//   } catch (e) {
//     console.log('error: no edition-PID');
//   }

//   try {
//     citeTitle = document.querySelector(`#tei-title-main-${locale}`).innerText.replace(/\n/, "");
//   } catch (e) {
//     console.log('error: no edition-title');
//   }

  if (lang == 'de') {
      citeTEMP = ': ';
      document.querySelector("#infoCiteTitle").style.fontStyle = 'inherit';
  } else {
      citeTEMP = '\xa0: ';
      document.querySelector("#infoCiteTitle").style.fontStyle = 'italic';
    //different title for french translations
    // try {
    //     citeTitle = document.querySelector("#tei-title-alt-fr").innerText.replace(/\n/, "");
    //   } catch (e) {
    //     console.log('error: no edition-title');
    //   }
  }

  citefinal = citePre + citeTEMP + citeTitle + ', ' + citeYear + '. ' + citePost;
    document.querySelector("#infoCite").innerText = citefinal;
    document.querySelector("#infoCitePre").innerText = citePre + citeTEMP;
    document.querySelector("#infoCiteTitle").innerText = citeTitle + ',';
    document.querySelector("#infoCitePost").innerText = citeYear + '. ' + citePost;
}

function setDownloadLinks() {
    var preLink = "/textgrid/data/";
    if (window.location.href.includes("dev")) {
        preLink = "/dev"+preLink;
    }
    var postLink = ".xml";
    var textGridURIDE = getParameter("edition");
    var textGridURIFR = getParameter("translation");
    var downloadLinkDE = preLink+textGridURIDE+postLink;
    var downloadLinkFR = preLink+textGridURIFR+postLink;
    var downloadTextPre = "ARCHITRAVE_Edition_";
    var downloadTextPost = ".xml";
    var downloadText = downloadTextPre+document.getElementById('navbarAuthorShort').innerHTML;
    var downloadElementDE = document.getElementById("downloadLinkDE");
    var downloadElementFR = document.getElementById("downloadLinkFR");

    downloadElementDE.setAttribute("href", downloadLinkDE);
    downloadElementDE.setAttribute("download", downloadText+"_de"+downloadTextPost);

    downloadElementFR.setAttribute("href", downloadLinkFR);
    downloadElementFR.setAttribute("download", downloadText+"_fr"+downloadTextPost);
}

/* make browser back-button work */   
let oldSearch = window.location.search
window.addEventListener('popstate', event => {
    console.log('popstate')
    console.log(oldSearch, window.location.search)
    if (oldSearch != window.location.search) {
        updatePage()
        oldSearch = window.location.search
    }
})

// preload metadata for current edition
window.addEventListener('DOMContentLoaded', event => {
    const url = AtUrl.current()
    const locale = url.params()['lang']
    
    const edition = (locale == 'de' ?
      url.params()['edition'] :
      url.params()['translation']
    )
    
    fetch(`api.json?action=metadata&edition=${edition}`).
        then(response => response.json()).
        then(data => window.metadata = data)
})
