/* Image Viewer Rezise */
function resetImageViewer() {
  var imgContainerParentHeight = $('#container-img').parent('.lm_content').css('height');
  var imgContainerParentWidth = $('#container-img').parent('.lm_content').css('width');
  var imgContainerWidth;
  var imgContainerHeight;
  var margin = 20;

  if (imgContainerParentHeight > imgContainerParentWidth) {
    imgContainerWidth = (parseInt(imgContainerParentWidth) - margin).toString() + 'px';
    $('#container-img').css('width', imgContainerHeight);
    $('#container-img').css('height', '96%');
  } else {
    imgContainerHeight = (parseInt(imgContainerParentHeight) - margin).toString() + 'px';
    $('#container-img').css('height', imgContainerHeight);
    $('#container-img').css('width', '96%');
  }
  viewer.refresh();
}
function setGoldenHeight() {
    var marginNavbarTop = $("body header").css("height");
    if ($("body header").css("display") == "none") {
        marginNavbarTop = 0;
    }
    var marginEditionTopBar = $("#editionNavBar").css("height");
    var marginAdditional = 26;
    var margin = parseInt(marginNavbarTop)+parseInt(marginEditionTopBar)+parseInt(marginAdditional);
    var height = "calc(100vh - "+margin.toString()+"px)";
    $("#goldenlayout").css("height",height);
    myLayout.updateSize();
}

/* Top Bar Functions */

/* Other Buttons */
function toggleBtnColor(section, status) {
  colorOn = '#c90057';
  colorOff = '#EEE9DF';
  if (status == 'on') {
    color = colorOn
  } else if (status == 'off') {
    color = colorOff
  }
  $(section).css('border-bottom-color', color);
}

function countActiveComponents() {
  var result = 0;
  result = myLayout.root.getComponentsByName('facs').length + myLayout.root.getComponentsByName('tran').length + myLayout.root.getComponentsByName('translation').length + myLayout.root.getComponentsByName('code').length;

  return result;
}

function attachResizeEvents() {
  var draghandlers = document.getElementsByClassName("lm_drag_handle");
  addEventListenersRezise(draghandlers);
}

function toggleComponent(section) {
  var countComponents = countActiveComponents();
  var sectionbutton = '#btn-' + section;
  if (myLayout.root.getItemsById(section)[0] == undefined) {
    goldenActivate(section);
    toggleBtnColor(sectionbutton, 'on');
  } else {
    if ((section == 'tran') && (myLayout.root.getComponentsByName('tran').length == 1) && (myLayout.root.getComponentsByName('translation').length == 0)) {
      errorGoldenEditionOrTranslation();
    } else if ((section == 'translation') && (myLayout.root.getComponentsByName('translation').length == 1) && (myLayout.root.getComponentsByName('tran').length == 0)) {
      errorGoldenEditionOrTranslation();
    } else if (parseInt(countComponents) > 2) {
      goldenDestroy(section);
      toggleBtnColor(sectionbutton, 'off');
    } else {
      errorGoldenTwoComponents();
    }
  }
  resetImageViewer()
  
  attachResizeEvents()
}

function btnFacs() {
  toggleComponent('facs');
}

function btnEdition() {
  toggleComponent('tran');
}

function btnTranslation() {
  toggleComponent('translation');
}

function btnCode() {
  toggleComponent('code');
}

/* JS Code for goldenLayout */

var facs = {
  type: 'component',
  width: 30,
  componentName: 'facs',
  id: 'facs',
  isClosable: false,
  title: '<i class="fas fa-2x fa-fw fa-image"></i>',
  componentState: {
    label: "<div style='text-align:center; margin:auto; width: 96%; top: 50%; transform: translateY(-50%); position: relative;height: calc(100vh - 290px);' id='container-img'></div><script src='templates/js/imageviewer.min.js'/><script>var viewer = ImageViewer('#container-img',{zoomValue:100,maxZoom:1500})</script>"
  }
}
var tran = {
  type: 'component',
  width: 30,
  componentName: 'tran',
  id: 'tran',
  isClosable: false,
  minItemWidth: 50,
  title: '<i class="fas fa-2x fa-fw fa-align-left"></i>',
  componentState: {
    label: '<h1>B</h1>'
  }
}
var translation = {
  type: 'component',
  componentName: 'translation',
  id: 'translation',
  isClosable: false,
  title: '<i class="fas fa-2x fa-fw fa-exchange-alt"></i>',
  componentState: {
    label: '<h1>D</h1>'
  }
}
var code = {
  type: 'component',
  componentName: 'code',
  id: 'code',
  isClosable: false,
  title: '<i class="fas fa-2x fa-fw fa-code"></i>',
  componentState: {
    label: 'C'
  }
}
var config = {
  settings: {
    hasHeaders: true,
    constrainDragToContainer: false,
    reorderEnabled: true,
    selectionEnabled: false,
    popoutWholeStack: false,
    blockedPopoutsThrowError: true,
    closePopoutsOnUnload: true,
    showPopoutIcon: false,
    showMaximiseIcon: false,
    showCloseIcon: false
  },
  dimensions: {
    borderWidth: 5,
    minItemHeight: 10,
    minItemWidth: 10,
    headerHeight: 20,
    dragProxyWidth: 300,
    dragProxyHeight: 200
  },
  labels: {
    close: 'close',
    maximise: 'maximise',
    minimise: 'minimise',
    popout: 'open in new window'
  },

  content: [{
    type: 'row',
    id: 'no-drop-target',
    content: [facs, tran]
  }],

  id: 'goldenlayout',
  isClosable: true,
  title: 'some title',
  activeItemIndex: 1
};

var myLayout = new GoldenLayout(config, $('#goldenlayout'));

myLayout.registerComponent('facs', function(container, componentState) {
  container.getElement().html(componentState.label);
  $('#btn-facs').css('border-bottom-color', '#c90057');
});

myLayout.registerComponent('code', function(container, componentState) {
  var newElement = '<div class="" id="code-container"/>';
  container.getElement().html(newElement);
  $('#btn-code').css('border-bottom-color', '#c90057');
  $('#code-container').parent().css("overflow-y", "scroll");
});

myLayout.registerComponent('translation', function(container, componentState) {
  var source = getParameter("translation");
  var page = getParameter("page");
  var nosource = "<h2>Error: No Source Defined</h2>";
  var newElement = '<div class="ftranslation sourceDoc edition-rules" id="translation-container"/>';
  //Check if source in getParameter is defined correctly!
  if ((source == "") || (source == null)) {
    newElement = nosource;
    container.getElement().html(newElement);
    console.log("no source defined!");
  } else {
    container.getElement().html(newElement);
  }
  $('#btn-translation').css('border-bottom-color', '#c90057');
});

myLayout.registerComponent('tran', function(container, state) {
  var source = getParameter("edition");
  var page = getParameter("page");
  var nosource = "<h2>Error: No Source Defined</h2>";
  var newElement = '<div class="ftran sourceDoc edition-rules" id="edition-container"/>';
  //Check if source in getParameter is defined correctly!
  if ((source == "") || (source == null)) {
    newElement = nosource;
    container.getElement().html(newElement);
    console.log("no source defined!");
  } else {
    container.getElement().html(newElement);
  }
  $('#btn-tran').css('border-bottom-color', '#c90057');
});

function goldenActivate(trigger) {
  var child;
  var indexNumber = 0;
  var activeContentItems = myLayout.root.contentItems[0].contentItems.length;
  if (trigger == 'code') {
    child = code;
    indexNumber = activeContentItems;
  } else if (trigger == 'translation') {
    child = translation;
    if ((countActiveComponents() == activeContentItems) && (myLayout.root.getComponentsByName('code').length == 1)) {
      indexNumber = activeContentItems - 1;
    } else {
      indexNumber = activeContentItems;
    }
  } else if (trigger == 'tran') {
    child = tran;
    if ((countActiveComponents() == activeContentItems) && (myLayout.root.getComponentsByName('facs').length == 1)) {
      indexNumber = 1;
    } else if (myLayout.root.getComponentsByName('facs').length == 0) {
      indexNumber = 0;
    } else {
      indexNumber = activeContentItems;
    }
  } else if (trigger == 'toc') {
    child = toc;
  } else if (trigger == 'facs') {
    child = facs;
    indexNumber = 0;
  }

  myLayout.root.contentItems[0].addChild(child, indexNumber);
  setTimeout(function() {
    resetImageViewer();
    loadFacs(getParameter('page'))
  }, 100);
  var insert;
  switch (trigger) {
    case 'code':
      loadCode();
      break;
    case 'tran':
      loadTrans(getParameter("edition"), getParameter("page"), "#edition-container");
      break;
    case 'translation':
      loadTrans(getParameter("translation"), getParameter("page"), "#translation-container");
      break;
    case 'facs':
      resetImageViewer()
      break;
    default:
      console.log("unknown trigger");
  };
}

function goldenDestroy(id) {
  myLayout.root.getItemsById(id)[0].remove();
}

$(window).resize(function() {
  myLayout.updateSize();
});
myLayout.init();
$('#loaderGoldenLayout').css('display', 'none');


/* JS Code for Mirador Viewer */
// FUNCTIONS FIRST
function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') c = c.substring(1);
    if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
  }
  return "";
}

//get Manifest from getParameter
function getManifest() {
  var result = null,
    tmp = [];
  location.search
    .substr(1)
    .split("&")
    .forEach(function(item) {
      tmp = item.split("=");
      if (tmp[0] === "manifest") result = decodeURIComponent(tmp[1]);
    });
  return result;
}
var manifest = getManifest();
//dann einfach bei der manifestUri anstatt textgrid:399bv.0 die variable manifest nehmen!!

function loadPage(source, number) {
  console.log('load page number: ' + number);
  if (sessionStorage.getItem('edition') == source) {} else {
    sessionStorage.setItem('edition', source);
  }

  setTimeout(function() {
    /* if edition view is enabled */
    if ($("#edition-container").length == '1') {
      loadTrans(source, number, '#edition-container');
    }

    /* if translation view is enabled */
    if ($("#translation-container").length == '1') {
      loadTrans(getParameter("translation"), number, '#translation-container');
    }

    /* if code view is enabled */
    if ($("#code-container").length == '1') {
      loadCode();
    }

    /* if edition view nor translation view is enabled */
    if (($("#edition-container").length != '1') && ($("#translation-container").length != '1')) {

      console.log("didn't found edition view nor translation view => no facs and no xml code");
    };
  }, 200);

}

function pastTransLoad(container,number) {
    $(container).parent().css("overflow-y", "scroll");
    addAllPages();
    loadFacs(number);
    setFontSize();
  $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    });
    
    // notify highlighter of new content
    window.dispatchEvent(new Event('trans-loaded'));
}

function addEventListenersRezise(classnames) {
  var refreshViewerSize = function() {
    setTimeout(function() {
      resetImageViewer();
    }, 100);
  };
  try {
    for (var i = 0; i < classnames.length; i++) {
      classnames[i].addEventListener('mouseup', refreshViewerSize, false);
    }
  } catch (e) {
    console.log(e)
  }
}

function frenchStart() {
    var lang = getParameter("lang");
    var edition = getParameter("translation");
    var page = getParameter("page");
    if (lang == "fr") {
        btnTranslation();
        loadTrans(edition, page, '#translation-container');
        btnEdition();
    }
    else {}
}

function setNavbarAuthorShort() {
    var edition = getParameter('edition');
    var authorName = '';
    if (edition == '34zmq') { authorName = 'Harrach'; }
    else if (edition == '34zs7') { authorName = 'Sturm'; }
    else if (edition == '3ptwg') { authorName = 'Corfey'; }
    else if (edition == '3qr4f') { authorName = 'Neumann'; }
    else if (edition == '34znb') { authorName = 'Pitzler'; }
    else if (edition == '3c0m2') { authorName = 'Knesebeck'; }
    else { authorName = 'Error'; }

    document.getElementById('navbarAuthorShort').innerHTML = authorName;
}

$(document).ready(function() {
  $(window).resize(function() {
    myLayout.updateSize();
  });
  resetFont();
  setNavbarAuthorShort();
  setDownloadLinks();

  /* event listeners for drag handlers = rezising imageviewer.js */
  //var draghandlers = document.getElementsByClassName("lm_drag_handle");
  //addEventListenersRezise(draghandlers);
  /*var draghandlers = document.getElementsByClassName("lm_header");
  addEventListenersRezise(draghandlers); <= does not work correctly*/
  attachResizeEvents()

  /* there is an issue (https://github.com/golden-layout/golden-layout/issues/405). they say this helps! */
  myLayout._isFullPage = true;
  /* imageviewer rezise on column create and row create */
  myLayout.on('stackCreated', function() {
    setTimeout(function() {
      resetImageViewer();
      loadFacs(getParameter('page'))
    }, 10)
  });
  /* imageviewer rezise on tab create */
  myLayout.on('tabCreated', function() {
    setTimeout(function() {
      resetImageViewer();
      loadFacs(getParameter('page'))
    }, 100)
  });
  /*myLayout.on('itemCreated',function(){setTimeout(function(){console.log('itemcreated');resetImageViewer();}, 1000)});
  myLayout.on('itemDestroyed',function(){setTimeout(function(){console.log('itemDestroyed');resetImageViewer();}, 1000)});*/

  /*myLayout.on('selectionChanged',function(){setTimeout(function(){console.log('selectionChanged');resetImageViewer();}, 1000)});
  myLayout.on('activeContentItemChanged',function(){setTimeout(function(){console.log('activeContentItemChanged');resetImageViewer();}, 1000)});
  /*myLayout.on('selectionChanged',function(){setTimeout(function(){console.log('selectionChanged');resetImageViewer();}, 1000)});
    myLayout.on('activeContentItemChanged',function(){setTimeout(function(){console.log('activeContentItemChanged');resetImageViewer();}, 1000)});

    /*myLayout.on('resize',function(){setTimeout(function(){console.log('resize');resetImageViewer();}, 1000)});*/
  /*myLayout.on('resize', function() {
    setTimeout(function() {
      console.log('resize');
      resetImageViewer();
    }, 1000)
  });*/

  frenchStart();
  resetImageViewer();
});

function getHeightOfSurroundings() {
  var surroundings = $('#editionNavBar').height();
  surroundings = $('body header').height() + surroundings;
  surroundings = $('body footer').height() + surroundings;
  var heightOfGolden = $(document).height() - surroundings;
  return heightOfGolden;
}

function loadFacs(page) {
  console.log("load facs for page: "+page);
  var imageurl;
  var imageurllow;
  var imageurlfull;
  var facs;
  try {
    var pbelement = document.getElementById('page' + page);
    facs = pbelement.getAttribute('facs');
    imageurllow = 'https://textgridlab.org/1.0/digilib/rest/IIIF/' + facs + '/full/,100/0/native.jpg';
    imageurlfull = 'https://textgridlab.org/1.0/digilib/rest/IIIF/' + facs + '/full/max/0/default.jpg';
    //because rendering of pitzler images is currently broken there are no low size images availalbe for pitzler!
    if (getParameter("edition") == '34znb')   {
        imageurllow = imageurlfull;
    }
    else if (getParameter("edition") == '350mg')   {
        imageurllow = imageurlfull;
    }
  } catch (e) {
    console.log(e)
  }
  // Rotation for one fac of Sturm
  if ( (getParameter("edition") == '34zs7' || getParameter("edition") == '3q4rq') && (page == "93") )   {
    console.log("Rotation for Sturm view 93", getParameter("edition"));
    imageurllow = 'https://textgridlab.org/1.0/digilib/rest/IIIF/' + facs + '/full/,100/90/native.jpg';
    imageurlfull = 'https://textgridlab.org/1.0/digilib/rest/IIIF/' + facs + '/full/max/90/default.jpg';
    viewer.load(imageurllow, imageurlfull);
  }
  else if ((facs != null) && (facs != '')) {
    viewer.load(imageurllow, imageurlfull);
  }
  // exception for Neumann views 88/89/90 (transformation corrupt for this pages)
  else if ( (getParameter("edition") == '3qr4f' || getParameter("edition") == '3r3nn') && (page == "88" || page == "89" || page == "90") )   {
    console.log("use exception for Neumann views 88/89/90, edition:", getParameter("edition"));
    facs = document.querySelector('[facs]').getAttribute("facs");
    imageurllow = 'https://textgridlab.org/1.0/digilib/rest/IIIF/' + facs + '/full/,100/0/native.jpg';
    imageurlfull = 'https://textgridlab.org/1.0/digilib/rest/IIIF/' + facs + '/full/max/0/default.jpg';
    viewer.load(imageurllow, imageurlfull);
  }
  else {
    console.log('no valid facs: ' + facs);
    viewer.load('https://architrave.eu/templates/image/image_error.svg');
  }
}

function loadCode() {
    console.log("load code in ");
  var data;
  var source;
  var lang = getParameter("lang");
  if (lang == "fr") {
      source = getParameter('translation');
      console.log("french");
  }
  else {
      source = getParameter('edition');
      console.log("german with url: ");
  }
  var number = sessionStorage.getItem('number');
  var url = source + "/" + source + "_p" + number + "_xml.html";
  // var url = `content.xml?edition=${source}&page=${number}&xml=yes`
  getContent(url, setCodeContent, 5000);
}
