/* Error messages */
function showError(closable,message) {
    if ($('#modal-warning').css('display') != 'block') {
        if (closable) {
            $('#modal-warning-footer').css('display','block');
            $('#modal-warning').modal({
                backdrop: true
            });
        }
        else {
            $('#modal-warning-footer').css('display','none');
            $('#modal-warning').modal({
                backdrop: 'static'
            });
        }
        $('#modal-warning-message').html(message);
        $('#modal-warning').modal('show');
    }
    else (console.log(message))
}

function errorGeneral(e) {
    showError(true,e)
    console.log(e);
}

function errorFile404() {
    showError(false,"Didn't found the file you're looking for (error: 404)<br/>Please check your request and reload this page.");
}

function errorFileLoad(error) {
    showError(false,error);
}

function errorGoldenEditionOrTranslation() {
    showError(true,'Sorry, either the "Edition" component or the "Translation" component must remain open.');
}

function errorGoldenTwoComponents() {
    console.log('SORRY, there must be at least two components left.')
	showError(true,'Sorry, there must be at least two components left.');
}

function errorOldVersion() {
    showError(false,"Your browser version is out of date. Please update your browser to use this page.");
}
function errorUnsupportedBrowser() {
    showError(false,"Your Browser is not supported. Please use Firefox, Chrome or Safari to use this page.");
}
