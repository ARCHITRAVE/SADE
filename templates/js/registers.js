const base_path = (
    document.location.href.match(/^http:\/\/localhost/) ?
    '/exist/restxq/' : // development
    '/services/' // production
)

function sortUl(ul) {
    const elements = ul.querySelectorAll('li')
    const sorted = [...elements].sort((a, b) => {
        return a.textContent.localeCompare(
            b.textContent,
            getLocale(),
            {sensitivity: 'accent'}
        )
    })
    ul.innerHTML = ''
    ul.append(...sorted)
}

$('#archOccurrencesPersonsInPlaces').on('hide.bs.collapse', function() {
  $('#archOccurrencesPersonsInPlacesWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPersonsInPlaces').on('show.bs.collapse', function() {
  const ul = document.querySelector('#archOccurrencesPersonsInPlaces ul')
  sortUl(ul)
  $('#archOccurrencesPersonsInPlacesWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPersonsInWorks').on('hide.bs.collapse', function() {
  $('#archOccurrencesPersonsInWorksWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPersonsInWorks').on('show.bs.collapse', function() {
  const ul = document.querySelector('#archOccurrencesPersonsInWorks ul')
  sortUl(ul)
  $('#archOccurrencesPersonsInWorksWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPlacesInPersons').on('hide.bs.collapse', function() {
  $('#archOccurrencesPlacesInPersonsWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPlacesInPersons').on('show.bs.collapse', function() {
  $('#archOccurrencesPlacesInPersonsWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPlacesInWorks').on('hide.bs.collapse', function() {
  $('#archOccurrencesPlacesInWorksWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPlacesInWorks').on('show.bs.collapse', function() {
  const ul = document.querySelector('#archOccurrencesPlacesInWorks ul')
  sortUl(ul)
  $('#archOccurrencesPlacesInWorksWrapper svg').toggleClass('hide');
})
$('#archOccurrencesWorksInPersons').on('hide.bs.collapse', function() {
  $('#archOccurrencesWorksInPersonsWrapper svg').toggleClass('hide');
})
$('#archOccurrencesWorksInPersons').on('show.bs.collapse', function() {
  $('#archOccurrencesWorksInPersonsWrapper svg').toggleClass('hide');
})
$('#archOccurrencesWorksInPlaces').on('hide.bs.collapse', function(event) {
  $('#archOccurrencesWorksInPlacesWrapper svg').toggleClass('hide');
})
$('#archOccurrencesWorksInPlaces').on('show.bs.collapse', function() {
  $('#archOccurrencesWorksInPlacesWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPersonsInText').on('hidden.bs.collapse', function() {
  if (document.getElementById('archOccurrencesPersonsInText').classList.contains('show') == false) {
    $('#archOccurrencesPersonsInTextWrapper svg').toggleClass('hide');
  }
})
$('#archOccurrencesPersonsInText').on('show.bs.collapse', function() {
  if (document.getElementById('archOccurrencesPersonsInText').classList.contains('show') == false) {
    $('#archOccurrencesPersonsInTextWrapper svg').toggleClass('hide');
  }
})
$('#archOccurrencesPersonsInTextPitzler').on('hide.bs.collapse', function() {
  $('#archOccurrencesPersonsInTextPitzlerWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPersonsInTextPitzler').on('show.bs.collapse', function() {
  $('#archOccurrencesPersonsInTextPitzlerWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPersonsInTextHarrach').on('hide.bs.collapse', function() {
  $('#archOccurrencesPersonsInTextHarrachWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPersonsInTextHarrach').on('show.bs.collapse', function() {
  $('#archOccurrencesPersonsInTextHarrachWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPersonsInTextCorfey').on('hide.bs.collapse', function() {
  $('#archOccurrencesPersonsInTextCorfeyWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPersonsInTextCorfey').on('show.bs.collapse', function() {
  $('#archOccurrencesPersonsInTextCorfeyWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPersonsInTextKnesebeck').on('hide.bs.collapse', function() {
  $('#archOccurrencesPersonsInTextKnesebeckWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPersonsInTextKnesebeck').on('show.bs.collapse', function() {
  $('#archOccurrencesPersonsInTextKnesebeckWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPersonsInTextSturm').on('hide.bs.collapse', function() {
  $('#archOccurrencesPersonsInTextSturmWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPersonsInTextSturm').on('show.bs.collapse', function() {
  $('#archOccurrencesPersonsInTextSturmWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPersonsInTextNeumann').on('hide.bs.collapse', function() {
  $('#archOccurrencesPersonsInTextNeumannWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPersonsInTextNeumann').on('show.bs.collapse', function() {
  $('#archOccurrencesPersonsInTextNeumannWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPlacesInText').on('hidden.bs.collapse', function() {
  if (document.getElementById('archOccurrencesPlacesInText').classList.contains('show') == false) {
    $('#archOccurrencesPlacesInTextWrapper svg').toggleClass('hide');
  }
})
$('#archOccurrencesPlacesInText').on('show.bs.collapse', function() {
  if (document.getElementById('archOccurrencesPlacesInText').classList.contains('show') == false) {
    $('#archOccurrencesPlacesInTextWrapper svg').toggleClass('hide');
  }
})
$('#archOccurrencesPlacesInTextPitzler').on('hide.bs.collapse', function() {
  $('#archOccurrencesPlacesInTextPitzlerWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPlacesInTextPitzler').on('show.bs.collapse', function() {
  $('#archOccurrencesPlacesInTextPitzlerWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPlacesInTextHarrach').on('hide.bs.collapse', function() {
  $('#archOccurrencesPlacesInTextHarrachWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPlacesInTextHarrach').on('show.bs.collapse', function() {
  $('#archOccurrencesPlacesInTextHarrachWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPlacesInTextCorfey').on('hide.bs.collapse', function() {
  $('#archOccurrencesPlacesInTextCorfeyWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPlacesInTextCorfey').on('show.bs.collapse', function() {
  $('#archOccurrencesPlacesInTextCorfeyWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPlacesInTextKnesebeck').on('hide.bs.collapse', function() {
  $('#archOccurrencesPlacesInTextKnesebeckWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPlacesInTextKnesebeck').on('show.bs.collapse', function() {
  $('#archOccurrencesPlacesInTextKnesebeckWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPlacesInTextSturm').on('hide.bs.collapse', function() {
  $('#archOccurrencesPlacesInTextSturmWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPlacesInTextSturm').on('show.bs.collapse', function() {
  $('#archOccurrencesPlacesInTextSturmWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPlacesInTextNeumann').on('hide.bs.collapse', function() {
  $('#archOccurrencesPlacesInTextNeumannWrapper svg').toggleClass('hide');
})
$('#archOccurrencesPlacesInTextNeumann').on('show.bs.collapse', function() {
  $('#archOccurrencesPlacesInTextNeumannWrapper svg').toggleClass('hide');
})
$('#archOccurrencesWorksInText').on('hidden.bs.collapse', function() {
  if (document.getElementById('archOccurrencesWorksInText').classList.contains('show') == false) {
    $('#archOccurrencesWorksInTextWrapper svg').toggleClass('hide');
  }
})
$('#archOccurrencesWorksInText').on('show.bs.collapse', function() {
  if (document.getElementById('archOccurrencesWorksInText').classList.contains('show') == false) {
    $('#archOccurrencesWorksInTextWrapper svg').toggleClass('hide');
  }
})
$('#archOccurrencesWorksInTextPitzler').on('hide.bs.collapse', function() {
  $('#archOccurrencesWorksInTextPitzlerWrapper svg').toggleClass('hide');
})
$('#archOccurrencesWorksInTextPitzler').on('show.bs.collapse', function() {
  $('#archOccurrencesWorksInTextPitzlerWrapper svg').toggleClass('hide');
})
$('#archOccurrencesWorksInTextHarrach').on('hide.bs.collapse', function() {
  $('#archOccurrencesWorksInTextHarrachWrapper svg').toggleClass('hide');
})
$('#archOccurrencesWorksInTextHarrach').on('show.bs.collapse', function() {
  $('#archOccurrencesWorksInTextHarrachWrapper svg').toggleClass('hide');
})
$('#archOccurrencesWorksInTextCorfey').on('hide.bs.collapse', function() {
  $('#archOccurrencesWorksInTextCorfeyWrapper svg').toggleClass('hide');
})
$('#archOccurrencesWorksInTextCorfey').on('show.bs.collapse', function() {
  $('#archOccurrencesWorksInTextCorfeyWrapper svg').toggleClass('hide');
})
$('#archOccurrencesWorksInTextKnesebeck').on('hide.bs.collapse', function() {
  $('#archOccurrencesWorksInTextKnesebeckWrapper svg').toggleClass('hide');
})
$('#archOccurrencesWorksInTextKnesebeck').on('show.bs.collapse', function() {
  $('#archOccurrencesWorksInTextKnesebeckWrapper svg').toggleClass('hide');
})
$('#archOccurrencesWorksInTextSturm').on('hide.bs.collapse', function() {
  $('#archOccurrencesWorksInTextSturmWrapper svg').toggleClass('hide');
})
$('#archOccurrencesWorksInTextSturm').on('show.bs.collapse', function() {
  $('#archOccurrencesWorksInTextSturmWrapper svg').toggleClass('hide');
})
$('#archOccurrencesWorksInTextNeumann').on('hide.bs.collapse', function() {
  $('#archOccurrencesWorksInTextNeumannWrapper svg').toggleClass('hide');
})
$('#archOccurrencesWorksInTextNeumann').on('show.bs.collapse', function() {
  $('#archOccurrencesWorksInTextNeumannWrapper svg').toggleClass('hide');
})

// no Entries Element for Occurences. Language Specific?
var noEntriesElement = '<li>keine Treffer</li>';
var lang;
try {
  lang = getParameter('lang');
  if (!(lang == 'de')) {
    lang = 'fra';
    noEntriesElement = '<li>aucun résultat</li>';
  }
} catch (e) {
  console.log(e);
  lang = 'fra';
}

function idnoTypeParser(type) {
  let output;
  switch(type) {
    case 'wikidata': 
      output = 'Wikidata';
      break;
    case 'bnf-catalogue': 
      output = 'BnF';
      break;
    case 'getty':
      output = 'Getty-TGN';
      break;
    case 'versailles-collections':
      output = 'Versailles-Collections';
      break;
    case 'versailles-vdse':
      output = 'Versailles-VDSE';
      break;
    case 'louvre':
      output = 'Louvre-Collections';
      break;
    case 'versailles-sjvt':
      output = 'Versailles-SVJT';
      break;
    case 'k10plus':
      output = 'K10plus-Verbundkatalog';
      break;
    case 'bsb':
      output = 'BSB-Katalog';
      break;
    case 'haab':
      output = 'HAAB-Digital';
      break;
    case 'bnf-data':
      output = 'Data-BnF';
      break;
    case 'bnf-gallica':
      output = 'Gallica-BnF';
      break;
    default:
      output = type.toUpperCase()
  }
  return output;
}

function setRegElement(element, content) {
  try {
    $(element).html(content)
  } catch (e) {
    console.log(e)
  }
}

function setRegElementLink(element, content) {
  try {
    if (content.length > 0) {
      $(element).attr('href', content)
      $(element).attr('target', '_blank').attr('rel', 'noopener')
      if (content.match(/uni-heidelberg/)) {
          $('#regWorkIDNOType').text('UB Heidelberg')
      }
      if (content.match(/inha/)) {
          $('#regWorkIDNOType').text('INHA')
      }
      if (content.match(/kaiserhof/)) {
          $('#regWorkIDNOType').text('Kaiserhof')
          $('#regPersonIDNOType').text('Kaiserhof')
      }
      if (content.match(/biblissima/)) {
          $('#regWorkIDNOType').text('Biblissima')
          $('#regPersonIDNOType').text('Biblissima')
      }
      if (content.match(/idref/)) {
          $('#regWorkIDNOType').text('IdRef')
          $('#regPersonIDNOType').text('IdRef')
          $('#regPlaceIDNOType').text('IdRef')
      }
      console.log(content)
    } else {
      console.log("Error: setRegElementLink: content empty")
      hideRegElement(element);
    }
  } catch (e) {
    console.log(e)
  }
}

function hideRegElement(element) {
  $(element).closest('.edition-info-item').css('display', 'none')
}

function hideRegElementDescription(element) {
  $(element).closest('.arch-edition-info-section').css('display', 'none')
}

function checkLanguageFrench() {
  var lang;
  try {
    lang = getParameter('lang');
    if (!(lang == 'de')) {
      lang = 'fra'
    }
  } catch (e) {
    console.log(e);
    lang = 'fra';
  }
  if (lang == 'fra') {
    return true;
  } else {
    return false;
  }
}

function getArtistNamesREST(elementID) {
  var lang;
  try {
    lang = getParameter('lang');
    if (!(lang == 'de')) {
      lang = 'fra'
    }
  } catch (e) {
    console.log(e);
    lang = 'fra';
  }
  var textGridID = '';
  if (elementID.includes('textgrid:')) {
    textGridID = elementID.split('textgrid:')[1];
  } else {
    console.log('error: could not find Artist via REST: no valid TextGridID:' + elementID)
  }
  var data;
  var result = $.ajax({
    type: 'GET',
    url: base_path + 'get-artist/' + textGridID,
    data: {},
    success: function(data) {
        //console.log('xxxx', data)
    }
  });
  return result
}

async function getArtistNames(artists) {
  var lang;
  try {
    lang = getParameter('lang');
    if (!(lang == 'de')) {
      lang = 'fra'
    }
  } catch (e) {
    console.log(e);
    lang = 'fra';
  }
  for (i = 0; i < artists.length; i++) {
    var elementID = '#' + artists[i].ref;
    elementID = elementID.replace(":", "").replace(":", "")
    var artist = await getArtistNamesREST(artists[i].ref);
    artist = artist.persName.filter(el => el['xml:lang'] == lang)[0]['#text'] + ' '
    $(elementID).text(artist.toString());
  }
}

function getArtistName(artistID) {
  var lang;
  try {
    lang = getParameter('lang');
    if (!(lang == 'de')) {
      lang = 'fra'
    }
  } catch (e) {
    console.log(e);
    lang = 'fra';
  }
  artistID = artistID.split('textgrid')[1];
  var result = $.ajax({
    type: 'GET',
    url: base_path + 'get-artist/' + artistID,
    data: {},
    success: function(data) {
        //console.log(data, artistID)
    }
  });
  return result
}


function getLocationNames(textGridID) {
  var lang;
  try {
    lang = getParameter('lang');
    if (!(lang == 'de')) {
      lang = 'fra'
    }
  } catch (e) {
    console.log(e);
    lang = 'fra';
  }
  var data;
  var result = $.ajax({
    type: 'GET',
    url: base_path + 'get-location/' + textGridID,
    data: {},
    success: function(data) {}
  });
  return result
}

function loadInternalLocation(triggerElement) {
  try {
    var elementID = triggerElement.id;
    if (elementID.includes('textgrid:')) {
      elementID = elementID.split('textgrid:')[1];
    } else if (elementID.includes('textgrid')) {
      elementID = elementID.split('textgrid')[1];
    }
    $("#modalRegisterWorks").modal("hide");
    openPlaceName(elementID);
  } catch (e) {
    console.log(e)
  }
}

function loadInternalArtist(triggerElement) {
  try {
    var elementID = triggerElement.children[0].id;
    if (elementID.includes('textgrid')) {
      elementID = elementID.split('textgrid')[1];
    } else if (elementID.includes('textgrid:')) {
      elementID = elementID.split('textgrid:')[1];
    }
    $("#modalRegisterWorks").modal("hide");
    $("#modalRegisterPlaces").modal("hide");
    openPersName(elementID);
  } catch (e) {
    console.log(e)
  }
}

function getRegArtist(arrayArtists, regElementID) {
  var lang;
  try {
    lang = getParameter('lang');
    if (!(lang == 'de')) {
      lang = 'fra'
    }
  } catch (e) {
    console.log(e);
    lang = 'fra';
  }
  var newArtistLinkHref = '';
  var newArtistLinkOnclick = '';
  var newArtistLinkBoolean = false;
  var newArtistLinkIcon = '';
  var newArtistText = '';
  var newArtistID = '';
  var arryInternalArtists = [];
  var newArtistNewTab = '';
  $(regElementID).html('');
  if (!(Array.isArray(arrayArtists))) {
    var temp = arrayArtists;
    arrayArtists = [];
    arrayArtists.push(temp);
  }

  for (i = 0; i < arrayArtists.length; i++) {
    if ((typeof arrayArtists[i]['#text'] === 'string')) {
      //Artist internal
      newArtistLinkIcon = '<i class="fas fa-user"/>';
      newArtistLinkBoolean = true;
      newArtistLinkHref = '#';
      newArtistLinkOnclick = 'loadInternalArtist(this);return false;';
      newArtistID = arrayArtists[i].ref.toString().replace(":", "").replace(":", "");
      arryInternalArtists.push(newArtistID.toString());
      newArtistNewTab = '">';
    } else if ((arrayArtists[i].length > 0) && (arrayArtists[i].includes('http'))) {
      //Artist with external link
      newArtistText = arrayArtists[i].toString().split('(')[0].replace("<", "").replace(">", "");
      newArtistLinkIcon = '<i class="fas fa-external-link-alt"/>';
      newArtistLinkBoolean = true;
      newArtistLinkHref = arrayArtists[i].toString().split('(')[1].split(")")[0];
      newArtistLinkOnclick = '';
      newArtistNewTab = '" target="_blank">';
    } else if ((arrayArtists[i].length > 0) && !(arrayArtists[i].includes('http'))) {
      //Artist without link
      newArtistLinkIcon = '';
      newArtistLinkBoolean = false;
      newArtistID = '';
      newArtistText = arrayArtists[i].toString().replace("<", "").replace(">", "");
    } else {
      console.log("ERROR: getRegArtist(): unknown artist")
    }
    
    newArtistText = duoLingo(newArtistText)

    //forall: create item!
    var newArtistItem = '';
    if (newArtistLinkBoolean) {
      newArtistItem = '<a href="' + newArtistLinkHref + '" onclick="' + newArtistLinkOnclick + newArtistNewTab + '<span class="text-muted" id="' + newArtistID + '">' + newArtistText + '</span>' + newArtistLinkIcon + '<span> </span></a>';
    } else {
      newArtistItem = '<span class="text-muted" id="' + newArtistID + '">' + newArtistText + '</span>';
    }
    if (i > 0) {
      const separator = (lang == 'de' ? '; ' : ' ; ')
      $(regElementID).append(separator)
    }
    $(regElementID).append(newArtistItem);
    getRegArtistsInternal(arryInternalArtists);
  }
}

async function getRegArtistsInternal(arryInternalArtists) {
  var artistName = "";
  var elementName = "";
  var lang;
  try {
    lang = getParameter('lang');
    if (!(lang == 'de')) {
      lang = 'fra'
    }
  } catch (e) {
    console.log(e);
    lang = 'fra';
  }
  try {
    for (var i = 0; i < arryInternalArtists.length; i++) {
      try {
        artistName = await getArtistName(arryInternalArtists[i]);
        artistName = artistName.persName.filter(el => el['xml:lang'] == lang)[0]['#text'];
      } catch (e) {
        console.log("I think the artist name is unknown");
        console.log(e)
        artistName = "Unknown";
      }
      artistName = artistName + ' ';
      elementName = arryInternalArtists[i];
      document.getElementById(elementName).innerHTML = artistName;
    }
  } catch (e) {
    console.log("I think here should be no error");
    console.log(e);
  }
  //replace internal artist links
}

async function getLocationPlaces(locations, elementID) {
  let lang = getLocale()
  if (!(lang == 'de')) {
    lang = 'fra'
  }
  
  if (locations.length == 0) {
      $(elementID).closest('.edition-info-item').hide()
      return
  }

  for (const [i, location] of Object.entries(locations)) {
      console.log(i, location)
      
      let result = '';
      if (typeof location === 'string') {
        const augmented = duoLingo(augmentValue(location))
        result = '<span class="text-muted">' + augmented + '</span>';
      } else if (typeof location['#text'] === 'string') {
        let href = '';
        let spanText = '';
        if (location.ref.includes('textgrid:')) {
          href = location.ref;
          let textGridID = location.ref.split('textgrid:')[1];
          spanText = location['#text'].replace(/\([a-z]{3}:textgrid:[a-z0-9]+\)/, '')
          //spanText = await getLocationNames(textGridID);
          //spanText = spanText.placeName.filter(el => el['xml:lang'] == lang)[0]['#text'] + ' '
        } else {
          href = location.ref;
          spanText = location['#text'];
        }
        const augmented = duoLingo(spanText.toString())
        // console.log([spanText.toString(), augmented])
        result = '<a id="' + href + '"href="#" onclick="loadInternalLocation(this)"><span class="text-muted">' + augmented + ' </span><i class="fas fa-map-marker-alt"/></a>';
      } else {
        console.log('error in getLocationPlaces()');
      }
      if (i > 0) {
          const separator = (lang == 'de' ? '; ' : ' ; ')
          $(elementID).append(separator);
      }
      $(elementID).append(result);
      // return result;
  }
}

function resetModalRegisterPersonsDefault() {
  $('#archOccurrencesPersonsInText').collapse('hide');
  $('#archOccurrencesPersonsInTextPitzler').collapse('hide');
  $('#archOccurrencesPersonsInTextHarrach').collapse('hide');
  $('#archOccurrencesPersonsInTextCorfey').collapse('hide');
  $('#archOccurrencesPersonsInTextSturm').collapse('hide');
  $('#archOccurrencesPersonsInTextKnesebeck').collapse('hide');
  $('#archOccurrencesPersonsInTextNeumann').collapse('hide');
  $('#archOccurrencesPersonsInPlaces').collapse('hide');
  $('#archOccurrencesPersonsInWorks').collapse('hide');
  $('#regPersonTitle').text('');
  $('#regPersonPersonNameType').text('');
  $('#regPersonIDNOType').text('');
  $('#regPersonNationality').text('');
  $('#regPersonFunction').text('');
  $('#regPersonBirthdate').text('');
  $('#regPersonBirthplace').text('');
  $('#regPersonDeathdate').text('');
  $('#regPersonDeathplace').text('');
  $('#regPersonDescription').text('');
  $('#modal-register-occurences-text').text('');
  $('#modal-register-occurences-text-Pitzler').text('');
  $('#modal-register-occurences-text-Harrach').text('');
  $('#modal-register-occurences-text-Corfey').text('');
  $('#modal-register-occurences-text-Knesebeck').text('');
  $('#modal-register-occurences-text-Sturm').text('');
  $('#modal-register-occurences-text-Neumann').text('');
  $('#modal-register-occurences-places').text('');
  $('#modal-register-occurences-works').text('');
  $('#regPersonPersonNameType').closest('.edition-info-item').css('display', 'flex');
  $('#regPersonIDNOType').closest('.edition-info-item').css('display', 'flex');
  $('#regPersonIDNOLink').closest('.edition-info-item').css('display', 'flex');
  $('#regPersonNationality').closest('.edition-info-item').css('display', 'flex');
  $('#regPersonFunction').closest('.edition-info-item').css('display', 'flex');
  $('#regPersonBirthdate').closest('.edition-info-item').css('display', 'flex');
  $('#regPersonBirthplace').closest('.edition-info-item').css('display', 'flex');
  $('#regPersonDeathdate').closest('.edition-info-item').css('display', 'flex');
  $('#regPersonDeathplace').closest('.edition-info-item').css('display', 'flex');
  $('#regPersonDescription').closest('.arch-edition-info-section').css('display', 'block');
}

function resetModalRegisterPlacesDefault() {
  $('#archOccurrencesPlacesInText').collapse('hide');
  $('#archOccurrencesPlacesInTextPitzler').collapse('hide');
  $('#archOccurrencesPlacesInTextHarrach').collapse('hide');
  $('#archOccurrencesPlacesInTextCorfey').collapse('hide');
  $('#archOccurrencesPlacesInTextSturm').collapse('hide');
  $('#archOccurrencesPlacesInTextKnesebeck').collapse('hide');
  $('#archOccurrencesPlacesInTextNeumann').collapse('hide');
  $('#archOccurrencesPlacesInPersons').collapse('hide');
  $('#archOccurrencesPlacesInWorks').collapse('hide');
  $('#regPlaceTitle').text('');
  $('#regPlacePlaceNameType').text('');
  $('#regPlaceIDNOType').text('');
  $('#regPlacePlaceNameCurrent').text('');
  $('#regPlacePlaceNameHistorical').text('');
  $('#regPlaceFunction').text('');
  $('#regPlaceState').text('');
  $('#regPlaceArtist').text('');
  $('#regPlaceConstruction').text('');
  $('#regPlaceLocationLinkText').text('');
  $('#regPlaceDescription').text('');
  $('#modal-register-occurences-places-text').text('');
  $('#modal-register-occurences-places-text-Pitzler').text('');
  $('#modal-register-occurences-places-text-Harrach').text('');
  $('#modal-register-occurences-places-text-Corfey').text('');
  $('#modal-register-occurences-places-text-Knesebeck').text('');
  $('#modal-register-occurences-places-text-Sturm').text('');
  $('#modal-register-occurences-places-text-Neumann').text('');
  $('#modal-register-occurences-places-in-works').text('');
  $('#regPersonPlaceNameType').closest('.edition-info-item').css('display', 'flex');
  $('#regPlaceIDNOLink').closest('.edition-info-item').css('display', 'flex');
  $('#regPlaceIDNOType').closest('.edition-info-item').css('display', 'flex');
  $('#regPlacePlaceNameCurrent').closest('.edition-info-item').css('display', 'flex');
  $('#regPlacePlaceNameHistorical').closest('.edition-info-item').css('display', 'flex');
  $('#regPlaceState').closest('.edition-info-item').css('display', 'flex');
  $('#regPlaceArtist').closest('.edition-info-item').css('display', 'flex');
  $('#regPlaceConstruction').closest('.edition-info-item').css('display', 'flex');
  $('#regPlaceLocation').closest('.edition-info-item').css('display', 'flex');
  $('#regPlaceDescription').closest('.arch-edition-info-section').css('display', 'block');
}

function resetModalRegisterWorksDefault() {
  $('#archOccurrencesWorksInText').collapse('hide');
  $('#archOccurrencesWorksInTextPitzler').collapse('hide');
  $('#archOccurrencesWorksInTextHarrach').collapse('hide');
  $('#archOccurrencesWorksInTextCorfey').collapse('hide');
  $('#archOccurrencesWorksInTextSturm').collapse('hide');
  $('#archOccurrencesWorksInTextKnesebeck').collapse('hide');
  $('#archOccurrencesWorksInTextNeumann').collapse('hide');
  $('#archOccurrencesWorksInPersons').collapse('hide');
  $('#archOccurrencesWorksInPlaces').collapse('hide');
  $('#regWorkTitle').text('');
  $('#regWorkType').text('');
  $('#regWorkIDNOType').text('');
  $('#regWorkArtist').text('');
  $('#regWorkDate').text('');
  $('#regWorkState').text('');
  $('#regWorkLocationFormer').text('');
  $('#regWorkLocationCurrent').text('');
  $('#regWorkMaterial').text('');
  $('#regWorkDimensions').text('');
  $('#regWorkDescription').text('');
  $('#modal-register-occurences-works-text').text('');
  $('#modal-register-occurences-works-text-Pitzler').text('');
  $('#modal-register-occurences-works-text-Harrach').text('');
  $('#modal-register-occurences-works-text-Corfey').text('');
  $('#modal-register-occurences-works-text-Knesebeck').text('');
  $('#modal-register-occurences-works-text-Sturm').text('');
  $('#modal-register-occurences-works-text-Neumann').text('');
  $('#regWorkType').closest('.edition-info-item').css('display', 'flex');
  $('#regWorkIDNOType').closest('.edition-info-item').css('display', 'flex');
  $('#regWorkIDNOLink').closest('.edition-info-item').css('display', 'flex');
  $('#regWorkArtist').closest('.edition-info-item').css('display', 'flex');
  $('#regWorkDate').closest('.edition-info-item').css('display', 'flex');
  $('#regWorkState').closest('.edition-info-item').css('display', 'flex');
  $('#regWorkLocationCurrent').closest('.edition-info-item').css('display', 'flex');
  $('#regWorkLocationFormer').closest('.edition-info-item').css('display', 'flex');
  $('#regWorkMaterial').closest('.edition-info-item').css('display', 'flex');
  $('#regWorkDimensions').closest('.edition-info-item').css('display', 'flex');
  $('#regWorkDescription').closest('.arch-edition-info-section').css('display', 'block');
}

function sortList(elementIDofParentDIV) {
  var list, i, switching, b, shouldSwitch;
  list = document.getElementById(elementIDofParentDIV).children.item("ul");
  switching = true;
  /* Make a loop that will continue until
  no switching has been done: */
  while (switching) {
    // Start by saying: no switching is done:
    switching = false;
    b = list.getElementsByTagName("LI");
    // Loop through all list items:
    for (i = 0; i < (b.length - 1); i++) {
      // Start by saying there should be no switching:
      shouldSwitch = false;
      /* Check if the next item should
      switch place with the current item: */
      if (b[i].innerHTML.toLowerCase() > b[i + 1].innerHTML.toLowerCase()) {
        /* If next item is alphabetically lower than current item,
        mark as a switch and break the loop: */
        shouldSwitch = true;
        break;
      }
    }
    if (shouldSwitch) {
      /* If a switch has been marked, make the switch
      and mark the switch as done: */
      b[i].parentNode.insertBefore(b[i + 1], b[i]);
      switching = true;
    }
  }
}

function openOccurencesPerson(elementID) {
  $("#modalRegisterPersons").modal("hide");
  $("#modalRegisterPlaces").modal("hide");
  $("#modalRegisterWorks").modal("hide");
  openPersName(elementID.id);
}

function openOccurencesWork(elementID) {
  $("#modalRegisterPersons").modal("hide");
  $("#modalRegisterPlaces").modal("hide");
  $("#modalRegisterWorks").modal("hide");
  openArtWork(elementID.id);
}

function openOccurencesPlace(elementID) {
  $("#modalRegisterPersons").modal("hide");
  $("#modalRegisterPlaces").modal("hide");
  $("#modalRegisterWorks").modal("hide");
  openPlaceName(elementID.id);
}

function addLiRegisterElementPerson(elementID, name, ancestorID) {
  $(elementID).append('<li><a id="' + ancestorID + '" href="#" onclick="openOccurencesPerson(this); return false"><span>' + name + '</span><i class="fas fa-user"/></a></li>');
}

function addLiRegisterElementPlace(elementID, name, ancestorID) {
  $(elementID).append('<li><a id="' + ancestorID + '" href="#" onclick="openOccurencesPlace(this); return false"><span>' + name + '</span><i class="fas fa-map-marker-alt"/></a></li>');
}

function addLiRegisterElementWork(elementID, name, ancestorID) {
  $(elementID).append('<li><a id="' + ancestorID + '" href="#" onclick="openOccurencesWork(this); return false"><span>' + name + '</span><i class="fas fa-gem"/></a></li>');
}

function addLiRegisterElementText(elementID, editionURI, editionPage, editionPagePre, lang) {
  const deFr = {
      '34znb': '350mg',
      '34zmq': '3czfj',
      '3ptwg': '3r0fv',
      '3c0m2': '3czn9',
      '34zs7': '3q4rq',
      '3qr4f': '3r3nn'
  }
  const frDe = {
      '350mg': '34znb',
      '3czfj': '34zmq',
      '3r0fv': '3ptwg',
      '3czn9': '3c0m2',
      '3q4rq': '34zs7',
      '3r3nn': '3qr4f'
  }
  
  var translation = "";
  
  if (editionURI in deFr) {
      translation = deFr[editionURI]
  } else if (editionURI in frDe) {
      translation = editionURI
      editionURI = frDe[editionURI]
  } else {
      throw "this shouldn't be! " + editionURI
  }
  
//   switch (editionURI) {
//     case "34znb":
//       translation = "350mg";
//       break;
//     case "34zmq":
//       translation = "3czfj";
//       break;
//     case "3ptwg":
//       translation = "3r0fv";
//       break;
//     case "3c0m2":
//       translation = "3czn9";
//       break;
//     case "34zs7":
//       translation = "3q4rq";
//       break;
//     case "3qr4f":
//       translation = "3r3nn";
//       break;
//     default:
//       translation = "";
//   }
  $(elementID).append('<li><a id="' + elementID + editionPage + '" href="view.html?edition=' + editionURI + '&page=' + editionPage + '&translation=' + translation + '&lang=' + lang + '" target="_blank" onclick=""><span>' + editionPagePre + editionPage + '</span><i class="fas fa-external-link-alt"></i></a></li>');
}

function getPersonsInWorks(textGridURI, elementID) {
  $.ajax({
    type: 'GET',
    url: base_path + 'get-persons-in-works/' + textGridURI,
    data: {
      get_param: 'value'
    },
    success: function(data) {
      window.d = data
      var counter = 0;
      var base;
      try {
        base = data.persName;
        if (Array.isArray(base)) { // array
          counter = base.length
        } else if (typeof base['#text'] === 'string') { // string (text node)
          counter = 1
          var temp = data.persName['#text']
          base = []
          base.push(temp);
        } else { // single object
          base = [base]
          counter = 1
        }
      } catch (e) {
          console.log(e)
      }
      $(elementID).text(counter);
      console.log(data, base, counter)

      if (counter > 0) {
        $('#archOccurrencesPersonsInWorks > ul').empty();
        var lang = '';
        var ref = 'error';
        try {
          lang = getLocale();
          if (!(lang == 'de')) {
            lang = 'fra'
          }
        } catch (e) {
          console.log(e);
          lang = 'fra';
        }
        var input;
        for (i = 0; i < counter; i++) {
          if (lang == 'de') {
            input = augmentValue(base[i].ancestorNameCurrentDE);
          } else {
            input = augmentValue(base[i].ancestorNameCurrentFRA);
          }
          ancestorID = base[i].ancestor;

          addLiRegisterElementWork('#archOccurrencesPersonsInWorks > ul', input, ancestorID);
        }
        if (counter > 1) {
          sortList('archOccurrencesPersonsInWorks');
        }
      } else {
        $('#archOccurrencesPersonsInWorks > ul').empty();
        $('#archOccurrencesPersonsInWorks > ul').append(noEntriesElement);
      }
    }
  });
}

function getPersonsInPlaces(textGridURI, elementID) {
  $.ajax({
    type: 'GET',
    url: base_path + 'get-persons-in-places/' + textGridURI,
    data: {
      get_param: 'value'
    },
    success: function(data) {
      var counter = 0;
      var base;
      try {
        base = data.persName;
        if (Array.isArray(base)) {
          counter = base.length
        } else if (typeof base['#text'] === 'string') {
          counter = 1
          var temp = data.persName['#text']
          base = []
          base.push(temp);
        } else {}
      } catch (e) {}
      $(elementID).text(counter);
      if (counter > 0) {
        $('#archOccurrencesPersonsInPlaces > ul').empty();
        var lang = '';
        var ancestorID = 'error';
        try {
          lang = getLocale();
          if (!(lang == 'de')) {
            lang = 'fra'
          }
        } catch (e) {
          console.log(e);
          lang = 'fra';
        }
        var input;
        for (i = 0; i < counter; i++) {
          if (lang == 'de') {
            input = augmentValue(base[i].ancestorNameCurrentDE);
          } else {
            input = augmentValue(base[i].ancestorNameCurrentFRA);
          }
          ancestorID = base[i].ancestor;
          addLiRegisterElementPlace('#archOccurrencesPersonsInPlaces > ul', input, ancestorID);
        }
        if (counter > 1) {
          sortList('archOccurrencesPersonsInPlaces');
        }
      } else {
        $('#archOccurrencesPersonsInPlaces > ul').empty();
        $('#archOccurrencesPersonsInPlaces > ul').append(noEntriesElement);
      }
    }
  });
}

function getPlacesInWorks(textGridURI, elementID) {
  $.ajax({
    type: 'GET',
    url: base_path + 'get-places-in-works/' + textGridURI,
    data: {
      get_param: 'value'
    },
    success: function(data) {
      var counter = 0;
      var base;
      try {
        base = data.placeName;
        if (Array.isArray(base)) {
          counter = base.length
        } else if (typeof base.ref === 'string') {
          counter = 1;
          var temp = data.placeName;
          base = [];
          base.push(temp);
        } else {}
      } catch (e) {}
      $(elementID).text(counter);
      if (counter > 0) {
        $('#archOccurrencesPlacesInWorks > ul').empty();
        var lang = '';
        var ancestorID = 'error';
        try {
          lang = getLocale();
          if (!(lang == 'de')) {
            lang = 'fra'
          }
        } catch (e) {
          console.log(e);
          lang = 'fra';
        }
        var input;
        for (i = 0; i < counter; i++) {
          if (lang == 'de') {
            input = augmentValue(base[i].ancestorNameCurrentDE);
          } else {
            input = augmentValue(base[i].ancestorNameCurrentFRA);
          }
          ancestorID = base[i].ancestor;
          addLiRegisterElementWork('#archOccurrencesPlacesInWorks > ul', input, ancestorID);
        }
        if (counter > 1) {
          sortList('archOccurrencesPlacesInWorks');
        }
      } else {
        $('#archOccurrencesPlacesInWorks > ul').empty();
        $('#archOccurrencesPlacesInWorks > ul').append(noEntriesElement);
      }
    }
  });
}

function getPersonsInTextsIndividual(textGridURI, textURI, elementBaseID, EditionName) {
  var elementID = elementBaseID + '-' + EditionName;
  var listElementID = "#archOccurrencesPersonsInText" + EditionName;
  console.log(base_path + 'get-persons-in-text/' + textURI + '/' + textGridURI)
  $(elementID).text('0');
  $.ajax({
    type: 'GET',
    url: base_path + 'get-persons-in-text/' + textURI + '/' + textGridURI,
    data: {
      get_param: 'value'
    },
    success: function(data) {
      var counter = 0;
      var base;
      try {
        base = data.persName;
        if (Array.isArray(base)) {
          counter = base.length
        } else if (typeof base.editionPage === 'string') {
          counter = 1;
          var temp = data.persName;
          base = [];
          base.push(temp);
        } else {}
      } catch (e) {}
      $(elementID).text(counter);
      var counterAll = parseInt(document.getElementById(elementBaseID.substring(1, elementBaseID.length)).innerText);
      counterAll = counterAll + parseInt(counter);
      $(elementBaseID).text(counterAll);
      if (counter > 0) {
        $(listElementID + ' > ul').empty();
        var lang = '';
        var ancestorID = 'error';
        var editionPagePre = 'Ansicht ';
        try {
          lang = getParameter('lang');
          if (!(lang == 'de')) {
            lang = 'fr';
            editionPagePre = 'vue ';
          }
        } catch (e) {
          console.log(e);
          lang = 'fr';
        }
        var input;
        for (i = 0; i < counter; i++) {
          editionPage = base[i].editionPage;
          addLiRegisterElementText(listElementID + ' > ul', textURI, editionPage, editionPagePre, lang);
        }
      } else {
        $(listElementID + ' > ul').empty();
        $(listElementID + ' > ul').append(noEntriesElement);
      }
    }
  });
}

function getPersonsInTexts(textGridURI, elementBaseID) {
  var EditionsDE = ["34znb", "34zmq", "3ptwg", "3c0m2", "3q4rq", "3qr4f"];
  var EditionsFR = ["350mg", "3czfj", "3r0fv", "3czn9", "3q4rq", "3r3nn"];
  var EditionNames = ["Pitzler", "Harrach", "Corfey", "Knesebeck", "Sturm", "Neumann"];
  var Editions = [];
  $(elementBaseID).text('0');
  try {
    lang = getParameter('lang');
    if (!(lang == 'de')) {
      lang = 'fra';
      Editions = EditionsFR;
    } else {
      Editions = EditionsDE;
    }
    for (var i = 0; i < EditionsDE.length; i++) {
      getPersonsInTextsIndividual(textGridURI, Editions[i], elementBaseID, EditionNames[i]);
    }
  } catch (e) {
    console.log(e);
  }
}

function getPlacesInTexts(textGridURI, elementBaseID) {
  var EditionsDE = ["34znb", "34zmq", "3ptwg", "3c0m2", "3q4rq", "3qr4f"];
  var EditionsFR = ["350mg", "3czfj", "3r0fv", "3czn9", "3q4rq", "3r3nn"];
  var EditionNames = ["Pitzler", "Harrach", "Corfey", "Knesebeck", "Sturm", "Neumann"];
  var Editions = [];
  $(elementBaseID).text('0');
  try {
    lang = getParameter('lang');
    if (!(lang == 'de')) {
      lang = 'fra';
      Editions = EditionsFR;
    } else {
      Editions = EditionsDE;
    }
    for (var i = 0; i < EditionsDE.length; i++) {
      getPlacesInTextsIndividual(textGridURI, Editions[i], elementBaseID, EditionNames[i]);
    }
  } catch (e) {
    console.log(e);
  }
}

function getPlacesInTextsIndividual(textGridURI, textURI, elementBaseID, EditionName) {
  var elementID = elementBaseID + '-' + EditionName;
  var listElementID = "#archOccurrencesPlacesInText" + EditionName;
  $(elementID).text('0');
  $.ajax({
    type: 'GET',
    url: base_path + 'get-places-in-text/' + textURI + '/' + textGridURI,
    data: {
      get_param: 'value'
    },
    success: function(data) {
      var counter = 0;
      var base;
      try {
        base = data.placeName;
        if (Array.isArray(base)) {
          counter = base.length
        } else if (typeof base.editionPage === 'string') {
          counter = 1;
          var temp = data.placeName;
          base = [];
          base.push(temp);
        } else {}
      } catch (e) {}
      $(elementID).text(counter);
      var counterAll = parseInt(document.getElementById(elementBaseID.substring(1, elementBaseID.length)).innerText);
      counterAll = counterAll + parseInt(counter);
      $(elementBaseID).text(counterAll);
      if (counter > 0) {
        $(listElementID + ' > ul').empty();
        var lang = '';
        var ancestorID = 'error';
        var editionPagePre = 'Ansicht ';
        try {
          lang = getParameter('lang');
          if (!(lang == 'de')) {
            lang = 'fr';
            editionPagePre = 'vue ';
          }
        } catch (e) {
          console.log(e);
          lang = 'fr';
        }
        var input;
        for (i = 0; i < counter; i++) {
          editionPage = base[i].editionPage;
          addLiRegisterElementText(listElementID + ' > ul', textURI, editionPage, editionPagePre, lang);
        }
      } else {
        $(listElementID + ' > ul').empty();
        $(listElementID + ' > ul').append(noEntriesElement);
      }
    }
  });
}

function getWorksInTexts(textGridURI, elementBaseID) {
  var EditionsDE = ["34znb", "34zmq", "3ptwg", "3c0m2", "3q4rq", "3qr4f"];
  var EditionsFR = ["350mg", "3czfj", "3r0fv", "3czn9", "3q4rq", "3r3nn"];
  var EditionNames = ["Pitzler", "Harrach", "Corfey", "Knesebeck", "Sturm", "Neumann"];
  var Editions = [];
  $(elementBaseID).text('0');
  try {
    lang = getParameter('lang');
    if (!(lang == 'de')) {
      lang = 'fra';
      Editions = EditionsFR;
    } else {
      Editions = EditionsDE;
    }
    for (var i = 0; i < EditionsDE.length; i++) {
      getWorksInTextsIndividual(textGridURI, Editions[i], elementBaseID, EditionNames[i]);
    }
  } catch (e) {
    console.log(e);
  }
}

function getWorksInTextsIndividual(textGridURI, textURI, elementBaseID, EditionName) {
  const x = arguments
  var elementID = elementBaseID + '-' + EditionName;
  var listElementID = "#archOccurrencesWorksInText" + EditionName;
  $(elementID).text('0');
  $.ajax({
    type: 'GET',
    url: base_path + 'get-works-in-text/' + textURI + '/' + textGridURI,
    data: {
      get_param: 'value'
    },
    success: function(data) {
      // console.log(x)
      // console.log(base_path + 'get-works-in-text/' + textURI + '/' + textGridURI)
      // console.log(data)
        
      var counter = 0;
      var base;
      try {
        base = data.item;
        if (Array.isArray(base)) {
          counter = base.length
        } else if (typeof base.editionPage === 'string') {
          counter = 1;
          var temp = data.item;
          base = [];
          base.push(temp);
        } else {}
      } catch (e) {}
      $(elementID).text(counter);
      var counterAll = parseInt(document.getElementById(elementBaseID.substring(1, elementBaseID.length)).innerText);
      counterAll = counterAll + parseInt(counter);
      $(elementBaseID).text(counterAll);
      if (counter > 0) {
        $(listElementID + ' > ul').empty();
        var lang = '';
        var ancestorID = 'error';
        var editionPagePre = 'Ansicht ';
        try {
          lang = getParameter('lang');
          if (!(lang == 'de')) {
            lang = 'fr';
            editionPagePre = 'vue ';
          }
        } catch (e) {
          console.log(e);
          lang = 'fr';
        }
        var input;
        for (i = 0; i < counter; i++) {
          editionPage = base[i].editionPage;
          addLiRegisterElementText(listElementID + ' > ul', textURI, editionPage, editionPagePre, lang);
        }
      } else {
        $(listElementID + ' > ul').empty();
        $(listElementID + ' > ul').append(noEntriesElement);
      }
    }
  });
}

function translateTypePerson(inputType) {
  var output = "";
  inputType = inputType.toLowerCase();
  switch (inputType) {
    case "princes":
      if (checkLanguageFrench()) {
        output = "Dirigeants et princes";
      } else {
        output = "Herrscher und Fürsten";
      }
      break;
    case "statesmen":
      if (checkLanguageFrench()) {
        output = "Membres du gouvernement et de l’administration";
      } else {
        output = "Regierungs- und Verwaltungsmitglieder";
      }
      break;
    case "scholars":
      if (checkLanguageFrench()) {
        output = "Savants et hommes de lettres";
      } else {
        output = "Gelehrte und Schriftsteller";
      }
      break;
    case "clerics":
      if (checkLanguageFrench()) {
        output = "Ecclésiastiques";
      } else {
        output = "Kirchliche Würdenträger";
      }
      break;
    case "artists":
      if (checkLanguageFrench()) {
        output = "Artistes";
      } else {
        output = "Künstler";
      }
      break;
    case "others":
      if (checkLanguageFrench()) {
        output = "Autres";
      } else {
        output = "Andere";
      }
      break;
    case "architects":
      if (checkLanguageFrench()) {
        output = "Infrastructure";
      } else {
        output = "Architekten";
      }
      break;
    case "sculptors":
      if (checkLanguageFrench()) {
        output = "Sculpteurs";
      } else {
        output = "Bildhauer";
      }
      break;
    case "printmakers":
      if (checkLanguageFrench()) {
        output = "Graveurs";
      } else {
        output = "Graphiker";
      }
      break;
    case "painters":
      if (checkLanguageFrench()) {
        output = "Peintres";
      } else {
        output = "Maler";
      }
      break;
    default:
      output = inputType + " (not translated)";
  }
  return output;
}

function translateTypePlace(inputType) {
  var output = "";
  inputType = inputType.toLowerCase();
  switch (inputType) {
    case "geographic":
      if (checkLanguageFrench()) {
        output = "Lieux géographiques";
      } else {
        output = "Geographische Orte";
      }
      break;
    case "infrastructure":
      if (checkLanguageFrench()) {
        output = "Infrastructures";
      } else {
        output = "Infrastruktur";
      }
      break;
    case "public":
      if (checkLanguageFrench()) {
        output = "Édifices publics";
      } else {
        output = "Öffentliche Gebäude";
      }
      break;
    case "monuments":
      if (checkLanguageFrench()) {
        output = "Monuments";
      } else {
        output = "Monumente";
      }
      break;
    case "domestic":
      if (checkLanguageFrench()) {
        output = "Édifices domestiques";
      } else {
        output = "Wohngebäude";
      }
      break;
    case "interior":
      if (checkLanguageFrench()) {
        output = "Édifices domestiques (intérieurs)";
      } else {
        output = "Wohngebäude (Innenräume)";
      }
      break;
    case "religious":
      if (checkLanguageFrench()) {
        output = "Édifices religieux";
      } else {
        output = "Religiöse Gebäude";
      }
      break;
    case "military":
      if (checkLanguageFrench()) {
        output = "Constructions militaires";
      } else {
        output = "Militäreinrichtungen";
      }
      break;
    case "garden":
      if (checkLanguageFrench()) {
        output = "Parcs et jardins";
      } else {
        output = "Parks und Gärten";
      }
      break;
    case "other":
      if (checkLanguageFrench()) {
        output = "Autres lieux";
      } else {
        output = "Andere Orte";
      }
      break;
    default:
      output = inputType + " (not translated)";
  }
  return output;
}

function translateTypeWork(inputType) {
  var output = "";
  inputType = inputType.toLowerCase();
  switch (inputType) {
    case "painting":
      if (checkLanguageFrench()) {
        output = "Peinture";
      } else {
        output = "Malerei";
      }
      break;
    case "sculpture":
      if (checkLanguageFrench()) {
        output = "Sculpture";
      } else {
        output = "Plastik";
      }
      break;
    case "engraving":
      if (checkLanguageFrench()) {
        output = "Gravure";
      } else {
        output = "Graphik";
      }
      break;
    case "books":
      if (checkLanguageFrench()) {
        output = "Livres";
      } else {
        output = "Bücher";
      }
      break;
    case "book":
      if (checkLanguageFrench()) {
        output = "Livres";
      } else {
        output = "Bücher";
      }
      break;
    case "others":
      if (checkLanguageFrench()) {
        output = "Autres";
      } else {
        output = "Andere";
      }
      break;
    default:
      output = inputType + " (not translated)";
  }
  return output;
}

function langDissect(input) {
  const locale = getParameter('lang')
  const country = input.match(/\(([^\)]+)\)/)[1].split(' / ')
  const city = input.split(' (')[0].split('/')
  let value = ''
  if (city.length == 1) {
      value += city[0]
  } else {
      value += (locale == 'de' ? city[0] : city[1])
  }
  value += ' (' + (locale == 'de' ? country[0] : country[1]) + ')'
  
  return value
}

function getRestAPIPerson(textGridURI) {
  console.log('getting Person with textGridURI: ' + textGridURI);
  getPersonsInWorks(textGridURI, '#modal-register-occurences-works');
  getPersonsInPlaces(textGridURI, '#modal-register-occurences-places');
  getPersonsInTexts(textGridURI, '#modal-register-occurences-text');
  $.ajax({
    type: 'GET',
    url: base_path + 'get-person/' + textGridURI,
    data: {
      get_param: 'value'
    },
    success: function(data) {
      var lang = '';
      var output;
      try {
        lang = getParameter('lang');
        if (!(lang == 'de')) {
          lang = 'fra'
        }
      } catch (e) {
        console.log(e);
        lang = 'fra';
      }
      //check unidentified
      try {
        output = data.person.type;
        if (output == 'unidentified') {
          $('#regPersonUnidentified').css('display', 'flex');
        } else {
          $('#regPersonUnidentified').css('display', 'none');
        }
      } catch (e) {}
      try {
        output = data.person.persName.filter(el => el['xml:lang'] == lang)[0]['#text'];
        if (!output) {
            output = data.person.occupation.filter(el => el['xml:lang'] == lang)[0]['#text']
        }
        if (typeof output === 'string') {
          setRegElement('#regPersonTitle', output)
        } else {
          console.log('title error')
        }
      } catch (e) {
        $('#regPersonTitle').text('');
      }
      try {
        output = data.person['subtype'];
        if (typeof output === 'string' && !output.match(/^\s*$/)) {
          setRegElement('#regPersonPersonNameType', translateTypePerson(output));
        } else {
          hideRegElement('#regPersonPersonNameType')
        }
      } catch (e) {
        $('#regPersonPersonNameType').text('ERROR');
      }
      try {
        output = data.person.idno.type;
        if (typeof output === 'string') {
          setRegElement('#regPersonIDNOType', idnoTypeParser(output))
        } else {
          hideRegElement('#regPersonIDNOType')
        }
      } catch (e) {
        hideRegElement('#regPersonIDNOType');
      }
      try {
        output = data.person.idno['xml:base'];
        if (typeof output === 'string') {
          setRegElementLink('#regPersonIDNOLink', output)
        } else {
          hideRegElement('#regPersonIDNOLink')
        }
      } catch (e) {
        hideRegElement('#regPersonIDNOLink');
      }
      try {
        output = data.person.nationality.filter(el => el['xml:lang'] == lang)[0]['#text'];
        if (typeof output === 'string') {
          setRegElement('#regPersonNationality', output)
        } else {
          hideRegElement('#regPersonNationality')
        }
      } catch (e) {
        $('#regPersonNationality').closest('.edition-info-item').css('display', 'none');
      }
      try {
        var occupations = data.person.occupation.filter(el => el['xml:lang'] == lang);
        output = '';
        var nonBreakingSpace = String.fromCharCode(160);
        for (i = 0; i < occupations.length; i++) {
          if (i == (occupations.length - 1)) {
            output += occupations[i]['#text'];
          } else {
            if (checkLanguageFrench()) {
              output += occupations[i]['#text'] + nonBreakingSpace + '; ';
            } else {
              output += occupations[i]['#text'] + '; ';
            }
          }
        }
        if (typeof output === 'string') {
          setRegElement('#regPersonFunction', output)
        } else {
          hideRegElement('#regPersonFunction')
        }
      } catch (e) {
        $('#regPersonFunction').closest('.edition-info-item').css('display', 'none');
      }
      try {
        output = data.person.birth.date.filter(el => el['xml:lang'] == lang)[0]['#text'];
        if (typeof output === 'string') {
          setRegElement('#regPersonBirthdate', output)
        } else {
          hideRegElement('#regPersonBirthdate')
        }
      } catch (e) {
        $('#regPersonBirthdate').closest('.edition-info-item').css('display', 'none');
      }
      try {
        output = data.person.birth.placeName;
        if (typeof output === 'string') {
          setRegElement('#regPersonBirthplace', langDissect(output))
        } else if (output && typeof output['#text'] === 'string') {
          setRegElement('#regPersonBirthplace', langDissect(output['#text']))
        } else {
          hideRegElement('#regPersonBirthplace')
        }
      } catch (e) {
          console.log(e)
        $('#regPersonBirthplace').closest('.edition-info-item').css('display', 'none');
      }
      try {
        output = data.person.death.date.filter(el => el['xml:lang'] == lang)[0]['#text'];
        if (typeof output === 'string') {
          setRegElement('#regPersonDeathdate', output)
        } else {
          hideRegElement('#regPersonDeathdate')
        }
      } catch (e) {
        $('#regPersonDeathdate').closest('.edition-info-item').css('display', 'none');
      }
      try {
        output = data.person.death.placeName;
        if (typeof output === 'string') {
          setRegElement('#regPersonDeathplace', langDissect(output))
        } else if (typeof output['#text'] === 'string') {
          setRegElement('#regPersonDeathplace', langDissect(output['#text']))
        } else {
          hideRegElement('#regPersonDeathplace')
        }
      } catch (e) {
        console.log(e)
        $('#regPersonDeathplace').closest('.edition-info-item').css('display', 'none');
      }
      try {
        output = data.person.note.filter(el => el['type'] == 'description').filter(el => el['xml:lang'] == lang)[0]['#text'];
        if (typeof output === 'string') {
          output = fixGermanQuotes(output)
          output = fixSmallCaps(output)
          setRegElement('#regPersonDescription', output)
        } else {
          hideRegElementDescription('#regPersonDescription')
        }
      } catch (e) {
        $('#regPersonDescription').closest('.edition-info-item').css('display', 'none');
      }
      
      // hide the header when none of the "general information" values are set
      const header = document.querySelector('.arch-general').previousElementSibling
      const rows = document.querySelectorAll('.arch-general > div')
      const allHidden = Array.from(rows).every(row => {
          console.log(row)
          return row.style.display === 'none'
      })
      if (allHidden) {
          header.style.display = 'none'
      } else {
          header.style.display = 'inline'
      }
    }
  });
}

function getRestAPIPlaces(textGridURI) {
  console.log('getting Place with textGridURI: ' + textGridURI);
  getPlacesInWorks(textGridURI, '#modal-register-occurences-places-in-works');
  getPlacesInTexts(textGridURI, '#modal-register-occurences-places-text');
  $.ajax({
    type: 'GET',
    url: base_path + 'get-place/' + textGridURI,
    data: {
      get_param: 'value'
    },
    success: function(data) {
      var lang = '';
      var output;
      try {
        lang = getParameter('lang');
        if (!(lang == 'de')) {
          lang = 'fra'
        }
      } catch (e) {
        console.log(e);
        lang = 'fra';
      }
      //check unidentified
      try {
        output = data.place.type;
        if (output == 'unidentified') {
          $('#regPlaceUnidentified').css('display', 'flex');
        } else {
          $('#regPlaceUnidentified').css('display', 'none');
        }
      } catch (e) {}
      try {
        output = data.place.placeName.filter(el => el['xml:lang'] == lang).filter(el => el['type'] == 'current')[0]['#text'];
        if (typeof output === 'string') {
          output = fixNonBreakingWhitespace(output)
          setRegElement('#regPlaceTitle', output)
        } else {
          console.log('title error')
        }
      } catch (e) {
        console.log(e)
        $('#regPlaceTitle').text('ERROR');
      }
      try {
        output = data.place['subtype'];
        if (typeof output === 'string') {
          setRegElement('#regPlacePlaceNameType', translateTypePlace(output));
        } else {
          hideRegElement('#regPlacePlaceNameType')
        }
      } catch (e) {
        $('#regPlacePlaceNameType').text('ERROR');
      }
      try {
        output = data.place.idno.type;
        if (typeof output === 'string') {
          setRegElement('#regPlaceIDNOType', idnoTypeParser(output))
        } else {
          hideRegElement('#regPlaceIDNOType')
        }
      } catch (e) {
        hideRegElement('#regPlaceIDNOType');
      }
      try {
        output = data.place.idno['xml:base'];
        if (typeof output === 'string') {
          setRegElementLink('#regPlaceIDNOLink', output)
        } else {
          hideRegElement('#regPlaceIDNOLink')
        }
      } catch (e) {
        hideRegElement('#regPlaceIDNOLink');
      }
      try {
        output = data.place.placeName.filter(el => el['xml:lang'] == lang).filter(el => el['type'] == 'current')[0]['#text'];
        if (typeof output === 'string') {
          output = fixNonBreakingWhitespace(output)
          setRegElement('#regPlacePlaceNameCurrent', output)
        } else {
          hideRegElement('#regPlacePlaceNameCurrent')
        }
      } catch (e) {
        hideRegElement('#regPlacePlaceNameCurrent');
      }
      try {
        output = data.place.placeName.filter(el => el['xml:lang'] == lang).filter(el => el['type'] == 'historical')[0]['#text'];
        if (typeof output === 'string') {
          setRegElement('#regPlacePlaceNameHistorical', output)
        } else {
          hideRegElement('#regPlacePlaceNameHistorical')
        }
      } catch (e) {
        hideRegElement('#regPlacePlaceNameHistorical');
      }


      try {
        var functions = data.place.note.filter(el => el['type'] == 'function').filter(el => el['xml:lang'] == lang);
        output = '';
        var nonBreakingSpace = String.fromCharCode(160);
        if (functions.length > 0) {
          for (i = 0; i < functions.length; i++) {
            if (i == (functions.length - 1)) {
              output += functions[i]['#text'];
            } else {
              if (checkLanguageFrench()) {
                output += functions[i]['#text'] + nonBreakingSpace + '; ';
              } else {
                output += functions[i]['#text'] + '; ';
              }
            }
          }
        }
        if ((functions.length > 0) && (typeof output === 'string')) {
          setRegElement('#regPlaceFunction', output);
        } else {
          hideRegElement('#regPlaceFunction')
        }
      } catch (e) {
        hideRegElement('#regPlaceFunction');
      }
      try {
        output = data.place.note.filter(el => el['xml:lang'] == lang).filter(el => el['type'] == 'state')[0]['#text'];
        if (typeof output === 'string') {
          setRegElement('#regPlaceState', output)
        } else {
          hideRegElement('#regPlaceState')
        }
      } catch (e) {
        hideRegElement('#regPlaceState');
      }
      try {
        var artists = data.place.note.filter(el => el['type'] == 'artist')[0].persName;
        if (typeof artists === 'string') {
          // the exist serializer seems to encode a single xml element as a json
          // object but we want to treat is as an array with 1 element instead
          getRegArtist([artists], '#regPlaceArtist');
        } else if (typeof artists === 'object') {
          getRegArtist(artists, '#regPlaceArtist');
        } else {
          hideRegElement('#regPlaceArtist');
        }
      } catch (e) {
        hideRegElement('#regPlaceArtist');
      }
      try {
        output = data.place.note.filter(el => el['xml:lang'] == lang).filter(el => el['type'] == 'event')[0]['#text'];
        if (typeof output === 'string') {
          setRegElement('#regPlaceConstruction', output)
        } else {
          hideRegElement('#regPlaceConstruction')
        }
      } catch (e) {
        hideRegElement('#regPlaceConstruction');
      }
      try {
        output = data.place.location.geo;
        console.log(data.place.location.geo);
        if (typeof output === 'string') {
          console.log("output=string");
          var result = output;
          result = result.trim();
          result = "http://www.google.com/maps/place/" + result;
          setRegElementLink('#regPlaceLocationLink', result);
          setRegElement('#regPlaceLocationLinkText', output);
        } else if (typeof output['#text'] === 'string') {
          console.log("output['#text']=string");
          setRegElement('#regPlaceLocation', output['#text']);
          var result = output['#text'];
          result = result.trim();
          result = "http://www.google.com/maps/place/" + result;
          setRegElementLink('#regPlaceLocationLink', result);
          setRegElement('#regPlaceLocationLinkText', output);
        } else {
          console.log("hide");
          hideRegElement('#regPlaceLocation')
        }
      } catch (e) {
        hideRegElement('#regPlaceLocation');
      }
      try {
        output = data.place.note.filter(el => el['type'] == 'description').filter(el => el['xml:lang'] == lang)[0]['#text'];
        if (typeof output === 'string') {
          output = fixGermanQuotes(output)
          output = fixSmallCaps(output)
          output = fixNonBreakingWhitespace(output)
          setRegElement('#regPlaceDescription', output)
        } else {
          hideRegElementDescription('#regPlaceDescription')
        }
      } catch (e) {
        hideRegElementDescription('#regPlaceDescription');
      }
    }
  });
}

function fixGermanQuotes(text) {
    return text.replace(/"([^"]*)"/g, '„$1“')
}

function fixFrenchApostrophe(input) {
    return input.replace(/''/g, '’')
}

function fixSmallCaps(text) {
  return text.replace(/small-caps'>.*?<\/span/gi, function(t) {
    return t.toLowerCase();
  });
}

function fixNonBreakingWhitespace(text) {
    if (getLocale() == 'de') {return text}
    
    const result = text.
      replace(/ ([:;!\?])/g, '\u00A0$1').
      replace(/« /g, '«\u00A0').
      replace(/ »/g, '\u00A0»').
      replace(/ (I<sup>[^<]+<\/sup>)/g, '\u00A0$1').
      replace(/ ([IVXMDCL]+(?![a-z]))/g, '\u00A0$1').
      replace(/(p\.|vol\.|INV|n°) (\d+)/g, '$1\u00A0$2').
      replace(/(\d+) vol\./g, '$1\u00A0vol.').
      replace(/t\. (II|III|IV|V|VI|VII|VIII|IX|X|XI|XII|XIII|XIV|XV)/g, 't.\u00A0$1')
    return result
}

function augmentValue(input) {
    let result = input
    
    result = fixGermanQuotes(result)
    result = fixFrenchApostrophe(result)
    
    return result
}

function getLocale() {
    const url = AtUrl.current()
    return url.params()['lang']
}

function duoLingo(input) {
    const locale = getLocale('locale')
    const [de, fr] = input.split(/\s+\/\s+/)
    
    if (de && fr) {
        return (locale == 'de' ? de : fr)
    } else {
        return de
    }
}

function getRestAPIWorks(textGridURI) {
  console.log('getting Work with textGridURI: ' + textGridURI);
  getWorksInTexts(textGridURI, '#modal-register-occurences-works-text');
  $.ajax({
    type: 'GET',
    url: base_path + 'get-work/' + textGridURI,
    data: {
      get_param: 'value'
    },
    success: function(data) {
      var lang = '';
      var output;
      try {
        lang = getParameter('lang');
        if (!(lang == 'de')) {
          lang = 'fra'
        }
      } catch (e) {
        console.log(e);
        lang = 'fra';
      }
      //check unidentified
      try {
        output = data.item.type;
        if (output == 'unidentified') {
          $('#regWorkUnidentified').css('display', 'flex');
        } else {
          $('#regWorkUnidentified').css('display', 'none');
        }
      } catch (e) {}
      try {
        output = data.item.name.filter(el => el['xml:lang'] == lang)[0]['#text'];
        output = augmentValue(output)
        if (typeof output === 'string') {
          setRegElement('#regWorkTitle', output)
        } else {
          console.log('title error')
        }
      } catch (e) {
        $('#regWorkTitle').text('ERROR');
      }
      try {
        output = data.item.idno.type;
        if (typeof output === 'string') {
          setRegElement('#regWorkIDNOType', idnoTypeParser(output))
        } else {
          hideRegElement('#regWorkIDNOType')
        }
      } catch (e) {
        hideRegElement('#regWorkIDNOType');
      }
      try {
        output = data.item.idno['xml:base'];
        if (typeof output === 'string') {
          setRegElementLink('#regWorkIDNOLink', output)
        } else {
          hideRegElement('#regWorkIDNOLink')
        }
      } catch (e) {
        hideRegElement('#regWorkIDNOLink');
      }try {
        output = data.item.subtype;
        if (typeof output === 'string') {
          setRegElement('#regWorkType', translateTypeWork(output))
        } else {
          hideRegElement('#regWorkType')
        }
      } catch (e) {
        hideRegElement('#regWorkType');
      }
      try {
        var artists = data.item.note.filter(el => el['type'] == 'artist')[0].persName;
        getRegArtist(artists, '#regWorkArtist');
      } catch (e) {
        hideRegElement('#regWorkArtist');
      }
      try {
        output = data.item.date.filter(el => el['xml:lang'] == lang)[0]['#text'];
        if (typeof output === 'string') {
          setRegElement('#regWorkDate', output)
        } else {
          hideRegElement('#regWorkDate')
        }
      } catch (e) {
        hideRegElement('#regWorkDate');
      }
      try {
        output = data.item.note.filter(el => el['xml:lang'] == lang).filter(el => el['type'] == 'state')[0]['#text'];
        if (typeof output === 'string') {
          setRegElement('#regWorkState', output)
        } else {
          hideRegElement('#regWorkState')
        }
      } catch (e) {
        hideRegElement('#regWorkState');
      }
      try {
        output = data.item.date.filter(el => el['xml:lang'] == lang)[0]['#text'];
        if (typeof output === 'string') {
          setRegElement('#regWorkDate', output)
        } else {
          hideRegElement('#regWorkDate')
        }
      } catch (e) {
        hideRegElement('#regWorkDate');
      }
      try {
        output = '';
        let locations = data.item.location;
        if (!locations) {locations = []}
        if (!$.isArray(locations)) {locations = [locations]}
        locations = locations.filter(el => el['type'] == 'former').map(el => el.placeName)
        
        if (locations && locations != []) {
            getLocationPlaces(locations, '#regWorkLocationFormer')
        } else {
            hideRegElement('#regWorkLocationFormer')
        }
      } catch (e) {
        hideRegElement('#regWorkLocationFormer');
      }
      try {
        output = '';
        let locations = data.item.location;
        if (!locations) {locations = []}
        if (!$.isArray(locations)) {locations = [locations]}    
        locations = locations.filter(el => el['type'] == 'current').map(el => el.placeName)
        
        if (locations.length > 0) {
            getLocationPlaces(locations, '#regWorkLocationCurrent')
        } else {
            hideRegElement('#regWorkLocationCurrent')
        }
      } catch (e) {
        hideRegElement('#regWorkLocationCurrent');
      }
      try {
        output = data.item.note.filter(el => el['xml:lang'] == lang).filter(el => el['type'] == 'material')[0]['#text'];
        if (typeof output === 'string') {
          setRegElement('#regWorkMaterial', output)
        } else {
          hideRegElement('#regWorkMaterial')
        }
      } catch (e) {
        hideRegElement('#regWorkMaterial');
      }
      try {
        output = data.item.note.filter(el => el['xml:lang'] == lang).filter(el => el['type'] == 'dimensions')[0]['#text'];
        if (typeof output === 'string') {
          setRegElement('#regWorkDimensions', output)
        } else {
          hideRegElement('#regWorkDimensions')
        }
      } catch (e) {
        hideRegElement('#regWorkDimensions');
      }
      try {
        output = data.item.note.filter(el => el['type'] == 'description').filter(el => el['xml:lang'] == lang)[0]['#text'];
        if (typeof output === 'string') {
          output = augmentValue(output)
          output = fixSmallCaps(output)
          output = fixNonBreakingWhitespace(output)
          setRegElement('#regWorkDescription', output)
        } else {
          hideRegElementDescription('#regWorkDescription')
        }
      } catch (e) {
        hideRegElementDescription('#regWorkDescription');
      }
    }
  });
}

// remove url hash params when dialog is closed
$('.modal').on('hide.bs.modal', event => {
    const url = AtUrl.current()
    url.updateHashParams({modal: null, type: null, id: null, locale: null})
    const newHash = (url.formatHash() == '' ? ' ' : url.formatHash())
    history.pushState(null, null, newHash)
})
