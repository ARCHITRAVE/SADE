$('header nav button.arch-bars').on('click', function(event) {
  event.preventDefault();
  $('body').addClass('arch-nav-overlay');
});

$('header nav button.arch-cross').on('click', function(event) {
  event.preventDefault();
  $('body').removeClass('arch-nav-overlay');
});

$('body #editionNavBar .arch-bars').on('click', function(event) {
  event.preventDefault();
  $('body #editionNavBar .arch-cross').removeClass('arch-cross-invisible');
  $('body #editionNavBar .arch-bars').addClass('arch-bars-invisible');
});

$('body #editionNavBar .arch-cross').on('click', function(event) {
  event.preventDefault();
  $('body #editionNavBar .arch-cross').addClass('arch-cross-invisible');
  $('body #editionNavBar .arch-bars').removeClass('arch-bars-invisible');
});

$('body #editionNavBar').ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

$('.arch-foldout .arch-toggle').on('click', function(event) {
  event.preventDefault();
  let foldout = $(event.target).parents('.arch-foldout');
  foldout.toggleClass('arch-show');
});

$('.arch-search').on('click', 'a', function(event) {
  event.preventDefault();
  let input = $(event.target).parents('.arch-search').find('input');
  input.toggleClass('show');
  input.focus();
});

$('.arch-search').on('blur', 'input', function(event) {
  let input = $(event.target);
  if (input.val() === '') {
    input.removeClass('show');
  }
});
