xquery version "3.1";
import module namespace test="http://exist-db.org/xquery/xqsuite" at "resource:org/exist/xquery/lib/xqsuite/xqsuite.xql";
import module namespace tests="https://sade.textgrid.de/ns/tests" at "test.xqm";

test:suite(util:list-functions("https://sade.textgrid.de/ns/tests"))
