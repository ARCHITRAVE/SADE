xquery version "3.1";
(:~ Test Module for SADE :)
module namespace tests="https://sade.textgrid.de/ns/tests";

import module namespace app="https://sade.textgrid.de/ns/app" at "modules/app.xqm";
import module namespace config="https://sade.textgrid.de/ns/config" at "modules/config.xqm";
import module namespace multiviewer="https://sade.textgrid.de/ns/multiviewer" at "modules/multiviewer.xqm";
import module namespace nav="https://sade.textgrid.de/ns/navigation" at "modules/navigation.xqm";
import module namespace tgconnect="https://sade.textgrid.de/ns/connect" at "modules/textgrid/connect.xqm";

import module namespace test="http://exist-db.org/xquery/xqsuite" at "resource:org/exist/xquery/lib/xqsuite/xqsuite.xql";

declare variable $tests:node := <node/>;
declare variable $tests:model := map{};

(:~ One test to fail just to check for the test engine to recognize failing tests.
 : No, I do not trust you guys. :)
declare
    %test:assertTrue
function tests:fail() {
    util:eval("false()")
};

(: ******* :)
(: * APP * :)
(: ******* :)

declare
    %test:assertExists
    %test:assertXPath("string-length($result) gt 0")
function tests:app-title() {
    util:eval("app:title($tests:node, $tests:model)")
};

declare
    %test:assertXPath("matches($result, ""^\d{4}$"")")
    %test:assertXPath("matches($result, ""^20\d{2}$"")")
function tests:app-currentyear() {
    util:eval("app:currentyear($tests:node, $tests:model)")
};

declare
    %test:arg("exist-resource", "publish.html") %test:assertXPath("count($result) = 1")
    %test:arg("exist-resource", "content.html") %test:assertXPath("count($result) = 2")
    %test:arg("exist-resource", "unexpected.html") %test:assertEmpty
function tests:app-css-injection($exist-resource) {
    util:eval("app:css-injection($tests:node, $tests:model, $exist-resource)")
};

declare
    %test:arg("exist-resource", "publish.html") %test:assertXPath("count($result) = 1")
    %test:arg("exist-resource", "unexpected.html") %test:assertEmpty
function tests:app-javascript-injection($exist-resource) {
    util:eval("app:javascript-injection($tests:node, $tests:model, $exist-resource)")
};

declare
    %test:assertXPath("matches(string($result/@class), '^fa fa\-[a-z]+$')")
function tests:app-icon() {
    util:eval("app:icon($tests:node, $tests:model)")
};

declare
    %test:assertExists
function tests:app-getAllLanguages() {
    util:eval("app:getAllLanguages()")
};

(: ********** :)
(: * CONFIG * :)
(: ********** :)
declare
    %test:assertExists
function tests:config-app-root() {
    util:eval("$config:app-root")
};

declare
    %test:arg("key", "project-id") %test:assertTrue
    %test:arg("key", "some-key-not-available") %test:assertFalse
function tests:config-key-available($key as xs:string){
    util:eval("config:key-available($key)")
};

declare
    %test:arg("key", "project-id") %test:assertXPath("matches($result, '^.+$')")
function tests:config-get($key){
    util:eval("config:get($key)")
};

declare
    %test:args("confluence.lang", "wiki") %test:assertXPath("count($result) gt 0")
function tests:config-get($key, $module-key){
    util:eval("config:get($key, $module-key)")
};

(: *************** :)
(: * MULTIVIEWER * :)
(: *************** :)

declare
    %test:arg("id", "textgrid:10qd4") %test:assertEquals("/data/10qd4.xml")
    %test:arg("id", "textgrid:10qd4.0") %test:assertEquals("/data/10qd4.xml")
    %test:arg("id", "10qd4") %test:assertEquals("/data/10qd4.xml")
    %test:arg("id", "10qd4.0") %test:assertEquals("/data/10qd4.xml")
function tests:multiviewer-idResolver($id) {
    util:eval("multiviewer:idResolver($id)")
};

declare
    %test:arg("id", "/data/10qd4.xml") %test:assertXPath(" matches( $result, '^/db/apps/sade-?[a-zA-Z0-9\-\.]*/textgrid/data/10qd4.xml')")
    %test:arg("id", "/docs/about.md") %test:assertXPath(" matches( $result, '^/db/apps/sade-?[a-zA-Z0-9\-\.]*/docs/about.md')")
    %test:arg("id", "docs/about.md") %test:assertXPath(" matches( $result, '^/db/apps/sade-?[a-zA-Z0-9\-\.]*/docs/about.md')")
function tests:multiviewer-getPath($id) {
   util:eval("multiviewer:getPath($id)")
};

declare
    %test:arg("id", "/data/10qd4.xml") %test:assertXPath("starts-with($result, '&lt;TEI xmlns=""http://www.tei-c.org/ns/1.0""')")
function tests:multiviewer-raw($id) {
    util:eval("multiviewer:raw($tests:node, $tests:model, $id)")
};

declare
    %test:args("/db/apps/sade/docs/about.md") %test:assertXPath('local-name($result) = "div"')
function tests:multiviewer-markdown($docpath) {
    util:eval( "multiviewer:markdown($docpath)" )
};

(: ************** :)
(: * NAVIGATION * :)
(: ************** :)

declare
    %test:assertXPath("count( $result?navitems ) gt 0")
function tests:nav-navitems() {
    util:eval("nav:navitems($tests:node, $tests:model)")
};

(: ************* :)
(: * TGCONNECT * :)
(: ************* :)

(:~ this function is meant as a replacement for /modules/textgrid/publish.xql
 : due to the absence of a (http) request object :)
declare
    %test:arg("uri", "textgrid:10qd4.0") %test:assertXPath("$result/ok")
    %test:arg("uri", "textgrid:vz4c.0") %test:assertXPath("$result/ok")
    %test:arg("uri", "textgrid:18wz6.1") %test:assertXPath("$result/ok")
    %test:arg("uri", "") %test:assertError("PUBLISH01")
function tests:aaa-tgconnect-publish($uri as xs:string) as node()* {
let $sid as xs:string := ""
let $target as xs:string := "data"
let $user as xs:string := ""
let $password as xs:string := ""
let $project as xs:string := "testgrid"
let $surface as xs:string := ""
    (: to pass the test on installation, we have to skip the authentication
        because auth is not available in embedded mode :)
let $login as xs:boolean := true()

return
    if( $uri = "" ) then
        error(QName("https://sade.textgrid.de/ns/error", "PUBLISH01"), "no URI provided")
    else
        <div>{
            for $i in util:eval("tgconnect:publish($uri,$sid,$target,$user,$password,$project,$surface,$login)")
            return
                <ok>{ $uri } » { $i }</ok>
        }</div>

};

(: ************* :)
(: * ARCHITRAVE Editions * :)
(: ************* :)

(:~ this function tests if the TextGridURIs of all editions have been downloaded from TGRep and transformed to HTML :)
declare
    %test:arg("uri", "some-uri") %test:assertFalse
    %test:arg("uri", "34zmq") %test:assertTrue
    %test:arg("uri", "3r3nn") %test:assertTrue
    %test:arg("uri", "34zs7") %test:assertTrue
    %test:arg("uri", "3ptwg") %test:assertTrue
    %test:arg("uri", "34zmq") %test:assertTrue
    %test:arg("uri", "3qr4f") %test:assertTrue
    %test:arg("uri", "34znb") %test:assertTrue
    %test:arg("uri", "3czfj") %test:assertTrue
    %test:arg("uri", "3q4rq") %test:assertTrue
    %test:arg("uri", "3r0fv") %test:assertTrue
    %test:arg("uri", "350mg") %test:assertTrue
    %test:arg("uri", "3c0m2") %test:assertTrue
    %test:arg("uri", "3czn9") %test:assertTrue
function tests:uri-transformed($uri as xs:string){
    doc-available('/db/apps/sade-architrave/templates/' || $uri || '/' || $uri || '_p1.html')
};
